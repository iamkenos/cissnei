package org.bitbucket.iamkenos.cissnei.jfx;

import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.stage.Stage;
import org.bitbucket.iamkenos.cissnei.jfx.controller.assist.LayoutAssist;
import org.bitbucket.iamkenos.cissnei.jfx.controller.assist.SettingsAssist;
import org.bitbucket.iamkenos.cissnei.jfx.controller.assist.WindowAssist;
import org.bitbucket.iamkenos.cissnei.jfx.model.MySetting;
import org.bitbucket.iamkenos.cissnei.selenium.Driver.Browser;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.lang.Boolean.TRUE;
import static org.bitbucket.iamkenos.cissnei.runner.ParentHook.isStopped;
import static org.bitbucket.iamkenos.cissnei.setup.CoreSetup.CORE_CONFIG;
import static org.bitbucket.iamkenos.cissnei.setup.JFXSetup.JFX_CONFIG;
import static org.bitbucket.iamkenos.cissnei.setup.SeleniumSetup.SELENIUM_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public class FXApp extends Application {
    private Thread runnerThread;
    private Service<Void> testService;
    private ScheduledExecutorService scheduler;
    private LayoutAssist layoutAssist;
    private WindowAssist windowAssist;
    private SettingsAssist settingsAssist;
    private MySetting mySetting;
    private Stage stage;

    public FXApp() {
        /*
         * DEFAULT CONSTRUCTOR
         */
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void createScheduler() {
        stopScheduler();
        this.scheduler = Executors.newScheduledThreadPool(1);
    }

    public void stopScheduler() {
        if (scheduler != null)
            scheduler.shutdownNow();
    }

    public void createTestService() {
        testService = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        createRunnerThread();
                        runnerThread.start();
                        runnerThread.join();
                        return null;
                    }
                };
            }
        };
    }

    private void createRunnerThread() {
        runnerThread = new Thread(() -> {
            try {
                TestApp.main(new String[]{});
            } catch (Exception e) {
                LOGGER.error(String.format("Error in invoking %s", runnerThread.getName()));
            }
        }, TestApp.class.getSimpleName());
    }

    public void pauseRunner() {
        runnerThread.suspend();
    }

    public void stopRunner() {
        isStopped.setValue(TRUE);
    }

    public void resumeRunner() {
        runnerThread.resume();
    }

    public ScheduledExecutorService getScheduler() {
        return scheduler;
    }

    public Runnable scheduledTest() {
        return () -> {
            createTestService();
            windowAssist.refreshButtonListeners();
            windowAssist.start();
            settingsAssist.setScheduledRun(TRUE);
        };
    }

    public Service<Void> getTestService() {
        return testService;
    }

    public LayoutAssist getLayoutAssist() {
        return layoutAssist;
    }

    public WindowAssist getWindowAssist() {
        return windowAssist;
    }

    public SettingsAssist getSettingsAssist() {
        return settingsAssist;
    }

    public MySetting getMySetting() {
        return this.mySetting;
    }

    @Override
    public void start(Stage primaryStage) {
        mySetting = new MySetting(
                JFX_CONFIG.getTestRunnerTags(),
                JFX_CONFIG.getTestRunnerClass(),
                JFX_CONFIG.getTestFeatureDir(),
                Browser.fromOrdinal(SELENIUM_CONFIG.getWdInstance()),
                CORE_CONFIG.getDataFile(),
                JFX_CONFIG.getTestSchedulerTime(),
                JFX_CONFIG.getTestSchedulerEnabled());

        stage = primaryStage;
        layoutAssist = new LayoutAssist(stage, this);
        layoutAssist.load();

        windowAssist = new WindowAssist(layoutAssist.getThiz(), this);
        windowAssist.load();
        windowAssist.addListeners();
        windowAssist.loadInfoPanel();
        windowAssist.loadScenarioPanel();

        settingsAssist = new SettingsAssist(stage, this);
        settingsAssist.setScheduledRun(mySetting.getIsScheduled());
        settingsAssist.addListeners();

        LOGGER.info("::STARTING CISSNEI::::::::::::::::::::::::::::::::::::::::::");
    }
}
