package org.bitbucket.iamkenos.cissnei.jfx.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import static org.bitbucket.iamkenos.cissnei.selenium.Driver.Browser;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class MySetting {
    private final StringProperty tags;
    private final StringProperty runner;
    private final StringProperty features;
    private final StringProperty environment;
    private final StringProperty browser;
    private final StringProperty schedule;
    private final BooleanProperty isScheduled;

    public MySetting(String tags, String runner, String features, Browser browser, String environment, String schedule, Boolean isScheduled) {
        this.tags = new SimpleStringProperty(tags);
        this.runner = new SimpleStringProperty(runner);
        this.features = new SimpleStringProperty(features);
        this.environment = new SimpleStringProperty(environment);
        this.browser = new SimpleStringProperty(browser.value);
        this.schedule = new SimpleStringProperty(schedule);
        this.isScheduled = new SimpleBooleanProperty(isScheduled);
    }

    public String getTags() {
        return tags.get();
    }

    public void setTags(String tags) {
        this.tags.set(tags);
    }

    public StringProperty tagsProperty() {
        return tags;
    }

    public String getRunner() {
        return runner.get();
    }

    public void setRunner(String runner) {
        this.runner.set(runner);
    }

    public StringProperty runnerProperty() {
        return runner;
    }

    public String getFeatures() {
        return features.get();
    }

    public void setFeatures(String features) {
        this.features.set(features);
    }

    public StringProperty featuresProperty() {
        return features;
    }

    public String getEnvironment() {
        return environment.get();
    }

    public void setEnvironment(String environment) {
        this.environment.set(environment);
    }

    public StringProperty environmentProperty() {
        return environment;
    }

    public String getBrowser() {
        return browser.get();
    }

    public void setBrowser(Browser browser) {
        this.browser.set(browser.value);
    }

    public StringProperty browserProperty() {
        return browser;
    }

    public String getSchedule() {
        return schedule.get();
    }

    public void setSchedule(String schedule) {
        this.schedule.set(schedule);
    }

    public StringProperty scheduleProperty() {
        return schedule;
    }

    public boolean getIsScheduled() {
        return isScheduled.get();
    }

    public void setIsScheduled(Boolean isScheduled) {
        this.isScheduled.set(isScheduled);
    }

    public BooleanProperty isScheduledProperty() {
        return isScheduled;
    }
}
