package org.bitbucket.iamkenos.cissnei.jfx.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class MyScenario {
    private final StringProperty status;
    private final StringProperty name;

    public MyScenario(String status, String name) {
        this.status = new SimpleStringProperty(status);
        this.name = new SimpleStringProperty(name);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }
}
