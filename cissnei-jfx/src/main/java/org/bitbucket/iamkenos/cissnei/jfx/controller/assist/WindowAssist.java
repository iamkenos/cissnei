package org.bitbucket.iamkenos.cissnei.jfx.controller.assist;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import com.sun.javafx.scene.control.skin.TableViewSkin;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import org.bitbucket.iamkenos.cissnei.jfx.FXApp;
import org.bitbucket.iamkenos.cissnei.jfx.controller.Window;
import org.bitbucket.iamkenos.cissnei.jfx.model.MyScenario;
import org.bitbucket.iamkenos.cissnei.setup.JFXSetup;
import org.bitbucket.iamkenos.cissnei.utils.FileUtils;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import static cucumber.api.Result.Type.*;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static javafx.beans.binding.Bindings.and;
import static javafx.beans.binding.Bindings.when;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bitbucket.iamkenos.cissnei.jfx.controller.assist.WindowAssist.BtnLabel.*;
import static org.bitbucket.iamkenos.cissnei.jfx.controller.assist.WindowAssist.InfoStatus.*;
import static org.bitbucket.iamkenos.cissnei.runner.ParentHook.*;
import static org.bitbucket.iamkenos.cissnei.setup.ReportSetup.DEF_REPORT_HTML_PATH;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class WindowAssist {
    private static final String HEX_PASSED = "#478131";
    private static final String HEX_FAILED = "#803333";
    private static final String HEX_PENDING = "#86592D";
    private static final String HEX_UNDEFINED = "#B38F00";
    private static final String HEX_OTHERS = "#1E6D94";

    private ObservableList<MyScenario> myScenarioData;
    private FXApp mainApp;
    private BorderPane layout;
    private AnchorPane thiz;
    private Window controller;

    public WindowAssist(BorderPane layout, FXApp mainApp) {
        this.layout = layout;
        this.mainApp = mainApp;
        this.myScenarioData = observableArrayList();
    }

    public void load() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXSetup.getFXML(Window.class));
            thiz = loader.load();

            controller = loader.getController();
            controller.setAssist(this);
            mainApp.createTestService();

            layout.setCenter(thiz);

            controller.getBtnStart().setDisable(FALSE);
            controller.getBtnPause().setDisable(TRUE);
            controller.getBtnResults().setDisable(FALSE);
        } catch (IOException e) {
            LogUtils.LOGGER.fatal(e.getMessage(), e);
        }
    }

    public void start() {
        switch (BtnLabel.fromString(controller.getBtnStart().getText())) {
            case START:
                myScenarioData.clear();
                mainApp.getTestService().restart();
                break;
            case STOP:
                mainApp.stopRunner();
                break;
        }
    }

    public void pause() {
        switch (BtnLabel.fromString(controller.getBtnPause().getText())) {
            case PAUSE:
                mainApp.pauseRunner();
                controller.getBtnPause().setText(RESUME.value);
                break;
            case RESUME:
                mainApp.resumeRunner();
                controller.getBtnPause().setText(PAUSE.value);
                break;
        }
    }

    public void results() {
        FileUtils.launchFile(DEF_REPORT_HTML_PATH);
    }

    public void addListeners() {
        //INFO STATUS
        mainApp.getTestService().runningProperty().addListener((obs, old, nu) -> {
            if (nu)
                controller.getLblStatus().setText(RUNNING.value);
            else
                controller.getLblStatus().setText(READY.value);
        });
        isStopped.addListener((obs, old, nu) -> {
            if (nu)
                controller.getLblStatus().setText(STOPPING.value);
        });
        //SCENARIO PANEL
        isFinished.addListener((obs, old, nu) -> {
            if (nu)
                myScenarioData.add(new MyScenario(scenario.getStatus().name(), scenario.getName()));
        });
        //TAGS TEXT FIELD
        controller.getTxtTags().textProperty().addListener((obs, old, nu) -> {
            mainApp.getMySetting().setTags(nu);
            JFXSetup.JFX_CONFIG.setTestRunnerTags(nu);
        });
        //BUTTONS
        refreshButtonListeners();
    }

    public void refreshButtonListeners() {
        controller.getBtnStart().textProperty()
                .bind(when(mainApp.getTestService().runningProperty())
                        .then(STOP.value)
                        .otherwise(START.value));
        controller.getBtnStart().disableProperty()
                .bind(and(isStopped, controller.getBtnStart().textProperty().isEqualTo(STOP.value))
                        .or(controller.getBtnPause().textProperty().isEqualTo(RESUME.value)));
        controller.getBtnPause().disableProperty()
                .bind(and(isStopped, controller.getBtnStart().textProperty().isEqualTo(STOP.value))
                        .or(mainApp.getTestService().runningProperty().not()));
        controller.getBtnResults().disableProperty()
                .bind(mainApp.getTestService().runningProperty());
    }

    public void loadScenarioPanel() {
        controller.getTxtTags().setText(mainApp.getMySetting().getTags());
        controller.getTblScenario().setPlaceholder(new Label(EMPTY));
        controller.getTblScenario().setColumnResizePolicy((param) -> true);
        controller.getTblScenario().widthProperty().addListener((obs, old, nu) -> disableColumnsSorting(controller.getTblScenario()));
        controller.getTblScenario().setItems(myScenarioData);
        controller.getTblColName().setCellValueFactory(data -> data.getValue().nameProperty());
        controller.getTblColStatus().setCellValueFactory(data -> data.getValue().statusProperty());
        controller.getTblColStatus().setCellFactory(column -> new TableCell<MyScenario, String>() {
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? EMPTY : getItem());
                if (!isEmpty()) {
                    if (item.equals(PASSED.name()))
                        setStyle(textFill(HEX_PASSED));
                    else if (item.equals(FAILED.name()))
                        setStyle(textFill(HEX_FAILED));
                    else if (item.equals(PENDING.name()))
                        setStyle(textFill(HEX_PENDING));
                    else if (item.equals(UNDEFINED.name()))
                        setStyle(textFill(HEX_UNDEFINED));
                    else
                        setStyle(textFill(HEX_OTHERS));
                }
                customTableResize(controller.getTblScenario());
            }
        });
    }

    public void loadInfoPanel() {
        controller.getLblEnvironment().textProperty().bind(mainApp.getMySetting().environmentProperty());
        controller.getLblBrowser().textProperty().bind(mainApp.getMySetting().browserProperty());
        controller.getLblScheduler().textProperty()
                .bind(when(mainApp.getMySetting().isScheduledProperty())
                        .then(mainApp.getMySetting().scheduleProperty()).otherwise("Disabled"));
        controller.getLblStatus().setText(READY.value);
    }

    private void disableColumnsSorting(TableView<?> view) {
        TableHeaderRow header = (TableHeaderRow) view.lookup("TableHeaderRow");
        header.reorderingProperty().addListener((obs, oldVal, newVal) -> header.setReordering(false));
    }

    private void customTableResize(TableView<?> view) {
        TableViewSkin<?> skin = (TableViewSkin<?>) view.getSkin();
        skin.getTableHeaderRow().getRootHeader().getColumnHeaders().forEach(header ->
                Optional.ofNullable(header.getTableColumn()).ifPresent(column -> {
                    try {
                        Method method = skin.getClass().getDeclaredMethod("resizeColumnToFitContent", TableColumn.class, int.class);
                        method.setAccessible(true);
                        method.invoke(skin, column, 30);
                    } catch (Exception e) {
                        LogUtils.LOGGER.error(e.getMessage(), e);
                    }
                }));
    }

    private String textFill(String fill) {
        return format("-fx-text-fill: %s;", fill);
    }

    enum BtnLabel {
        START("Start"),
        STOP("Stop"),
        PAUSE("Pause"),
        RESUME("Resume");

        private final String value;

        BtnLabel(final String value) {
            this.value = value;
        }

        private static final BtnLabel fromString(String value) {
            return Arrays.stream(values()).filter(item -> item.value.equals(value)).findFirst().get();
        }

        private static final BtnLabel fromOrdinal(Integer ordinal) {
            return values()[ordinal];
        }
    }

    enum InfoStatus {
        READY("Ready"),
        RUNNING("Running..."),
        STOPPING("Stopping...");

        private final String value;

        InfoStatus(final String value) {
            this.value = value;
        }

        private static final InfoStatus fromString(String value) {
            return Arrays.stream(values()).filter(item -> item.value.equals(value)).findFirst().get();
        }

        private static final InfoStatus fromOrdinal(Integer ordinal) {
            return values()[ordinal];
        }
    }
}