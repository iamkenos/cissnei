package org.bitbucket.iamkenos.cissnei.jfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.bitbucket.iamkenos.cissnei.jfx.controller.assist.WindowAssist;
import org.bitbucket.iamkenos.cissnei.jfx.model.MyScenario;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Window {
    @FXML
    private Button btnStart;
    @FXML
    private Button btnPause;
    @FXML
    private Button btnResults;
    @FXML
    private TextField txtTags;
    @FXML
    private Label lblEnvironment;
    @FXML
    private Label lblBrowser;
    @FXML
    private Label lblScheduler;
    @FXML
    private Label lblStatus;
    @FXML
    private TableView<MyScenario> tblScenario;
    @FXML
    private TableColumn<MyScenario, String> tblColStatus;
    @FXML
    private TableColumn<MyScenario, String> tblColName;

    private WindowAssist assist;

    @FXML
    private void initialize() {
    }

    public Button getBtnStart() {
        return btnStart;
    }

    public Button getBtnPause() {
        return btnPause;
    }

    public Button getBtnResults() {
        return btnResults;
    }

    public TextField getTxtTags() {
        return txtTags;
    }

    public Label getLblEnvironment() {
        return lblEnvironment;
    }

    public Label getLblBrowser() {
        return lblBrowser;
    }

    public Label getLblScheduler() {
        return lblScheduler;
    }

    public Label getLblStatus() {
        return lblStatus;
    }

    public TableView<MyScenario> getTblScenario() {
        return tblScenario;
    }

    public TableColumn<MyScenario, String> getTblColStatus() {
        return tblColStatus;
    }

    public TableColumn<MyScenario, String> getTblColName() {
        return tblColName;
    }

    public void setAssist(WindowAssist assist) {
        this.assist = assist;
    }

    @FXML
    private void handleStart() {
        assist.start();
    }

    @FXML
    private void handlePause() {
        assist.pause();
    }

    @FXML
    private void handleResults() {
        assist.results();
    }
}
