package org.bitbucket.iamkenos.cissnei.jfx.controller.assist;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.bitbucket.iamkenos.cissnei.jfx.FXApp;
import org.bitbucket.iamkenos.cissnei.jfx.controller.Layout;
import org.bitbucket.iamkenos.cissnei.setup.JFXSetup;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.ClassLoader.getSystemClassLoader;
import static org.bitbucket.iamkenos.cissnei.setup.CoreSetup.CORE_CONFIG;
import static org.bitbucket.iamkenos.cissnei.setup.JFXSetup.JFX_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.FileUtils.launchFile;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class LayoutAssist {
    private FXApp fxApp;
    private Stage stage;
    private BorderPane thiz;
    private Layout controller;

    public LayoutAssist(Stage stage, FXApp fxApp) {
        this.stage = stage;
        this.fxApp = fxApp;
    }

    public BorderPane getThiz() {
        return thiz;
    }

    public Layout load() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXSetup.getFXML(Layout.class));
            thiz = loader.load();

            Scene scene = new Scene(thiz);
            stage.getIcons().add(new Image(getSystemClassLoader().getResourceAsStream("images/cissnei.png")));
            stage.setScene(scene);
            stage.setResizable(false);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setAlwaysOnTop(Boolean.TRUE);

            controller = loader.getController();
            controller.setAssist(this);

            setPosition();
            handleDragDrop();
            stage.show();

            return controller;
        } catch (IOException e) {
            LogUtils.LOGGER.fatal(e.getMessage(), e);
            return null;
        }
    }

    public void close() {
        Platform.exit();
        System.exit(0);
    }

    public void min() {
        stage.setIconified(true);
    }

    public void open(String location) {
        launchFile(location);
    }

    public void loadSettings() {
        fxApp.getSettingsAssist().load();
        fxApp.getSettingsAssist().loadValues();

        fxApp.getSettingsAssist().buildBrowserComboBox();
        fxApp.getSettingsAssist().buildEnvComboBox();
        fxApp.getSettingsAssist().addSchedulerListener();
        fxApp.getSettingsAssist().addTxtSchedulerRestriction();
    }

    public void uberTemplate() {
        launchFile(JFX_CONFIG.copyUberTemplate());
    }

    public void protoTemplate() {
        launchFile(CORE_CONFIG.copyProtoTemplate());
    }

    private void setPosition() {
        final Rectangle2D bounds = Screen.getPrimary().getVisualBounds();

        stage.setX(bounds.getMinX() + bounds.getWidth() - thiz.getPrefWidth());
        stage.setY(bounds.getMinY() + bounds.getHeight() - thiz.getPrefHeight());
    }

    private void handleDragDrop() {
        final AtomicReference<Double> xOffset = new AtomicReference<>();
        final AtomicReference<Double> yOffset = new AtomicReference<>();

        thiz.setOnMousePressed(event -> {
            xOffset.set(event.getSceneX());
            yOffset.set(event.getSceneY());
        });

        thiz.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - xOffset.get());
            stage.setY(event.getScreenY() - yOffset.get());
        });
    }
}
