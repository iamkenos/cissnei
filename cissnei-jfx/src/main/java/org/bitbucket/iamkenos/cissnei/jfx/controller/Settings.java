package org.bitbucket.iamkenos.cissnei.jfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.bitbucket.iamkenos.cissnei.jfx.controller.assist.SettingsAssist;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Settings {
    @FXML
    private TextField txtRunner;
    @FXML
    private TextField txtFeatures;
    @FXML
    private ComboBox<String> ddlEnv;
    @FXML
    private ComboBox<String> ddlBrowser;
    @FXML
    private TextField txtScheduler;
    @FXML
    private CheckBox cBoxScheduler;
    @FXML
    private Label lblError;

    private SettingsAssist assist;

    @FXML
    private void initialize() {
    }

    public TextField getTxtRunner() {
        return txtRunner;
    }

    public TextField getTxtFeatures() {
        return txtFeatures;
    }

    public ComboBox<String> getDdlEnv() {
        return ddlEnv;
    }

    public ComboBox<String> getDdlBrowser() {
        return ddlBrowser;
    }

    public TextField getTxtScheduler() {
        return txtScheduler;
    }

    public CheckBox getCBoxScheduler() {
        return cBoxScheduler;
    }

    public void setAssist(SettingsAssist assist) {
        this.assist = assist;
    }

    public Label getLblError() {
        return lblError;
    }

    @FXML
    private void handleCancel() {
        assist.cancel();
    }

    @FXML
    private void handleSave() {
        assist.save();
    }
}
