package org.bitbucket.iamkenos.cissnei.config;

import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;

import static org.bitbucket.iamkenos.cissnei.Commons.DEF_TEMPLATES_DIR;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class JFXConfig extends CommonsConfig {

    /*
     *************************************************************************
     * JFX PROPERTIES
     *************************************************************************
     */
    private static final String TEST_RUNNER_TAGS = "test.runner.tags";
    private static final String TEST_RUNNER_CLASS = "test.runner.class";
    private static final String TEST_FEATURE_DIR = "test.feature.dir";
    private static final String TEST_SCHEDULER_ENABLED = "test.scheduler.enabled";
    private static final String TEST_SCHEDULER_TIME = "test.scheduler.time";

    private static final String PROP_FILE = "cissnei-jfx.properties";
    private ConfigAdapter properties;

    public JFXConfig() {
        properties = getProperties(PROP_FILE, this.getClass());
    }


    public String getTestRunnerTags() {
        return getValue(properties, TEST_RUNNER_TAGS);
    }

    public void setTestRunnerTags(String value) {
        setValue(properties, TEST_RUNNER_TAGS, value);
    }

    public String getTestRunnerClass() {
        return getValue(properties, TEST_RUNNER_CLASS);
    }

    public void setTestRunnerClass(String value) {
        setValue(properties, TEST_RUNNER_CLASS, value);
    }

    public String getTestFeatureDir() {
        return getValue(properties, TEST_FEATURE_DIR);
    }

    public void setTestFeatureDir(String value) {
        setValue(properties, TEST_FEATURE_DIR, value);
    }

    public Boolean getTestSchedulerEnabled() {
        return Boolean.valueOf(getValue(properties, TEST_SCHEDULER_ENABLED));
    }

    public void setTestSchedulerEnabled(Boolean value) {
        setValue(properties, TEST_SCHEDULER_ENABLED, String.valueOf(value));
    }

    public String getTestSchedulerTime() {
        return getValue(properties, TEST_SCHEDULER_TIME);
    }

    public void setTestSchedulerTime(String value) {
        setValue(properties, TEST_SCHEDULER_TIME, value);
    }

    public String copyUberTemplate() {
        return copyToResources(this.getClass(), DEF_TEMPLATES_DIR + "pom-template.xml");
    }
}
