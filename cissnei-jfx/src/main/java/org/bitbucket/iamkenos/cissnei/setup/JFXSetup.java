package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.config.JFXConfig;

import java.net.URL;

import static java.lang.ClassLoader.getSystemClassLoader;
import static java.lang.String.format;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class JFXSetup {
    public static final JFXConfig JFX_CONFIG = new JFXConfig();

    private JFXSetup() {

    }

    public static URL getFXML(Class clazz) {
        return getSystemClassLoader().getResource(format("view/%s.fxml", clazz.getSimpleName()));
    }
}
