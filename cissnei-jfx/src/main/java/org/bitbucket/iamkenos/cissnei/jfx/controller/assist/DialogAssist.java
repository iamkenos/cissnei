package org.bitbucket.iamkenos.cissnei.jfx.controller.assist;

import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import static javafx.stage.Modality.WINDOW_MODAL;
import static javafx.stage.StageStyle.UNDECORATED;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class DialogAssist {
    private Stage stage;
    private Stage dialogStage;

    public DialogAssist(Stage stage) {
        this.stage = stage;
    }

    public void load(AnchorPane dialog) {
        Double centerX = stage.getX() + stage.getWidth() / 2d;
        Double centerY = stage.getY() + stage.getHeight() / 2d;

        Scene scene = new Scene(dialog);
        dialogStage = new Stage();
        dialogStage.initStyle(UNDECORATED);
        dialogStage.initModality(WINDOW_MODAL);
        dialogStage.initOwner(stage);
        dialogStage.setScene(scene);

        dialogStage.setOnShowing(e -> dialogStage.hide());
        dialogStage.setOnShown(e -> {
            dialogStage.setX(centerX - dialogStage.getWidth() / 2d);
            dialogStage.setY(centerY - dialogStage.getHeight() / 2d);
            dialogStage.show();
        });

        dialogStage.show();
    }

    public void close() {
        dialogStage.close();
    }
}