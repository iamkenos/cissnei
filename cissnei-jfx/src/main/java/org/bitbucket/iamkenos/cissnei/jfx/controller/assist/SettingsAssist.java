package org.bitbucket.iamkenos.cissnei.jfx.controller.assist;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.bitbucket.iamkenos.cissnei.jfx.FXApp;
import org.bitbucket.iamkenos.cissnei.jfx.controller.Settings;
import org.bitbucket.iamkenos.cissnei.setup.CoreSetup;
import org.bitbucket.iamkenos.cissnei.setup.JFXSetup;
import org.bitbucket.iamkenos.cissnei.setup.SeleniumSetup;
import org.bitbucket.iamkenos.cissnei.utils.DateUtils;
import org.bitbucket.iamkenos.cissnei.utils.FileUtils;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.UnaryOperator;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Arrays.asList;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.commons.lang3.StringUtils.*;
import static org.bitbucket.iamkenos.cissnei.selenium.Driver.Browser;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class SettingsAssist {
    private static final String OFFSET_DELIMITER = "+";
    private static final String CAP_GRP_0 = "(:)";
    private static final String CAP_GRP_1 = "([0-1]\\d?|2[0-3]?)";
    private static final String CAP_GRP_2 = "([0-5]\\d?)";
    private static final String CAP_GRP_3 = "\\" + OFFSET_DELIMITER;
    private static final String CAP_GRP_4 = "(" + CAP_GRP_3 + "[1-7])?";
    private static final String CAP_GRP_5 = CAP_GRP_1 + CAP_GRP_0 + CAP_GRP_2 + CAP_GRP_0 + CAP_GRP_2;

    private FXApp fxApp;
    private Stage stage;
    private AnchorPane thiz;
    private DialogAssist assist;
    private Settings controller;

    public SettingsAssist(Stage stage, FXApp mainApp) {
        this.stage = stage;
        this.fxApp = mainApp;
    }

    public void load() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXSetup.getFXML(Settings.class));
            thiz = loader.load();

            controller = loader.getController();
            controller.setAssist(this);

            assist = new DialogAssist(stage);
            assist.load(thiz);
        } catch (IOException e) {
            LogUtils.LOGGER.fatal(e.getMessage(), e);
        }
    }

    public void cancel() {
        assist.close();
    }

    public void save() {
        if (isSettingsValid()) {
            fxApp.getMySetting().setRunner(controller.getTxtRunner().getText());
            fxApp.getMySetting().setFeatures(controller.getTxtFeatures().getText());
            fxApp.getMySetting().setEnvironment(controller.getDdlEnv().getValue());
            fxApp.getMySetting().setBrowser(Browser.fromString(controller.getDdlBrowser().getValue()));
            fxApp.getMySetting().setIsScheduled(controller.getCBoxScheduler().isSelected());
            fxApp.getMySetting().setSchedule(controller.getTxtScheduler().getText());
            controller.getLblError().setVisible(FALSE);
            assist.close();
        } else {
            controller.getLblError().setVisible(TRUE);
        }
    }

    public void loadValues() {
        controller.getTxtRunner().setText(fxApp.getMySetting().getRunner());
        controller.getTxtFeatures().setText(fxApp.getMySetting().getFeatures());
        controller.getDdlEnv().setValue(fxApp.getMySetting().getEnvironment());
        controller.getDdlBrowser().setValue(fxApp.getMySetting().getBrowser());
        controller.getTxtScheduler().setText(fxApp.getMySetting().getSchedule());
        controller.getTxtScheduler().setDisable(!fxApp.getMySetting().getIsScheduled());
        controller.getCBoxScheduler().setSelected(fxApp.getMySetting().getIsScheduled());
    }

    public void addListeners() {
        fxApp.getMySetting().runnerProperty().addListener((obs, old, nu) -> JFXSetup.JFX_CONFIG.setTestRunnerClass(nu));
        fxApp.getMySetting().featuresProperty().addListener((obs, old, nu) -> JFXSetup.JFX_CONFIG.setTestFeatureDir(nu));
        fxApp.getMySetting().environmentProperty().addListener((obs, old, nu) -> CoreSetup.CORE_CONFIG.setDataFile(nu));
        fxApp.getMySetting().browserProperty().addListener((obs, old, nu) -> SeleniumSetup.SELENIUM_CONFIG.setWdInstance(Browser.fromString(nu).ordinal()));
        fxApp.getMySetting().isScheduledProperty().addListener((obs, old, nu) -> {
            JFXSetup.JFX_CONFIG.setTestSchedulerEnabled(nu);
            setScheduledRun(nu);
        });
        fxApp.getMySetting().scheduleProperty().addListener((obs, old, nu) -> {
            JFXSetup.JFX_CONFIG.setTestSchedulerTime(nu);
            setScheduledRun(fxApp.getMySetting().getIsScheduled());
        });
    }

    public void buildBrowserComboBox() {
        ObservableList<String> browsers = observableArrayList();

        Arrays.stream(Browser.values()).forEach(browser -> browsers.add(browser.value));
        controller.getDdlBrowser().setItems(browsers);
    }

    public void buildEnvComboBox() {
        ObservableList<String> environments = observableArrayList();
        FileUtils.getFileNames(CoreSetup.CORE_CONFIG.getDataDir())
                .forEach(item -> environments.add(removeEnd(item, CoreSetup.CORE_CONFIG.getDataFileFormat())));

        controller.getDdlEnv().setItems(environments);
    }

    public void addSchedulerListener() {
        controller.getCBoxScheduler().selectedProperty().addListener((obs, old, nu) -> controller.getTxtScheduler().setDisable(!nu));
    }

    public void setScheduledRun(Boolean isScheduled) {
        if (isScheduled) {
            Long diff = getTimeDifference();
            fxApp.createScheduler();
            fxApp.getScheduler().schedule(fxApp.scheduledTest(), diff, TimeUnit.SECONDS);
        } else {
            fxApp.stopScheduler();
        }
    }

    public void addTxtSchedulerRestriction() {
        UnaryOperator<Change> filter = text -> {
            if (text.isAdded())
                if (text.getControlNewText().length() <= 2)
                    filterText(text, regexWrap(CAP_GRP_1));
                else if (text.getControlNewText().length() <= 3)
                    filterText(text, regexWrap(CAP_GRP_1 + CAP_GRP_0));
                else if (text.getControlNewText().length() <= 5)
                    filterText(text, regexWrap(CAP_GRP_1 + CAP_GRP_0 + CAP_GRP_2));
                else if (text.getControlNewText().length() <= 6)
                    filterText(text, regexWrap(CAP_GRP_1 + CAP_GRP_0 + CAP_GRP_2 + CAP_GRP_0));
                else if (text.getControlNewText().length() <= 8)
                    filterText(text, regexWrap(CAP_GRP_5));
                else if (text.getControlNewText().length() <= 9)
                    filterText(text, regexWrap(CAP_GRP_5 + CAP_GRP_3));
                else if (text.getControlNewText().length() <= 10)
                    filterText(text, regexWrap(CAP_GRP_5 + CAP_GRP_4));
                else
                    text.setText(EMPTY);
            return text;
        };

        controller.getTxtScheduler().setTextFormatter(new TextFormatter<>(filter));
    }

    private void filterText(Change change, String regex) {
        if (!change.getControlNewText().matches(regex))
            change.setText(EMPTY);
    }

    private Boolean isSettingsValid() {
        List<Object> mySettings = new ArrayList<>(asList(
                controller.getTxtRunner().getText(),
                controller.getDdlEnv().getValue(),
                controller.getDdlBrowser().getValue()));

        Boolean firstCheck = !mySettings.contains(null);
        Boolean secondCheck = controller.getTxtScheduler().getText().matches(regexWrap(CAP_GRP_5 + CAP_GRP_4));
        secondCheck = !controller.getCBoxScheduler().isSelected() ? TRUE : secondCheck;

        return firstCheck && secondCheck;
    }

    private Long getTimeDifference() {
        String base = fxApp.getMySetting().getSchedule();
        String target = substringBefore(base, OFFSET_DELIMITER);
        String offset = substringAfter(base, OFFSET_DELIMITER);
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime schedule = DateUtils.newLocalDateTime("yyyy-M-d HH:mm:ss", String.format("%s-%s-%s %s",
                now.getYear(),
                now.getMonth().getValue(),
                now.getDayOfMonth(),
                target)).atZone(ZoneId.systemDefault());

        if (offset.isEmpty())
            if (SECONDS.between(now, schedule) <= 0)
                return SECONDS.between(now, schedule.plusDays(1));
            else
                return SECONDS.between(now, schedule);
        else
            return SECONDS.between(now, schedule.plusDays(Integer.valueOf(offset)));
    }

    private String regexWrap(String str) {
        return "^" + str + "$";
    }
}
