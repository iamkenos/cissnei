package org.bitbucket.iamkenos.cissnei.jfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.bitbucket.iamkenos.cissnei.jfx.controller.assist.LayoutAssist;
import org.bitbucket.iamkenos.cissnei.setup.JFXSetup;

import static org.bitbucket.iamkenos.cissnei.setup.CoreSetup.CORE_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.FileUtils.launchLink;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Layout {
    @FXML
    private Button btnClose;
    @FXML
    private Button btnMin;

    private LayoutAssist assist;

    @FXML
    private void initialize() {
    }

    public LayoutAssist getAssist() {
        return assist;
    }

    public void setAssist(LayoutAssist assist) {
        this.assist = assist;
    }

    @FXML
    private void handleClose() {
        assist.close();
    }

    @FXML
    private void handleMin() {
        assist.min();
    }

    @FXML
    private void openFeatures() {
        assist.open(JFXSetup.JFX_CONFIG.getTestFeatureDir());
    }

    @FXML
    private void openData() {
        assist.open(CORE_CONFIG.getDataDir());
    }

    @FXML
    private void openSettings() {
        assist.loadSettings();
    }

    @FXML
    private void openLogs() {
        assist.open("logs/");
    }

    @FXML
    private void copyUberTemplate() {
        assist.open(JFXSetup.JFX_CONFIG.copyUberTemplate());
    }

    @FXML
    private void copyProtoTemplate() {
        assist.open(CORE_CONFIG.copyProtoTemplate());
    }

    @FXML
    private void openUserGuide() {
        launchLink("https://bitbucket.org/iamkenos/cissnei");
    }
}
