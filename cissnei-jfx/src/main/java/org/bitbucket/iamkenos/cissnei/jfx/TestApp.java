package org.bitbucket.iamkenos.cissnei.jfx;

import cucumber.api.CucumberOptions;
import org.bitbucket.iamkenos.cissnei.adapter.OptionsAdapter;
import org.bitbucket.iamkenos.cissnei.adapter.RunWithAdapter;
import org.bitbucket.iamkenos.cissnei.junit.Cissnei;
import org.bitbucket.iamkenos.cissnei.junit.CissneiCore;
import org.bitbucket.iamkenos.cissnei.utils.AnnotationUtils;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;
import org.junit.runner.RunWith;

import static org.bitbucket.iamkenos.cissnei.setup.JFXSetup.JFX_CONFIG;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class TestApp {
    public static void main(String[] args) throws Exception {
        try {
            test(Class.forName(JFX_CONFIG.getTestRunnerClass()));
        } catch (Exception e) {
            LogUtils.LOGGER.fatal(e.getMessage(), e);
            throw e;
        }
    }

    private static void test(Class<?> runnerClass) {
        try {
            RunWithAdapter runWithAdapter = new RunWithAdapter(runnerClass.getAnnotation(RunWith.class));
            OptionsAdapter optionsAdapter = new OptionsAdapter(runnerClass.getAnnotation(CucumberOptions.class));
            runWithAdapter.setRunner(Cissnei.class);
            optionsAdapter.setFeatures(new String[]{JFX_CONFIG.getTestFeatureDir()});
            optionsAdapter.setTags(new String[]{JFX_CONFIG.getTestRunnerTags()});
            AnnotationUtils.modifyOn(runnerClass, RunWith.class, runWithAdapter);
            AnnotationUtils.modifyOn(runnerClass, CucumberOptions.class, optionsAdapter);

            new CissneiCore().run(runnerClass);
        } catch (Exception e) {
            LogUtils.LOGGER.fatal(e.getMessage());
            throw e;
        }
    }
}
