package org.bitbucket.iamkenos.cissnei.playground;

import com.google.common.collect.ImmutableSet;

import java.util.*;

import static java.lang.String.format;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Main {

    /* ********************************************
     * SETS, MAPS
     * ********************************************/
    public static void main1(String[] args) {
        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";


        printSection("SETS");
        /* ********************************************************
         * SETS
         * ********************************************************/
        //LINKED HASH SET: CONSTRUCTED BASED ON INSERTION ORDER
        Set<String> set1 = new LinkedHashSet<>();
        System.out.println(format("Adding %s: %s", e, set1.add(e)));
        System.out.println(format("Adding %s: %s", a, set1.add(a)));
        System.out.println(format("Adding %s: %s", a, set1.add(a)));
        System.out.println(format("Adding %s: %s", d, set1.add(d)));
        System.out.println(format("Adding %s: %s", c, set1.add(c)));
        System.out.println(format("Adding %s: %s", b, set1.add(b)));

        printCollection(set1, 1);

        //HASH SET: NO ORDERING GUARANTEED; CONSTANT TIME FOR OPERATIONS GUARANTEED
        Set<String> set2 = new HashSet<>(set1);
        set2.add("1");
        set2.add("{");
        printCollection(set2, 2);

        //TREE SET: CONSTRUCTED BASED ON NATURAL ORDER
        Set<String> set3 = new TreeSet<>(set1);
        set3.add("1");
        set3.add("0");
        set3.add("{");
        printCollection(set3, 3);

        //IMMUTABLE SETS
        Set<String> set4 = set1;
        Set<String> set5 = ImmutableSet.copyOf(set1);

        set1.clear();
        printCollection(set1, 1);
        printCollection(set4, 4);
        printCollection(set5, 5);


        printSection("MAPS");
        /* ********************************************************
         * MAPS
         * ********************************************************/
        //LINKED HASH MAP: CONSTRUCTED BASED ON INSERTION ORDER
        Map<String, String> kvp1 = new LinkedHashMap<>();
        System.out.println(format("Adding %s: %s", e, kvp1.put(e, e)));
        System.out.println(format("Adding %s: %s", a, kvp1.put(a, a)));
        System.out.println(format("Adding %s: %s", a, kvp1.put(a, c)));
        System.out.println(format("Adding %s: %s", d, kvp1.put(d, d)));
        System.out.println(format("Adding %s: %s", c, kvp1.put(c, c)));
        System.out.println(format("Adding %s: %s", b, kvp1.put(b, b)));

        printMap(kvp1, 1);

        //HASH MAP: NO ORDERING GUARANTEED; CONSTANT TIME FOR OPERATIONS GUARANTEED
        Map<String, String> kvp2 = new HashMap<>(kvp1);
        kvp2.put("b", "1");
        kvp2.put("1", "{");
        printMap(kvp2, 2);

        //TREE MAP: CONSTRUCTED BASED ON NATURAL ORDER OF KEYS
        Map<String, String> kvp3 = new TreeMap<>(kvp2);
//        kvp3.put("b", "1");
//        kvp3.put("a", "3");
//        kvp3.put("1", "{");
        printMap(kvp3, 3);

        //MAP: SORTED BY VALUES
        List<Map.Entry<String, String>> kvp2Entries = new LinkedList<>(kvp2.entrySet());
        Collections.sort(kvp2Entries, new MapComparator());
        Map<String, String> kvp4 = new LinkedHashMap<>();

        kvp2Entries.forEach(entry -> kvp4.put(entry.getValue(), entry.getKey()));
        printMap(kvp4, 4);
    }

    private static void printSection(String section) {
        System.out.println("\n********************************************************");
        System.out.println(section);
        System.out.println("********************************************************");
    }

    private static <T> void printCollection(Collection<T> collection, int id) {
        System.out.println("\n" + collection.getClass().getSimpleName() + ": " + id);
        collection.forEach(System.out::println);
    }

    private static <T> void printCollection(Collection<T> collection, String comment) {
        System.out.println("\n" + collection.getClass().getSimpleName() + ": " + comment);
        collection.forEach(System.out::println);
    }

    private static <T, U> void printMap(Map<T, U> map, int id) {
        System.out.println("\n" + map.getClass().getSimpleName() + ": " + id);
        map.entrySet().forEach(entry -> System.out.println(entry.getKey() + ": " + entry.getValue()));
    }

    /* ********************************************
     * ARRAYS, LISTS
     * ********************************************/
    public static void main3(String[] args) {
        int[] intArr = {9, 4, 55, 8, 1, 2, 3, 88, 4, 0, -1};

        System.out.println("UNSORTED:");
        printIntArr(intArr);

        int[] intArr1 = intArr.clone();
        Arrays.sort(intArr1);
        System.out.println("\nSORTED: [Arrays.sort]");
        printIntArr(intArr1);

        int[] intArr2 = intArr.clone();
        System.out.println("\nSORTED: [Bubble Sort]");
        bubbleSort(intArr2);
        printIntArr(intArr2);


        String[] strArr = {"ALPHA", "!CHARLIE", "{CHARLIE", "`CHARLIE", "ECHO", "BRAVO", "aLPHA", "DELTA"};

        System.out.println("UNSORTED:");
        printCollection(Arrays.asList(strArr), 0);

        String[] strArr1 = strArr.clone();
        Arrays.sort(strArr1);
        System.out.println("\nSORTED: [Arrays.sort]");
        printCollection(Arrays.asList(strArr1), 1);

        String[] strArr2 = strArr.clone();
        System.out.println("\nSORTED: [Bubble Sort]");
        bubbleSort(strArr2);
        printCollection(Arrays.asList(strArr2), 2);
    }

    private static void printIntArr(int[] arr) {
        for (int i : arr) {
            System.out.println(i);
        }
    }

    private static void bubbleSort(int[] arr) {
        //int temp = 0;

        for (int foo = 0; foo < arr.length; foo++) {
            for (int bar = 1; bar < arr.length - foo; bar++) {
                int baz = bar - 1;
                if (arr[baz] > arr[bar]) {
                    int temp = arr[baz];
                    arr[baz] = arr[bar];
                    arr[bar] = temp;
                }
            }
        }
    }

    private static void bubbleSort(String[] arr) {
        String temp = "";

        for (int foo = 0; foo < arr.length; foo++) {
            for (int bar = 1; bar < arr.length - foo; bar++) {
                int baz = bar - 1;
                if (arr[baz].compareTo(arr[bar]) > 0) {
                    temp = arr[baz];
                    arr[baz] = arr[bar];
                    arr[bar] = temp;
                }
            }
        }
    }

    /* ********************************************
     * MISC
     * ********************************************/
    public static void main(String[] args) {
        int[] arr = {0, 10, -24, 56, 12, 11, 5, 7, 9};
        int sum = 16;

        for (int foo = 0; foo < arr.length; foo++) {
            int temp = arr[foo];

            for (int bar = foo + 1; bar < arr.length; bar++) {
                if (temp + arr[bar] == sum) {
                    System.out.println(format("{%s,%s}", temp, arr[bar]));
                }
            }
        }

        for (int i = 0; i < arr.length - 1; i++) {
            int curr = arr[i];
            int next = arr[i + 1];

            if (curr + next == sum) {
                System.out.println(format("{%s,%s}", curr, next));
            }
        }
    }

    private static class MapComparator implements Comparator<Map.Entry<String, String>> {
        @Override
        public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2) {
            return o1.getValue().compareTo(o2.getValue());
        }
    }
}
