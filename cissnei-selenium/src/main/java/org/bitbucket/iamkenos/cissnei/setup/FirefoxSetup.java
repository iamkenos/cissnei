package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.utils.FileUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.io.File;

import static java.lang.String.format;
import static java.lang.System.setProperty;
import static org.bitbucket.iamkenos.cissnei.Commons.DEF_RESOURCES_DIR;
import static org.bitbucket.iamkenos.cissnei.setup.SeleniumSetup.SELENIUM_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.FileUtils.*;
import static org.openqa.selenium.UnexpectedAlertBehaviour.*;
import static org.openqa.selenium.firefox.FirefoxDriver.SystemProperty.BROWSER_LOGFILE;
import static org.openqa.selenium.firefox.GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY;
import static org.openqa.selenium.remote.CapabilityType.*;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class FirefoxSetup extends DriverSetup {
    private FirefoxOptions options;
    private String driverDlPath;

    public FirefoxSetup() {
        setDriverOptions();
    }

    @Override
    public FirefoxOptions getDriverOptions() {
        return options;
    }

    @Override
    public void setDriverOptions() {
        Proxy proxy = new Proxy();
        options = new FirefoxOptions();

        String drBin = SELENIUM_CONFIG.getFfBin();
        String drMasterDir = SELENIUM_CONFIG.getFfMasterDir();
        String drVersionDir = SELENIUM_CONFIG.getFfVersionDir();
        String drRelativePath = DEF_DRIVER_DIR + drMasterDir + drVersionDir + drBin;
        String drFullPath = DEF_RESOURCES_DIR + DEF_SELENIUM_RES_DIR + drRelativePath;
        String drDlDir = SELENIUM_CONFIG.getWdDlDir();
        File downloads = new File(DEF_RESOURCES_DIR + DEF_SELENIUM_RES_DIR + DEF_DRIVER_DIR + drMasterDir + drVersionDir + drDlDir);
        Boolean drHeadless = SELENIUM_CONFIG.getFfHeadlessEnabled();
        Boolean drAcceptSsl = SELENIUM_CONFIG.getFfAcceptSsl();
        Boolean drSupportCache = SELENIUM_CONFIG.getFfSupportCache();
        Boolean drAlertsHandled = SELENIUM_CONFIG.getFfAlertsHandled();
        String drAlertsActions = SELENIUM_CONFIG.getFfAlertsAction();
        Boolean drPromptForDl = SELENIUM_CONFIG.getFfPromptForDl();
        Boolean drAllowInsecureContent = SELENIUM_CONFIG.getFfAllowInsecureContent();
        Boolean drCustomProfileEnabled = SELENIUM_CONFIG.getFfCustomProfileEnabled();
        String drCustomProfileDir = SELENIUM_CONFIG.getFfCustomProfileDir();

        if (!isExisting(drFullPath))
            copyDriver(drRelativePath);

        setProperty(GECKO_DRIVER_EXE_PROPERTY, drFullPath);
        setProperty(BROWSER_LOGFILE, "/dev/null");

        options.setHeadless(drHeadless);

        if (wdProxyEnabled) {
            proxy.setFtpProxy(wdProxy).setHttpProxy(wdProxy).setSslProxy(wdProxy);
            options.setCapability(PROXY, proxy);
        }

        createDirs(downloads);

        if (wdClearDownloadsDirEnabled)
            FileUtils.deleteFiles(downloads);

        driverDlPath = downloads.getAbsolutePath();

        options.setCapability(ACCEPT_SSL_CERTS, drAcceptSsl);
        options.setCapability(SUPPORTS_APPLICATION_CACHE, drSupportCache);
        options.setCapability(SUPPORTS_ALERTS, drAlertsHandled);

        switch (UnexpectedAlertBehaviour.fromString(drAlertsActions)) {
            case ACCEPT:
                options.setUnhandledPromptBehaviour(ACCEPT);
                break;
            case DISMISS:
                options.setUnhandledPromptBehaviour(DISMISS);
                break;
            case IGNORE:
                options.setUnhandledPromptBehaviour(IGNORE);
                break;
            default:
                options.setUnhandledPromptBehaviour(DISMISS);
                break;
        }

        if (drCustomProfileEnabled)
            options.addArguments(format("user-data-dir=%s", drCustomProfileDir));

        options.setAcceptInsecureCerts(drAllowInsecureContent);
        options.addPreference("download.prompt_for_download", drPromptForDl);
        options.addPreference("download.default_directory", driverDlPath);
        options.addPreference("profile.default_content_settings.popups", 0);
        options.setCapability(CapabilityType.LOGGING_PREFS, getLoggingPrefs());
    }

    @Override
    public String getDriverDlPath() {
        return driverDlPath;
    }
}
