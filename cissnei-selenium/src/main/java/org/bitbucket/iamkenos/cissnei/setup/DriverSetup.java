package org.bitbucket.iamkenos.cissnei.setup;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.bitbucket.iamkenos.cissnei.Commons.DEF_RESOURCES_DIR;
import static org.bitbucket.iamkenos.cissnei.setup.SeleniumSetup.SELENIUM_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
abstract class DriverSetup {
    static final String DEF_SELENIUM_RES_DIR = "selenium/";
    static final String DEF_DRIVER_DIR = "drivers/";

    Boolean wdClearDownloadsDirEnabled = SELENIUM_CONFIG.getWdClearDlDirEnabled();
    Boolean wdProxyEnabled = SELENIUM_CONFIG.getWdProxyEnabled();
    String wdProxy = SELENIUM_CONFIG.getWdProxy();


    abstract MutableCapabilities getDriverOptions();

    abstract void setDriverOptions();

    abstract String getDriverDlPath();

    LoggingPreferences getLoggingPrefs() {
        LoggingPreferences prefs = new LoggingPreferences();
        prefs.enable(LogType.BROWSER, Level.OFF);
        prefs.enable(LogType.CLIENT, Level.OFF);
        prefs.enable(LogType.DRIVER, Level.OFF);
        prefs.enable(LogType.PERFORMANCE, Level.OFF);
        prefs.enable(LogType.PROFILER, Level.OFF);
        prefs.enable(LogType.SERVER, Level.OFF);

        return prefs;
    }

    void copyDriver(String driverBinary) {
        try {
            File file = new File(DEF_RESOURCES_DIR + DEF_SELENIUM_RES_DIR + driverBinary);
            copyInputStreamToFile(this.getClass().getResourceAsStream("/" + driverBinary), file);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
