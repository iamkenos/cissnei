package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.config.SeleniumConfig;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class SeleniumSetup {
    public static final SeleniumConfig SELENIUM_CONFIG = new SeleniumConfig();

    private SeleniumSetup() {

    }
}
