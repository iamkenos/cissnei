package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.utils.FileUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.util.HashMap;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static java.lang.String.format;
import static java.lang.System.setProperty;
import static org.bitbucket.iamkenos.cissnei.Commons.DEF_RESOURCES_DIR;
import static org.bitbucket.iamkenos.cissnei.setup.SeleniumSetup.SELENIUM_CONFIG;
import static org.openqa.selenium.UnexpectedAlertBehaviour.*;
import static org.openqa.selenium.chrome.ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY;
import static org.openqa.selenium.remote.CapabilityType.*;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ChromeSetup extends DriverSetup {
    private ChromeOptions options;
    private String driverDlPath;

    public ChromeSetup() {
        setDriverOptions();
    }

    @Override
    public String getDriverDlPath() {
        return driverDlPath;
    }

    @Override
    public ChromeOptions getDriverOptions() {
        return options;
    }

    @Override
    public void setDriverOptions() {
        HashMap<String, Object> preferences = newLinkedHashMap();
        Proxy proxy = new Proxy();
        options = new ChromeOptions();

        String drBin = SELENIUM_CONFIG.getCdBin();
        String drMasterDir = SELENIUM_CONFIG.getCdMasterDir();
        String drVersionDir = SELENIUM_CONFIG.getCdVersionDir();
        String drRelativePath = DEF_DRIVER_DIR + drMasterDir + drVersionDir + drBin;
        String drFullPath = DEF_RESOURCES_DIR + DEF_SELENIUM_RES_DIR + drRelativePath;
        String drDlDir = SELENIUM_CONFIG.getWdDlDir();
        File downloads = new File(DEF_RESOURCES_DIR + DEF_SELENIUM_RES_DIR + DEF_DRIVER_DIR + drMasterDir + drVersionDir + drDlDir);
        Boolean drHeadless = SELENIUM_CONFIG.getCdHeadlessEnabled();
        Boolean drAcceptSsl = SELENIUM_CONFIG.getCdAcceptSsl();
        Boolean drSupportCache = SELENIUM_CONFIG.getCdSupportCache();
        Boolean drAlertsHandled = SELENIUM_CONFIG.getCdAlertsHandled();
        String drAlertsActions = SELENIUM_CONFIG.getCdAlertsAction();
        Boolean drPromptForDl = SELENIUM_CONFIG.getCdPromptForDl();
        Boolean drStartMaximized = SELENIUM_CONFIG.getCdStartMaximized();
        Boolean drAllowInsecureContent = SELENIUM_CONFIG.getCdAllowInsecureContent();
        Boolean drCustomProfileEnabled = SELENIUM_CONFIG.getCdCustomProfileEnabled();
        String drCustomProfileDir = SELENIUM_CONFIG.getCdCustomProfileDir();

        if (!FileUtils.isExisting(drFullPath))
            copyDriver(drRelativePath);

        setProperty(CHROME_DRIVER_EXE_PROPERTY, drFullPath);

        options.setHeadless(drHeadless);

        if (wdProxyEnabled) {
            proxy.setFtpProxy(wdProxy).setHttpProxy(wdProxy).setSslProxy(wdProxy);
            options.setCapability(PROXY, proxy);
        }

        FileUtils.createDirs(downloads);

        if (wdClearDownloadsDirEnabled)
            FileUtils.deleteFiles(downloads);

        driverDlPath = downloads.getAbsolutePath();

        options.setCapability(ACCEPT_SSL_CERTS, drAcceptSsl);
        options.setCapability(SUPPORTS_APPLICATION_CACHE, drSupportCache);
        options.setCapability(SUPPORTS_ALERTS, drAlertsHandled);

        switch (fromString(drAlertsActions)) {
            case ACCEPT:
                options.setUnhandledPromptBehaviour(ACCEPT);
                break;
            case DISMISS:
                options.setUnhandledPromptBehaviour(DISMISS);
                break;
            case IGNORE:
                options.setUnhandledPromptBehaviour(IGNORE);
                break;
            default:
                options.setUnhandledPromptBehaviour(DISMISS);
                break;
        }

        if (drStartMaximized)
            options.addArguments("--start-maximized");
        if (drCustomProfileEnabled)
            options.addArguments(format("user-data-dir=%s", drCustomProfileDir));

        preferences.put("download.prompt_for_download", drPromptForDl);
        preferences.put("download.default_directory", driverDlPath);
        preferences.put("profile.default_content_settings.popups", 0);
        options.setAcceptInsecureCerts(drAllowInsecureContent);
        options.setExperimentalOption("prefs", preferences);
        options.setExperimentalOption("useAutomationExtension", SELENIUM_CONFIG.getCdAutomationExtensionEnabled());
    }
}

