package org.bitbucket.iamkenos.cissnei.selenium;

import com.google.common.collect.Iterables;
import cucumber.api.Scenario;
import org.awaitility.Duration;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.cissnei.setup.ChromeSetup;
import org.bitbucket.iamkenos.cissnei.setup.FirefoxSetup;
import org.bitbucket.iamkenos.cissnei.setup.SeleniumSetup;
import org.bitbucket.iamkenos.cissnei.utils.FileUtils;
import org.bitbucket.iamkenos.cissnei.utils.RandUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.io.FileUtils.readFileToByteArray;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.truncate;
import static org.awaitility.Awaitility.await;
import static org.bitbucket.iamkenos.cissnei.selenium.Driver.Browser.CHROME;
import static org.bitbucket.iamkenos.cissnei.selenium.Driver.Browser.FIREFOX;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;
import static org.openqa.selenium.OutputType.BYTES;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public class Driver implements WebDriver {
    private Scenario scenario;
    private WebDriver webDriver;
    private Wait<WebDriver> fluentWait;
    private Duration duration;
    private String screenshotFormat;
    private JavascriptExecutor jse;
    private Actions actions;
    private String parentHandle;
    private ChromeSetup chSetup;
    private FirefoxSetup ffSetup;
    private Browser browser;

    public Driver() {
        newInstance(SeleniumSetup.SELENIUM_CONFIG.getWdInstance());
    }

    public Driver(Scenario scenario) {
        this();
        this.scenario = scenario;
    }

    private void newInstance(Integer instance) {
        Integer fluentWaitTimeout = SeleniumSetup.SELENIUM_CONFIG.getWdFluentTimeout();
        browser = Browser.fromOrdinal(instance);

        try {
            if (browser == CHROME) {
                chSetup = new ChromeSetup();
                webDriver = new ChromeDriver(chSetup.getDriverOptions());
            } else if (browser == FIREFOX) {
                ffSetup = new FirefoxSetup();
                webDriver = new FirefoxDriver(ffSetup.getDriverOptions());
                if (SeleniumSetup.SELENIUM_CONFIG.getFfStartMaximized()) manage().window().maximize();
            }

            screenshotFormat = SeleniumSetup.SELENIUM_CONFIG.getWdSsFormat();
            jse = (JavascriptExecutor) webDriver;
            actions = new Actions(webDriver);

            fluentWait = new FluentWait<>(webDriver).withTimeout(ofSeconds(fluentWaitTimeout))
                    .pollingEvery(ofMillis(500))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .ignoring(UnhandledAlertException.class);
            duration = new Duration(fluentWaitTimeout, SECONDS);
            parentHandle = getWindowHandle();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public WebElement findElement(By by) {
        try {
            WebElement element = fluentWait.until(presenceOfElementLocated(by));
            focus(element);
            return element;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public WebElement findNestedElement(WebElement parent, By child) {
        try {
            WebElement element = fluentWait.until(presenceOfNestedElementLocatedBy(parent, child));
            focus(element);
            return element;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public WebElement findNestedElement(By parent, By child) {
        try {
            WebElement element = fluentWait.until(presenceOfNestedElementLocatedBy(parent, child));
            focus(element);
            return element;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public WebElement findLastElement(List<WebElement> elements) {
        return Iterables.getLast(elements);
    }

    public WebElement findLastElement(By by) {
        return findLastElement(findElements(by));
    }

    public WebElement findElementFromListWithText(List<WebElement> elements, String text) {
        String err = format("Element with text '%s' not found in list %s", text, getBy(elements));
        return elements.stream().filter(item -> getElementText(item).equals(text)).findFirst()
                .orElseThrow(() -> new NoSuchElementException(err));
    }

    public WebElement findElementFromListWithText(By by, String text) {
        return findElementFromListWithText(findElements(by), text);
    }

    public WebElement findElementFromListContainingText(List<WebElement> elements, String text) {
        String err = format("Element containing text '%s' not found in list %s", text, getBy(elements));
        return elements.stream().filter(item -> getElementText(item).contains(text)).findFirst()
                .orElseThrow(() -> new NoSuchElementException(err));
    }

    public WebElement findElementFromListContainingText(By by, String text) {
        return findElementFromListContainingText(findElements(by), text);
    }

    @Override
    public List<WebElement> findElements(By by) {
        try {
            return fluentWait.until(presenceOfAllElementsLocatedBy(by));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<WebElement> findNestedElements(WebElement parent, By child) {
        return findNestedElements(By.xpath(getXPath(parent)), child);
    }

    public List<WebElement> findNestedElements(By parent, By child) {
        try {
            return fluentWait.until(presenceOfNestedElementsLocatedBy(parent, child));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    private WebElement visibleElement(WebElement element) {
        return fluentWait.until(refreshed(visibilityOf(element)));
    }

    private WebElement clickableElement(WebElement element) {
        return fluentWait.until(refreshed(elementToBeClickable(element)));
    }

    /*
     *************************************************************************
     * ACTIONS
     *************************************************************************
     */
    @Override
    public void get(String url) {
        try {
            webDriver.get(url);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public void close() {
        try {
            webDriver.close();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public void quit() {
        try {
            switchToParentWindow();
            getWindowHandles().forEach(handle -> switchTo().window(handle).close());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        } finally {
            webDriver.quit();
        }
    }

    @Override
    public Options manage() {
        try {
            return webDriver.manage();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public Navigation navigate() {
        try {
            return webDriver.navigate();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public TargetLocator switchTo() {
        try {
            return webDriver.switchTo();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void alertAccept() {
        try {
            getAlert().accept();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void alertDismiss() {
        try {
            getAlert().dismiss();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void clear(WebElement element) {
        try {
            visibleElement(element).clear();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void clear(By by) {
        clear(findElement(by));
    }

    public void click(WebElement element) {
        try {
            clickableElement(element).click();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void click(By by) {
        click(findElement(by));
    }

    public void doubleClick(WebElement element) {
        try {
            actions.doubleClick(clickableElement(element)).perform();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void doubleClick(By by) {
        doubleClick(findElement(by));
    }

    public void dragAndDrop(WebElement source, WebElement target) {
        try {
            executeScript(Script.DRAGDROP.value, source, target);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void dragAndDrop(By source, By target) {
        dragAndDrop(findElement(source), findElement(target));
    }

    public void rightClick(WebElement element) {
        try {
            actions.contextClick(visibleElement(element)).perform();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void rightClick(By by) {
        rightClick(findElement(by));
    }

    public void clickJS(WebElement element) {
        try {
            executeScript(Script.CLICK.value, element);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void clickJS(By by) {
        clickJS(findElement(by));
    }

    private Select driverDropdown(WebElement element) {
        try {
            return new Select(visibleElement(element));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    private Select driverDropdown(By by) {
        return driverDropdown(findElement(by));
    }

    public void dropdownSelectIndex(WebElement element, int index) {
        try {
            driverDropdown(element).selectByIndex(index);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void dropdownSelectIndex(By by, int index) {
        dropdownSelectIndex(findElement(by), index);
    }

    public void dropdownSelectText(WebElement element, String text) {
        try {
            driverDropdown(element).selectByVisibleText(text);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void dropdownSelectText(By by, String text) {
        dropdownSelectText(findElement(by), text);
    }

    public void dropdownSelectValue(WebElement element, String value) {
        try {
            driverDropdown(element).selectByValue(value);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void dropdownSelectValue(By by, String text) {
        dropdownSelectValue(findElement(by), text);
    }

    public void radioSelectIndex(List<WebElement> elements, int index) {
        try {
            click(elements.get(index));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void radioSelectIndex(By by, int index) {
        radioSelectIndex(findElements(by), index);
    }

    public void radioSelectText(List<WebElement> elements, String text) {
        try {
            click(findElementFromListWithText(elements, text));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void radioSelectText(By by, String text) {
        radioSelectText(findElements(by), text);
    }

    public void refresh() {
        try {
            navigate().refresh();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public Object executeScript(String script, Object... args) {
        try {
            return jse.executeScript(script, args);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public Object executeAsyncScript(String script, Object... args) {
        try {
            return jse.executeAsyncScript(script, args);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void navigateForward() {
        try {
            navigate().forward();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void navigateBackward() {
        try {
            navigate().back();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void focus(WebElement element) {
        try {
            actions.moveToElement(visibleElement(element)).perform();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
        }
    }

    public void focus(By by) {
        focus(findElement(by));
    }

    public void removeElement(WebElement element) {
        try {
            executeScript(Script.REMOVE.value, element);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void removeElement(By by) {
        removeElement(findElement(by));
    }

    public void sendKeys(WebElement element, String keys) {
        try {
            visibleElement(element).sendKeys(keys);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void sendKeys(By by, String keys) {
        sendKeys(findElement(by), keys);
    }

    public void sendKeys(Keys... keys) {
        try {
            actions.sendKeys(keys).perform();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void sendKeys(WebElement element, Keys keys) {
        try {
            visibleElement(element).sendKeys(keys);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void sendKeys(By by, Keys keys) {
        sendKeys(findElement(by), keys);
    }

    public void setInnerHtml(WebElement element, String innerHtml) {
        try {
            executeScript(Script.SETINNERHTML.value, element, innerHtml);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void setInnerHtml(By by, String innerHtml) {
        setInnerHtml(findElement(by), innerHtml);
    }

    public void setAttribute(WebElement element, String attribute, String attributeValue) {
        try {
            String script = format(Script.SETATTRIBVAL.value, attribute, attributeValue);
            executeScript(script, element);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void setAttribute(By by, String attribute, String attributeValue) {
        setAttribute(findElement(by), attribute, attributeValue);
    }

    public void setValueJS(WebElement element, String keys) {
        try {
            String script = format(Script.SETVAL.value, keys);
            executeScript(script, element);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void setValueJS(By by, String keys) {
        setValueJS(findElement(by), keys);
    }

    public void switchToAlert() {
        try {
            switchTo().alert();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void switchToParentWindow() {
        try {
            switchTo().window(parentHandle);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void switchToWindowWithText(String text) {
        switchToWindow(1, text);
    }

    public void switchToWindowWithUrl(String url) {
        switchToWindow(2, url);
    }

    public void switchToWindowWithTitle(String title) {
        switchToWindow(3, title);
    }

    private void switchToWindow(Integer condition, String arg) {
        try {
            Boolean flag = false;
            if (fluentWait.until(numberOfWindowsToBe(2))) {
                Set<String> handles = new LinkedHashSet<>(getWindowHandles());
                handles.remove(parentHandle);

                for (String handle : handles) {
                    switchTo().window(handle);
                    switch (condition) {
                        case 1:
                            flag = isPageSourceContaining(arg);
                            break;
                        case 2:
                            flag = isPageUrlEqualTo(arg);
                            break;
                        case 3:
                            flag = isTitleEqualTo(arg);
                            break;
                        default:
                            break;
                    }

                    if (flag) {
                        manage().window().maximize();
                        break;
                    } else {
                        switchToParentWindow();
                    }
                }
            }

            if (getWindowHandle().equals(parentHandle)) {
                throw new NoSuchWindowException("Unable to find any windows or tabs to switch to.");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public void scrollToTop() {
        executeScript(Script.SCROLLUP.value);
    }

    public void scrollToBottom() {
        executeScript(Script.SCROLLDOWN.value);
    }

    public void scrollUp(Integer lines) {
        executeScript(Script.SCROLLUPLINES.value, lines);
    }

    public void scrollDown(Integer lines) {
        executeScript(Script.SCROLLDOWNLINES.value, lines);
    }

    public void scrollToElement(By by) {
        scrollToElement(findElement(by));
    }

    public void scrollToElement(WebElement element) {
        executeScript(Script.SCROLLELEM.value, visibleElement(element));
    }

    /*
     *************************************************************************
     * GET
     *************************************************************************
     */
    public Actions getActions() {
        return actions;
    }

    public String getDownloadsDir() {
        String downloads;
        if (browser == CHROME) {
            downloads = chSetup.getDriverDlPath();
        } else if (browser == FIREFOX) {
            downloads = ffSetup.getDriverDlPath();
        } else {
            downloads = SeleniumSetup.SELENIUM_CONFIG.getWdDlDir();
        }
        return downloads + "/";
    }

    @Override
    public String getCurrentUrl() {
        try {
            return webDriver.getCurrentUrl();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public String getPageSource() {
        try {
            return webDriver.getPageSource();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public String getTitle() {
        try {
            return webDriver.getTitle();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public String getWindowHandle() {
        try {
            return webDriver.getWindowHandle();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @Override
    public Set<String> getWindowHandles() {
        try {
            return webDriver.getWindowHandles();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public Alert getAlert() {
        try {
            return fluentWait.until(alertIsPresent());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getAttribute(WebElement element, String attribute) {
        try {
            return element.getAttribute(attribute);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getAttribute(By by, String attribute) {
        return getAttribute(findElement(by), attribute);
    }

    public String getInnerHtml(WebElement element) {
        try {
            return getAttribute(element, "innerHTML");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getInnerHtml(By by) {
        return getInnerHtml(findElement(by));
    }

    public String getElementText(WebElement element) {
        try {
            return element.getText();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getElementText(By by) {
        return getElementText(findElement(by));
    }

    public List<String> getElementsText(List<WebElement> elements) {
        try {
            return elements.stream().map(this::getElementText).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<String> getElementsText(By by) {
        return getElementsText(findElements(by));
    }

    public String getElementValue(WebElement element) {
        try {
            return visibleElement(element).getAttribute("value");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getElementValue(By by) {
        return getElementValue(findElement(by));
    }

    public String getDropdownSelectedText(WebElement element) {
        try {
            return (String) executeScript(Script.GETDDLSELECTEDTEXT.value, visibleElement(element));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getDropdownSelectedText(By by) {
        return getDropdownSelectedText(findElement(by));
    }

    public String getDropdownSelectedValue(WebElement element) {
        try {
            return (String) executeScript(Script.GETDDLSELECTEDVAL.value, visibleElement(element));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getDropdownSelectedValue(By by) {
        return getDropdownSelectedValue(findElement(by));
    }

    public Integer getDropdownSelectedIndex(WebElement element) {
        try {
            return (Integer) executeScript(Script.GETDDLSELECTEDINDEX.value, visibleElement(element));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public Integer getDropdownSelectedIndex(By by) {
        return getDropdownSelectedIndex(findElement(by));
    }

    public List<WebElement> getDropdownOptions(WebElement element) {
        try {
            return driverDropdown(element).getOptions();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<WebElement> getDropdownOptions(By by) {
        return getDropdownOptions(findElement(by));
    }

    public List<WebElement> getDropdownSelectedOptions(WebElement element) {
        try {
            return driverDropdown(element).getAllSelectedOptions();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<WebElement> getDropdownSelectedOptions(By by) {
        return getDropdownSelectedOptions(findElement(by));
    }

    public Integer getDropdownOptionsCount(WebElement element) {
        try {
            return getDropdownOptions(element).size();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public Integer getDropdownOptionsCount(By by) {
        return getDropdownOptionsCount(findElement(by));
    }

    public Integer getDropdownSelectedOptionsCount(WebElement element) {
        try {
            return getDropdownSelectedOptions(element).size();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public Integer getDropdownSelectedOptionsCount(By by) {
        return getDropdownSelectedOptionsCount(findElement(by));
    }

    public List<Integer> getDropdownOptionsIndexes(WebElement element) {
        try {
            List<WebElement> options = getDropdownOptions(element);

            return options.stream()
                    .map(options::indexOf)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<Integer> getDropdownOptionsIndexes(By by) {
        return getDropdownOptionsIndexes(findElement(by));
    }

    public List<String> getDropdownOptionsTexts(WebElement element) {
        try {
            return getDropdownOptions(element).stream()
                    .map(this::getElementText)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<String> getDropdownOptionsTexts(By by) {
        return getDropdownOptionsTexts(findElement(by));
    }

    public List<String> getDropdownOptionsValues(WebElement element) {
        try {
            return getDropdownOptions(element).stream()
                    .map(this::getElementValue)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<String> getDropdownOptionsValues(By by) {
        return getDropdownOptionsValues(findElement(by));
    }

    public List<Integer> getDropdownSelectedIndexes(WebElement element) {
        try {
            List<WebElement> options = getDropdownSelectedOptions(element);

            return options.stream()
                    .map(options::indexOf)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<Integer> getDropdownSelectedIndexes(By by) {
        return getDropdownSelectedIndexes(findElement(by));
    }

    public List<String> getDropdownSelectedTexts(WebElement element) {
        try {
            return getDropdownSelectedOptions(element).stream()
                    .map(this::getElementText)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<String> getDropdownSelectedTexts(By by) {
        return getDropdownSelectedTexts(findElement(by));
    }

    public List<String> getDropdownSelectedValues(WebElement element) {
        try {
            return getDropdownSelectedOptions(element).stream()
                    .map(this::getElementValue)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public List<String> getDropdownSelectedValues(By by) {
        return getDropdownSelectedValues(findElement(by));
    }

    public String getXPath(WebElement element) {
        try {
            return (String) executeScript(Script.GETXPATH.value, element);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getXPath(By by) {
        return getXPath(findElement(by));
    }

    /*
     *************************************************************************
     * DRIVER WAIT
     *************************************************************************
     */
    public void explicitWait(long milliSeconds) {
        try {
            await().atLeast(new Duration(milliSeconds, MILLISECONDS));
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    /*
     *************************************************************************
     * BOOLEANS & VALIDATIONS
     *************************************************************************
     */
    public Boolean isPageReady() {
        try {
            return fluentWait.until(expectedCondition((Boolean) jse.executeScript(Script.ISPAGEREADY.value)));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemPresent(By by) {
        try {
            findElement(by);
            return TRUE;
        } catch (Exception e) {
            return FALSE;
        }
    }

    public void thenElemPresent(By by) {
        Validate.isTrue(isElemPresent(by));
    }

    public void thenPageUrlIs(String url) {
        Validate.equals(getCurrentUrl(), url);
    }

    public Boolean isElemVisible(WebElement element) {
        try {
            return fluentWait.until(expectedCondition(element.isDisplayed()));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemVisible(By by) {
        return isElemVisible(findElement(by));
    }

    public void thenElemVisible(WebElement element) {
        Validate.isTrue(isElemVisible(element));
    }

    public void thenElemVisible(By by) {
        Validate.isTrue(isElemVisible(by));
    }

    public Boolean isElemInvisible(WebElement element) {
        try {
            return fluentWait.until(invisibilityOf(element));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemInvisible(By by) {
        try {
            return fluentWait.until(invisibilityOfElementLocated(by));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public void thenElemInvisible(WebElement element) {
        Validate.isTrue(isElemInvisible(element));
    }

    public void thenElemInvisible(By by) {
        Validate.isTrue(isElemInvisible(by));
    }

    public Boolean isElemsInvisible(List<WebElement> elements) {
        try {
            return fluentWait.until(invisibilityOfAllElements(elements));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public void thenElemsInvisible(List<WebElement> elements) {
        Validate.isTrue(isElemsInvisible(elements));
    }

    public Boolean isElemEnabled(WebElement element) {
        try {
            return fluentWait.until(expectedCondition(visibleElement(element).isEnabled()));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemEnabled(By by) {
        return isElemEnabled(findElement(by));
    }

    public void thenElemEnabled(WebElement element) {
        Validate.isTrue(isElemEnabled(element));
    }

    public void thenElemEnabled(By by) {
        Validate.isTrue(isElemEnabled(by));
    }

    public Boolean isElemDisabled(WebElement element) {
        return fluentWait.until(expectedCondition(!visibleElement(element).isEnabled()));
    }

    public Boolean isElemDisabled(By by) {
        return isElemDisabled(findElement(by));
    }

    public void thenElemDisabled(WebElement element) {
        Validate.isTrue(isElemDisabled(element));
    }

    public void thenElemDisabled(By by) {
        Validate.isTrue(isElemDisabled(by));
    }

    public Boolean isElemValueEmpty(WebElement element) {
        try {
            return fluentWait.until(expectedCondition(getElementValue(element).length() == 0));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueEmpty(By by) {
        return isElemValueEmpty(findElement(by));
    }

    public void thenElemValueEmpty(WebElement element) {
        Validate.equals(getElementValue(element).length(), 0);
    }

    public void thenElemValueEmpty(By by) {
        Validate.equals(getElementValue(by).length(), 0);
    }

    public Boolean isElemValueNotEmpty(WebElement element) {
        try {
            return fluentWait.until(expectedCondition(getElementValue(element).length() > 0));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueNotEmpty(By by) {
        return isElemValueNotEmpty(findElement(by));
    }

    public void thenElemValueNotEmpty(WebElement element) {
        Validate.isTrue(isElemValueNotEmpty(element));
    }

    public void thenElemValueNotEmpty(By by) {
        Validate.isTrue(isElemValueNotEmpty(by));
    }

    public Boolean isElemValueEqualTo(WebElement element, String value) {
        try {
            return fluentWait.until(expectedCondition(getElementValue(element).equals(value)));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueEqualTo(By by, String value) {
        return isElemValueEqualTo(findElement(by), value);
    }

    public void thenElemValueEqualTo(WebElement element, String value) {
        Validate.equals(getElementValue(element), value);
    }

    public void thenElemValueEqualTo(By by, String value) {
        Validate.equals(getElementValue(by), value);
    }

    public Boolean isElemValueContaining(WebElement element, String value) {
        try {
            return fluentWait.until(textToBePresentInElementValue(visibleElement(element), value));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueContaining(By by, String value) {
        return isElemValueContaining(findElement(by), value);
    }

    public void thenElemValueContains(WebElement element, String value) {
        Validate.isTrue(isElemValueContaining(element, value));
    }

    public void thenElemValueContains(By by, String value) {
        Validate.isTrue(isElemValueContaining(by, value));
    }

    public Boolean isElemValueLengthMoreThan(WebElement element, Integer length) {
        try {
            return fluentWait.until(expectedCondition(getElementValue(element).length() > length));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueLengthMoreThan(By by, Integer length) {
        return isElemValueLengthMoreThan(findElement(by), length);
    }

    public void thenElemValueLengthMoreThan(WebElement element, Integer length) {
        Validate.isTrue(isElemValueLengthMoreThan(element, length));
    }

    public void thenElemValueLengthMoreThan(By by, Integer length) {
        Validate.isTrue(isElemValueLengthMoreThan(by, length));
    }

    public Boolean isElemValueLengthLessThan(WebElement element, Integer length) {
        try {
            return fluentWait.until(expectedCondition(getElementValue(element).length() < length));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueLengthLessThan(By by, Integer length) {
        return isElemValueLengthLessThan(findElement(by), length);
    }

    public void thenElemValueLengthLessThan(WebElement element, Integer length) {
        Validate.isTrue(isElemValueLengthLessThan(element, length));
    }

    public void thenElemValueLengthLessThan(By by, Integer length) {
        Validate.isTrue(isElemValueLengthLessThan(by, length));
    }

    public Boolean isElemValueLengthEqualTo(WebElement element, Integer length) {
        try {
            return fluentWait.until(expectedCondition(getElementValue(element).length() == length));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemValueLengthEqualTo(By by, Integer length) {
        return isElemValueLengthEqualTo(findElement(by), length);
    }

    public void thenElemValueLengthEqualTo(WebElement element, Integer length) {
        Validate.equals(getElementValue(element).length(), length);
    }

    public Boolean isElemSelected(WebElement element) {
        try {
            return fluentWait.until(elementSelectionStateToBe(visibleElement(element), TRUE));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemSelected(By by) {
        return isElemSelected(findElement(by));
    }

    public void thenElemSelected(WebElement element) {
        Validate.isTrue(isElemSelected(element));
    }

    public void thenElemSelected(By by) {
        Validate.isTrue(isElemSelected(by));
    }

    public Boolean isElemDeselected(WebElement element) {
        try {
            return fluentWait.until(elementSelectionStateToBe(visibleElement(element), FALSE));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemDeselected(By by) {
        return isElemDeselected(findElement(by));
    }

    public void thenElemDeselected(WebElement element) {
        Validate.isTrue(isElemDeselected(element));
    }

    public void thenElemDeselected(By by) {
        Validate.isTrue(isElemDeselected(by));
    }

    public Boolean isElemAttribEqualTo(WebElement element, String attribute, String attributeValue) {
        try {
            return fluentWait.until(attributeToBe(element, attribute, attributeValue));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemAttribEqualTo(By by, String attribute, String attributeValue) {
        return isElemAttribEqualTo(findElement(by), attribute, attributeValue);
    }

    public Boolean isElemAttribContaining(WebElement element, String attribute, String attributeValue) {
        try {
            return fluentWait.until(attributeContains(element, attribute, attributeValue));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemAttribContaining(By by, String attribute, String attributeValue) {
        return isElemAttribContaining(findElement(by), attribute, attributeValue);
    }

    public void thenElemAttribContains(WebElement element, String attribute, String attributeValue) {
        Validate.isTrue(isElemAttribContaining(element, attribute, attributeValue));
    }

    public void thenElemAttribContains(By by, String attribute, String attributeValue) {
        Validate.isTrue(isElemAttribContaining(by, attribute, attributeValue));
    }

    public Boolean isElemAttribNotContaining(WebElement element, String attribute, String attributeValue) {
        try {
            return fluentWait.until(expectedCondition(!getAttribute(element, attribute).contains(attributeValue)));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemAttribNotContaining(By by, String attribute, String attributeValue) {
        return isElemAttribNotContaining(findElement(by), attribute, attributeValue);
    }

    public void thenElemAttribNotContains(WebElement element, String attribute, String attributeValue) {
        Validate.isTrue(isElemAttribNotContaining(element, attribute, attributeValue));
    }

    public void thenElemAttribNotContains(By by, String attribute, String attributeValue) {
        Validate.isTrue(isElemAttribNotContaining(by, attribute, attributeValue));
    }

    public Boolean isElemTextEqualTo(WebElement element, String text) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).equals(text)));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextEqualTo(By by, String text) {
        return isElemTextEqualTo(findElement(by), text);
    }

    public void thenElemTextEqualTo(WebElement element, String text) {
        Validate.equals(getElementText(element), text);
    }

    public void thenElemTextEqualTo(By by, String text) {
        Validate.equals(getElementText(by), text);
    }

    public Boolean isElemTextContaining(WebElement element, String text) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).contains(text)));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextContaining(By by, String text) {
        return isElemTextContaining(findElement(by), text);
    }

    public void thenElemTextContains(WebElement element, String text) {
        Validate.isTrue(isElemTextContaining(element, text));
    }

    public void thenElemTextContains(By by, String text) {
        Validate.isTrue(isElemTextContaining(by, text));
    }

    public Boolean isElemTextLengthMoreThan(WebElement element, Integer length) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).length() > length));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextLengthMoreThan(By by, Integer length) {
        return isElemTextLengthMoreThan(findElement(by), length);
    }

    public void thenElemTextLengthMoreThan(WebElement element, Integer length) {
        Validate.isTrue(isElemTextLengthMoreThan(element, length));
    }

    public void thenElemTextLengthMoreThan(By by, Integer length) {
        Validate.isTrue(isElemTextLengthMoreThan(by, length));
    }

    public Boolean isElemTextLengthLessThan(WebElement element, Integer length) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).length() < length));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextLengthLessThan(By by, Integer length) {
        return isElemTextLengthLessThan(findElement(by), length);
    }

    public void thenElemTextLengthLessThan(WebElement element, Integer length) {
        Validate.isTrue(isElemTextLengthLessThan(element, length));
    }

    public void thenElemTextLengthLessThan(By by, Integer length) {
        Validate.isTrue(isElemTextLengthLessThan(by, length));
    }

    public Boolean isElemTextLengthEqualTo(WebElement element, Integer length) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).length() == length));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextLengthEqualTo(By by, Integer length) {
        return isElemTextLengthEqualTo(findElement(by), length);
    }

    public void thenElemTextLengthEqualTo(WebElement element, Integer length) {
        Validate.equals(getElementText(element).length(), length);
    }

    public void thenElemTextLengthEqualTo(By by, Integer length) {
        Validate.equals(getElementText(by).length(), length);
    }

    public Boolean isElemTextEmpty(WebElement element) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).isEmpty()));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextEmpty(By by) {
        return isElemTextEmpty(findElement(by));
    }

    public void thenElemTextEmpty(WebElement element) {
        Validate.equals(getElementText(element), EMPTY);
    }

    public void thenElemTextEmpty(By by) {
        Validate.equals(getElementText(by), EMPTY);
    }

    public Boolean isElemTextNotEmpty(WebElement element) {
        try {
            return fluentWait.until(expectedCondition(getElementText(element).length() > 0));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isElemTextNotEmpty(By by) {
        return isElemTextNotEmpty(findElement(by));
    }

    public void thenElemTextNotEmpty(WebElement element) {
        Validate.isTrue(isElemTextNotEmpty(element));
    }

    public void thenElemTextNotEmpty(By by) {
        Validate.isTrue(isElemTextNotEmpty(by));
    }

    public Boolean isPageUrlEqualTo(String text) {
        try {
            return fluentWait.until(urlToBe(text));
        } catch (Exception e) {
            return FALSE;
        }

    }

    public Boolean isPageUrlContaining(String text) {
        try {
            return fluentWait.until(urlContains(text));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isTitleEqualTo(String text) {
        try {
            return fluentWait.until(titleIs(text));
        } catch (Exception e) {
            return FALSE;
        }

    }

    public Boolean isTitleContaining(String text) {
        try {
            return fluentWait.until(titleContains(text));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isPageSourceContaining(String text) {
        try {
            return fluentWait.until(textToBePresentInElementLocated(By.xpath("//*"), text));
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isFileDownloaded(String filename) {
        try {
            String file = getDownloadsDir() + filename;
            await().atMost(duration).until(() -> (FileUtils.isExisting(file) && !FileUtils.isLocked(file)));
            return TRUE;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return FALSE;
        }
    }

    public void thenFileDownloaded(String filename) {
        Validate.isTrue(isFileDownloaded(filename));
    }

    public Boolean isDropdownForMultiSelection(WebElement element) {
        try {
            return driverDropdown(element).isMultiple();
        } catch (Exception e) {
            return FALSE;
        }
    }

    public Boolean isDropdownForMultiSelection(By by) {
        return isDropdownForMultiSelection(findElement(by));
    }

    public void thenDropdownForMultiSelection(WebElement element) {
        Validate.isTrue(isDropdownForMultiSelection(element));
    }

    public void thenDropdownForMultiSelection(By by) {
        Validate.isTrue(isDropdownForMultiSelection(by));
    }

    public Boolean isDropdownNotForMultiSelection(WebElement element) {
        return !isDropdownForMultiSelection(element);
    }

    public Boolean isDropdownNotForMultiSelection(By by) {
        return isDropdownNotForMultiSelection(findElement(by));
    }

    public void thenDropdownNotForMultiSelection(WebElement element) {
        Validate.isTrue(isDropdownNotForMultiSelection(element));
    }

    public void thenDropdownNotForMultiSelection(By by) {
        Validate.isTrue(isDropdownNotForMultiSelection(by));
    }

    public void thenDropdownOptionsIndexAre(WebElement element, List<Integer> options) {
        Validate.equals(getDropdownOptionsIndexes(element), options);
    }

    public void thenDropdownOptionsIndexAre(By by, List<Integer> options) {
        Validate.equals(getDropdownOptionsIndexes(by), options);
    }

    public void thenDropdownOptionsTextAre(WebElement element, List<String> options) {
        Validate.equals(getDropdownOptionsTexts(element), options);
    }

    public void thenDropdownOptionsTextAre(By by, List<String> options) {
        Validate.equals(getDropdownOptionsTexts(by), options);
    }

    public void thenDropdownOptionsValuesAre(WebElement element, List<String> options) {
        Validate.equals(getDropdownOptionsValues(element), options);
    }

    public void thenDropdownOptionsValuesAre(By by, List<String> options) {
        Validate.equals(getDropdownOptionsValues(by), options);
    }

    public void thenDropdownSelectedIndexIs(WebElement element, Integer index) {
        Validate.equals(getDropdownSelectedIndex(element), index);
    }

    public void thenDropdownSelectedIndexIs(By by, Integer index) {
        Validate.equals(getDropdownSelectedIndex(by), index);
    }

    public void thenDropdownSelectedTextIs(WebElement element, String text) {
        Validate.equals(getDropdownSelectedText(element), text);
    }

    public void thenDropdownSelectedTextIs(By by, String text) {
        Validate.equals(getDropdownSelectedText(by), text);
    }

    public void thenDropdownSelectedValueIs(WebElement element, String value) {
        Validate.equals(getDropdownSelectedValue(element), value);
    }

    public void thenDropdownSelectedValueIs(By by, String value) {
        Validate.equals(getDropdownSelectedValue(by), value);
    }

    public void thenDropdownSelectedIndexAre(WebElement element, List<Integer> options) {
        Validate.equals(getDropdownSelectedIndexes(element), options);
    }

    public void thenDropdownSelectedIndexAre(By by, List<Integer> options) {
        Validate.equals(getDropdownSelectedIndexes(by), options);
    }

    public void thenDropdownSelectedTextAre(WebElement element, List<String> options) {
        Validate.equals(getDropdownSelectedTexts(element), options);
    }

    public void thenDropdownSelectedTextAre(By by, List<String> options) {
        Validate.equals(getDropdownSelectedTexts(by), options);
    }

    public void thenDropdownSelectedValuesAre(WebElement element, List<String> options) {
        Validate.equals(getDropdownSelectedValues(element), options);
    }

    public void thenDropdownSelectedValuesAre(By by, List<String> options) {
        Validate.equals(getDropdownSelectedValues(by), options);
    }

    public void thenFieldAcceptsAlpha(WebElement element, Integer length) {
        thenFieldValueAccepts(element, length, 1);
    }

    public void thenFieldAcceptsAlpha(By by, Integer length) {
        thenFieldAcceptsAlpha(findElement(by), length);
    }

    public void thenFieldAcceptsNum(WebElement element, Integer length) {
        thenFieldValueAccepts(element, length, 2);
    }

    public void thenFieldAcceptsNum(By by, Integer length) {
        thenFieldAcceptsNum(findElement(by), length);
    }

    public void thenFieldAcceptsAlphaNum(WebElement element, Integer length) {
        thenFieldValueAccepts(element, length, 3);
    }

    public void thenFieldAcceptsAlphaNum(By by, Integer length) {
        thenFieldAcceptsAlphaNum(findElement(by), length);
    }

    public void thenFieldAcceptsAlphaNumSpec(WebElement element, Integer length) {
        thenFieldValueAccepts(element, length, 4);
    }

    public void thenFieldAcceptsAlphaNumSpec(By by, Integer length) {
        thenFieldAcceptsAlphaNumSpec(findElement(by), length);
    }

    private void thenFieldValueAccepts(WebElement element, Integer length, Integer arg) {
        clear(element);
        String input;

        switch (arg) {
            case 1:
                input = RandUtils.randStringAlpha(length);
                break;
            case 2:
                input = RandUtils.randStringNum(length);
                break;
            case 3:
                input = RandUtils.randStringAlphaNum(length);
                break;
            default:
                input = RandUtils.randStringAscii(length);
                break;
        }
        sendKeys(element, input);
        thenElemValueEqualTo(element, input);
    }

    public void thenFieldValueMaxAlphaLengthIs(WebElement element, Integer max) {
        thenFieldValueMaxLengthIs(element, max, 1);
    }

    public void thenFieldValueMaxAlphaLengthIs(By by, Integer max) {
        thenFieldValueMaxAlphaLengthIs(findElement(by), max);
    }

    public void thenFieldValueMaxNumLengthIs(WebElement element, Integer max) {
        thenFieldValueMaxLengthIs(element, max, 2);
    }

    public void thenFieldValueMaxNumLengthIs(By by, Integer max) {
        thenFieldValueMaxNumLengthIs(findElement(by), max);
    }

    public void thenFieldValueMaxAlphaNumLengthIs(WebElement element, Integer max) {
        thenFieldValueMaxLengthIs(element, max, 3);
    }

    public void thenFieldValueMaxAlphaNumLengthIs(By by, Integer max) {
        thenFieldValueMaxAlphaNumLengthIs(findElement(by), max);
    }

    public void thenFieldValueMaxAlphaNumSpecLengthIs(WebElement element, Integer max) {
        thenFieldValueMaxLengthIs(element, max, 4);
    }

    public void thenFieldValueMaxAlphaNumSpecLengthIs(By by, Integer max) {
        thenFieldValueMaxAlphaNumLengthIs(findElement(by), max);
    }

    private void thenFieldValueMaxLengthIs(WebElement element, Integer max, Integer arg) {
        clear(element);

        switch (arg) {
            case 1:
                sendKeys(element, RandUtils.randStringAlpha(max + 1));
                break;
            case 2:
                sendKeys(element, RandUtils.randStringNum(max + 1));
                break;
            case 3:
                sendKeys(element, RandUtils.randStringAlphaNum(max + 1));
                break;
            default:
                sendKeys(element, RandUtils.randStringAscii(max + 1));
                break;
        }
        thenElemValueLengthEqualTo(element, max);
    }

    /*
     *************************************************************************
     * UTILITIES
     *************************************************************************
     */
    public String getSessionId() {
        try {
            return ((RemoteWebDriver) webDriver).getSessionId().toString();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return EMPTY;
        }
    }

    private void embedScreenshot(byte[] byteArr) {
        if (scenario != null) scenario.embed(byteArr, format("image/%s", screenshotFormat));
    }

    public void embedScreenshot() {
        embedScreenshot((byte[]) takeScreenshot(BYTES));
    }

    public void embedElementScreenshot(By by) {
        embedElementScreenshot(findElement(by));
    }

    public void embedElementScreenshot(WebElement element) {
        try {
            embedScreenshot(readFileToByteArray(takeElementScreenshot(element)));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void embedText(String text) {
        if (scenario != null) scenario.write(text);
    }

    public Object takeScreenshot(OutputType outputType) {
        try {
            return ((TakesScreenshot) webDriver).getScreenshotAs(outputType);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public File takeScreenshot() {
        return ((File) takeScreenshot(OutputType.FILE));
    }

    public File takeElementScreenshot(WebElement element) {
        File screenshot = takeScreenshot();
        try {
            BufferedImage screenImg = ImageIO.read(screenshot);

            Point point = element.getLocation();
            Integer elemWidth = element.getSize().getWidth();
            Integer elemHeight = element.getSize().getHeight();

            BufferedImage elementImg = screenImg.getSubimage(point.getX(), point.getY(), elemWidth, elemHeight);
            ImageIO.write(elementImg, screenshotFormat, screenshot);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return screenshot;
    }

    public File takeElementScreenshot(By by) {
        return takeElementScreenshot(findElement(by));
    }

    private String getBy(List<WebElement> elements) {
        return getBy(elements.get(0));
    }

    private String getBy(WebElement element) {
        String str = element.toString();
        return getBy(truncate(str, str.length() - 1), "->");
    }

    private String getBy(By by) {
        return getBy(by.toString(), "By.");
    }

    private String getBy(String arg, String delim) {
        try {
            return arg.substring(arg.indexOf(delim) + 3);
        } catch (Exception e) {
            LOGGER.warn("Unable to get [By] locator.");
            return EMPTY;
        }
    }

    private Function<WebDriver, Boolean> expectedCondition(Boolean condition) {
        return driver -> condition;
    }

    /*
     *************************************************************************
     * BROWSERS
     *************************************************************************
     */
    public enum Browser {
        CHROME("Chrome"),
        FIREFOX("Firefox");

        public final String value;

        Browser(final String value) {
            this.value = value;
        }

        public static final Browser fromString(String value) {
            return Arrays.stream(Browser.values()).filter(item -> item.value.equals(value)).findFirst().get();
        }

        public static Browser fromOrdinal(Integer ordinal) {
            return Browser.values()[ordinal];
        }
    }

    /*
     *************************************************************************
     * JS
     *************************************************************************
     */
    private enum Script {
        CLICK("arguments[0].click();"),
        SETVAL("arguments[0].value='%s';"),
        REMOVE("arguments[0].remove();"),
        SETINNERHTML("arguments[0].innerHTML=arguments[1];"),
        SETATTRIBVAL("arguments[0].setAttribute('%s', '%s');"),
        ISPAGEREADY("return document.readyState == 'complete'"),
        GETDDLSELECTEDTEXT("return arguments[0].options[arguments[0].selectedIndex].text;"),
        GETDDLSELECTEDVAL("return arguments[0].options[arguments[0].selectedIndex].value;"),
        GETDDLSELECTEDINDEX("return arguments[0].selectedIndex;"),
        SCROLLUPLINES("window.scrollBy(0, -arguments[0]);"),
        SCROLLDOWNLINES("window.scrollBy(0, arguments[0]);"),
        SCROLLUP("window.scrollBy(0, -document.body.scrollHeight);"),
        SCROLLDOWN("window.scrollBy(0, document.body.scrollHeight);"),
        SCROLLELEM("arguments[0].scrollIntoView(true);"),
        GETXPATH("function absoluteXPath(element) {var comp, comps = [];var parent = null; var xpath = '';var getPos = function(element) {var position = 1, curNode;"
                + "if (element.nodeType == Node.ATTRIBUTE_NODE) {return null;} for (curNode = element.previousSibling; curNode; curNode = curNode.previousSibling) {"
                + "if (curNode.nodeName == element.nodeName) {++position;}}return position;}; if (element instanceof Document) {return '/';}"
                + "for (; element && !(element instanceof Document); element = element.nodeType == Node.ATTRIBUTE_NODE ? element.ownerElement : element.parentNode) {"
                + "comp = comps[comps.length] = {};switch (element.nodeType) {case Node.TEXT_NODE: comp.name = 'text()';break;case Node.ATTRIBUTE_NODE:"
                + "comp.name = '@' + element.nodeName;break;case Node.PROCESSING_INSTRUCTION_NODE: comp.name = 'processing-instruction()';break;case Node.COMMENT_NODE:"
                + "comp.name = 'comment()';break;case Node.ELEMENT_NODE:comp.name = element.nodeName; break;}comp.position = getPos(element);}"
                + "for (var i = comps.length - 1; i >= 0; i--) {comp = comps[i]; xpath += '/' + comp.name.toLowerCase();if (comp.position !== null) {"
                + "xpath += '[' + comp.position + ']';}}return xpath;} return absoluteXPath(arguments[0]);"),
        DRAGDROP("function createEvent(typeOfEvent) {var event = document.createEvent('CustomEvent');event.initCustomEvent(typeOfEvent, true, true, null);"
                + "event.dataTransfer = {data: {},setData: function (key, value) {this.data[key] = value;},getData: function (key) {return this.data[key];}};return event;}"
                + "function dispatchEvent(element, event, transferData) {if (transferData !== undefined) {event.dataTransfer = transferData;}if (element.dispatchEvent) "
                + "{element.dispatchEvent(event);} else if (element.fireEvent) {element.fireEvent('on' + event.type, event);}}"
                + "function simulateHTML5DragAndDrop(element, target) {var dragStartEvent = createEvent('dragstart');dispatchEvent(element, dragStartEvent);"
                + "var dropEvent = createEvent('drop');dispatchEvent(target, dropEvent, dragStartEvent.dataTransfer);var dragEndEvent = createEvent('dragend');"
                + "dispatchEvent(element, dragEndEvent, dropEvent.dataTransfer);}simulateHTML5DragAndDrop(arguments[0], arguments[1])");

        private final String value;

        Script(final String value) {
            this.value = value;
        }
    }
}
