package org.bitbucket.iamkenos.cissnei.config;

import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class SeleniumConfig extends CommonsConfig {

    /*
     *************************************************************************
     * WEB DRIVER PROPERTIES
     *************************************************************************
     */
    private static final String WD_INSTANCE = "webdriver.instance";
    private static final String WD_FLUENT_TIMEOUT = "webdriver.fluent.timeout";
    private static final String WD_DL_DIR = "webdriver.downloads.dir";
    private static final String WD_CLEAR_DL_DIR_ENABLED = "webdriver.clear.downloads.dir.enabled";
    private static final String WD_PROXY_ENABLED = "webdriver.proxy.enabled";
    private static final String WD_PROXY = "webdriver.proxy";
    private static final String WD_SS_FORMAT = "webdriver.screenshot.format";
    /*
     *************************************************************************
     * CHROME DRIVER PROPERTIES
     *************************************************************************
     */
    private static final String CD_BIN = "chdriver.bin";
    private static final String CD_MASTER_DIR = "chdriver.master.dir";
    private static final String CD_VERSION_DIR = "chdriver.version.dir";
    private static final String CD_HEADLESS_ENABLED = "chdriver.headless.enabled";
    private static final String CD_ACCEPT_SSL = "chdriver.accept.ssl.alerts";
    private static final String CD_SUPPORT_CACHE = "chdriver.support.cache";
    private static final String CD_ALERTS_HANDLED = "chdriver.unexpected.alerts.handled";
    private static final String CD_ALERTS_ACTION = "chdriver.unexpected.alerts.action";
    private static final String CD_PROMPT_FOR_DL = "chdriver.prompt.for.download";
    private static final String CD_START_MAXIMIZED = "chdriver.start.maximized";
    private static final String CD_ALLOW_INSECURE_CONTENT = "chdriver.allow.insecure.content";
    private static final String CD_AUTOMATION_EXTENSION_ENABLED = "chdriver.automation.extension.enabled";
    private static final String CD_CUSTOM_PROFILE_ENABLED = "chdriver.custom.profile.enabled";
    private static final String CD_CUSTOM_PROFILE_DIR = "chdriver.custom.profile.dir";

    /*
     *************************************************************************
     * FIREFOX DRIVER PROPERTIES
     *************************************************************************
     */
    private static final String FF_BIN = "ffdriver.bin";
    private static final String FF_MASTER_DIR = "ffdriver.master.dir";
    private static final String FF_VERSION_DIR = "ffdriver.version.dir";
    private static final String FF_HEADLESS_ENABLED = "ffdriver.headless.enabled";
    private static final String FF_ACCEPT_SSL = "ffdriver.accept.ssl.alerts";
    private static final String FF_SUPPORT_CACHE = "ffdriver.support.cache";
    private static final String FF_ALERTS_HANDLED = "ffdriver.unexpected.alerts.handled";
    private static final String FF_ALERTS_ACTION = "ffdriver.unexpected.alerts.action";
    private static final String FF_PROMPT_FOR_DL = "ffdriver.prompt.for.download";
    private static final String FF_START_MAXIMIZED = "ffdriver.start.maximized";
    private static final String FF_ALLOW_INSECURE_CONTENT = "ffdriver.allow.insecure.content";
    private static final String FF_CUSTOM_PROFILE_ENABLED = "ffdriver.custom.profile.enabled";
    private static final String FF_CUSTOM_PROFILE_DIR = "ffdriver.custom.profile.dir";

    private static final String PROP_FILE = "cissnei-selenium.properties";
    private ConfigAdapter properties;

    public SeleniumConfig() {
        properties = getProperties(PROP_FILE, this.getClass());
    }

    public Integer getWdInstance() {
        return Integer.valueOf(getValue(properties, WD_INSTANCE));
    }

    public void setWdInstance(Integer value) {
        setValue(properties, WD_INSTANCE, String.valueOf(value));
    }

    public Integer getWdFluentTimeout() {
        return Integer.valueOf(getValue(properties, WD_FLUENT_TIMEOUT));
    }

    public String getWdDlDir() {
        return getValue(properties, WD_DL_DIR);
    }

    public Boolean getWdClearDlDirEnabled() {
        return Boolean.valueOf(getValue(properties, WD_CLEAR_DL_DIR_ENABLED));
    }

    public Boolean getWdProxyEnabled() {
        return Boolean.valueOf(getValue(properties, WD_PROXY_ENABLED));
    }

    public String getWdProxy() {
        return getValue(properties, WD_PROXY);
    }

    public String getWdSsFormat() {
        return getValue(properties, WD_SS_FORMAT);
    }

    public String getCdBin() {
        return getValue(properties, CD_BIN);
    }

    public String getCdMasterDir() {
        return getValue(properties, CD_MASTER_DIR);
    }

    public String getCdVersionDir() {
        return getValue(properties, CD_VERSION_DIR);
    }

    public Boolean getCdHeadlessEnabled() {
        return Boolean.valueOf(getValue(properties, CD_HEADLESS_ENABLED));
    }

    public Boolean getCdAcceptSsl() {
        return Boolean.valueOf(getValue(properties, CD_ACCEPT_SSL));
    }

    public Boolean getCdSupportCache() {
        return Boolean.valueOf(getValue(properties, CD_SUPPORT_CACHE));
    }

    public Boolean getCdAlertsHandled() {
        return Boolean.valueOf(getValue(properties, CD_ALERTS_HANDLED));
    }

    public String getCdAlertsAction() {
        return getValue(properties, CD_ALERTS_ACTION);
    }

    public Boolean getCdPromptForDl() {
        return Boolean.valueOf(getValue(properties, CD_PROMPT_FOR_DL));
    }

    public Boolean getCdStartMaximized() {
        return Boolean.valueOf(getValue(properties, CD_START_MAXIMIZED));
    }

    public Boolean getCdAllowInsecureContent() {
        return Boolean.valueOf(getValue(properties, CD_ALLOW_INSECURE_CONTENT));
    }

    public Boolean getCdAutomationExtensionEnabled() {
        return Boolean.valueOf(getValue(properties, CD_AUTOMATION_EXTENSION_ENABLED));
    }

    public Boolean getCdCustomProfileEnabled() {
        return Boolean.valueOf(getValue(properties, CD_CUSTOM_PROFILE_ENABLED));
    }

    public String getCdCustomProfileDir() {
        return getValue(properties, CD_CUSTOM_PROFILE_DIR);
    }

    public String getFfBin() {
        return getValue(properties, FF_BIN);
    }

    public String getFfMasterDir() {
        return getValue(properties, FF_MASTER_DIR);
    }

    public String getFfVersionDir() {
        return getValue(properties, FF_VERSION_DIR);
    }

    public Boolean getFfHeadlessEnabled() {
        return Boolean.valueOf(getValue(properties, FF_HEADLESS_ENABLED));
    }

    public Boolean getFfAcceptSsl() {
        return Boolean.valueOf(getValue(properties, FF_ACCEPT_SSL));
    }

    public Boolean getFfSupportCache() {
        return Boolean.valueOf(getValue(properties, FF_SUPPORT_CACHE));
    }

    public Boolean getFfAlertsHandled() {
        return Boolean.valueOf(getValue(properties, FF_ALERTS_HANDLED));
    }

    public String getFfAlertsAction() {
        return getValue(properties, FF_ALERTS_ACTION);
    }

    public Boolean getFfPromptForDl() {
        return Boolean.valueOf(getValue(properties, FF_PROMPT_FOR_DL));
    }

    public Boolean getFfStartMaximized() {
        return Boolean.valueOf(getValue(properties, FF_START_MAXIMIZED));
    }

    public Boolean getFfAllowInsecureContent() {
        return Boolean.valueOf(getValue(properties, FF_ALLOW_INSECURE_CONTENT));
    }

    public Boolean getFfCustomProfileEnabled() {
        return Boolean.valueOf(getValue(properties, FF_CUSTOM_PROFILE_ENABLED));
    }

    public String getFfCustomProfileDir() {
        return getValue(properties, FF_CUSTOM_PROFILE_DIR);
    }
}
