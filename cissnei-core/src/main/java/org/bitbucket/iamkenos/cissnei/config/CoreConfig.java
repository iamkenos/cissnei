package org.bitbucket.iamkenos.cissnei.config;

import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;

import static org.bitbucket.iamkenos.cissnei.Commons.DEF_RESOURCES_DIR;
import static org.bitbucket.iamkenos.cissnei.Commons.DEF_TEMPLATES_DIR;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class CoreConfig extends CommonsConfig {

    /*
     *************************************************************************
     * TEST DATA PROPERTIES
     *************************************************************************
     */
    private static final String TD_TEST_FILE_DIR = "data.test.file.dir";
    private static final String TD_DIR = "data.dir";
    private static final String TD_FILE = "data.file";
    private static final String TD_LABEL_COL = "data.label.col";
    private static final String TD_VALUE_COL = "data.value.col";
    private static final String TD_START_ROW = "data.start.row";
    private static final String TD_FILE_FORMAT = "data.file.format";

    private static final String PROP_FILE = "cissnei-core.properties";
    private ConfigAdapter properties;

    public CoreConfig() {
        properties = getProperties(PROP_FILE, this.getClass());
    }

    public String getTestFileDir() {
        return getValue(properties, TD_TEST_FILE_DIR);
    }

    public String getDataDir() {
        return getValue(properties, TD_DIR);
    }

    public String getDataFile() {
        return getValue(properties, TD_FILE);
    }

    public void setDataFile(String value) {
        setValue(properties, TD_FILE, value);
    }

    public String getDataLabelCol() {
        return getValue(properties, TD_LABEL_COL);
    }

    public String getDataValueCol() {
        return getValue(properties, TD_VALUE_COL);
    }

    public String getDataStartRow() {
        return getValue(properties, TD_START_ROW);
    }

    public String getDataFileFormat() {
        return getValue(properties, TD_FILE_FORMAT);
    }

    public String copyProtoTemplate() {
        String proto = DEF_TEMPLATES_DIR + "proto/";
        String protoConf = proto + "config/";

        copyToResources(this.getClass(),
                proto + "pro-template.proto",
                protoConf + "protoc-compile.bat",
                protoConf + "read-me.txt");

        return DEF_RESOURCES_DIR + proto;
    }
}
