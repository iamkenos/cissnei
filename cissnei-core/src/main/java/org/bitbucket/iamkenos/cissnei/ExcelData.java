package org.bitbucket.iamkenos.cissnei;

import org.bitbucket.iamkenos.cissnei.adapter.ExcelAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.bitbucket.iamkenos.cissnei.Commons.DEF_RESOURCES_DIR;
import static org.bitbucket.iamkenos.cissnei.setup.CoreSetup.CORE_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.FileUtils.isExisting;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ExcelData {
    private static final String DEF_DATA_DIR = "data/";
    private static final String DEF_DATA_NAME = "LOCAL.xlsx";
    private static final String DEF_DATA_PATH = DEF_RESOURCES_DIR + DEF_DATA_DIR + DEF_DATA_NAME;
    private ExcelAdapter excelAdapter;
    private String fullPath;

    public ExcelData() throws IOException {
        final String FILE_DIR = CORE_CONFIG.getDataDir();
        final String FILE_NAME = CORE_CONFIG.getDataFile();
        final String FILE_FORMAT = CORE_CONFIG.getDataFileFormat();
        final String LABEL_COL = CORE_CONFIG.getDataLabelCol();
        final String VALUE_COL = CORE_CONFIG.getDataValueCol();
        final Integer START_ROW = Integer.parseInt(CORE_CONFIG.getDataStartRow()) - 1;
        fullPath = format("%s%s%s", FILE_DIR, FILE_NAME, FILE_FORMAT);

        copyDefaultIfInvalid();
        excelAdapter = new ExcelAdapter(new File(fullPath), LABEL_COL, VALUE_COL, START_ROW);
    }

    public ExcelData(String sheetName) throws IOException {
        this();
        excelAdapter.setWorksheet(sheetName);
    }

    public String getCellData(String cellAddress) {
        return excelAdapter.getCellData(cellAddress);
    }

    public void setCellData(String cellAddress, String value) {
        excelAdapter.setCellData(cellAddress, value);
    }

    public String getLabelData(String label) {
        return excelAdapter.getLabelData(label);
    }

    public void setLabelData(String label, String value) {
        excelAdapter.setLabelData(label, value);
    }

    private void copyDefaultIfInvalid() throws IOException {
        if (!isExisting(fullPath) && !isExisting(DEF_DATA_PATH)) {
            try {
                File file = new File(DEF_DATA_PATH);
                copyInputStreamToFile(this.getClass().getResourceAsStream("/" + DEF_DATA_DIR + DEF_DATA_NAME), file);
                throw new FileNotFoundException(format("File %s not found. Use the newly provided template: %s to create the file.", fullPath, DEF_DATA_PATH));
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
                throw e;
            }
        }
    }
}
