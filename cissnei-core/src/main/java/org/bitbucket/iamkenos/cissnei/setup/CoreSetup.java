package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.config.CoreConfig;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class CoreSetup {
    public static final CoreConfig CORE_CONFIG = new CoreConfig();
    public static final String DEF_HOOK_GLUE_ARG = "classpath:org/bitbucket/iamkenos/cissnei/runner/";

    private CoreSetup() {

    }
}
