package org.bitbucket.iamkenos.cissnei.utils;

import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Message;
import cucumber.api.DataTable;
import org.bitbucket.iamkenos.cissnei.ExcelData;

import java.lang.reflect.Field;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_UNDERSCORE;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.time.LocalDate.now;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.*;
import static org.bitbucket.iamkenos.cissnei.setup.CoreSetup.CORE_CONFIG;
import static org.bitbucket.iamkenos.cissnei.utils.DateUtils.newStringDate;
import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.CapGroupName.*;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;
import static org.bitbucket.iamkenos.cissnei.utils.RandUtils.*;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class GherkinUtils {

    public static final String TO_REPLACE = ":temp:";
    private static final String SEPARATOR = "_";
    private static final String DT_MODIFIER_SEPARATOR = "::";
    private static final String RND_GRP_WRAPPER = format("(?<%s>\\[%<s])", TO_REPLACE);
    private static final String RND_PATTERN_1 = "\\d{1,4}";
    private static final String RND_PATTERN_2 = "\\d{2}";
    private static final String RND_PATTERN_3 = "\\.";
    private static final Pattern PATTERN = getPattern();

    private GherkinUtils() {

    }

    /**
     * <p>Transforms {@code data} into another string if it matches any of the predefined
     * capturing group patterns in {@link CapGroup}. Returns the same string {@code data}
     * if there are no matches found. </p>
     * <pre>
     * GherkinUtils.transformData("{SIT_APP URL}")
     *    = String data defined in cissnei-core.properties
     *    [data.dir][data.file].[data.file.format]
     *
     *    e.g:
     *    data parallel to the 'APP URL' label under 'SIT' worksheet inside resources/data/LOCAL.xlsx
     * GherkinUtils.transformData("[ASCII_10]")                     = a random series of 10 ASCII characters: qW3rtYu!0p
     * GherkinUtils.transformData("[SPEC_10]")                      = a random series of 10 ASCII special characters: `~!@#$%^-*
     * GherkinUtils.transformData("[ALPHA_10]")                     = a random series of 10 alphabetic characters: qWErtYuiop
     * GherkinUtils.transformData("[ANUM_10]")                      = a random series of 10 alpha numeric characters: qWErtYu10P
     * GherkinUtils.transformData("[ASPEC_10]")                     = a random series of 10 alphabetic and ASCII special characters: qWe#tYu!@P
     * GherkinUtils.transformData("[NUM_10]")                       = a random series of 10 numeric characters: 1029384756
     * GherkinUtils.transformData("[NSPEC_10]")                     = a random series of 10 numeric and ASCII special characters: !@2/?*47$6
     * GherkinUtils.transformData("[DECI_10.9]")                    = a random series of 10 integral characters and 9 fractional characters separated by .: 1029384756.029384756
     * GherkinUtils.transformData("[EMAIL_10]")                     = a random series of 10 alpha numeric characters appended by @email.com: qWErtYu10P@email.com
     * GherkinUtils.transformData("[FILE_TEST.PDF]")
     *    = relative path of the file TEST.PDF residing under [data.test.file.dir] defined in cissnei-core.properties
     *    [data.test.file.dir]TEST.PDF
     *
     *    e.g:
     *    resources/test-files/TEST.PDF
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy]")              = current date in dd/MM/yyyy format
     * GherkinUtils.transformData("[DATE_dd-MMM-yyyy::1]")          = current date in dd-MMM-yyyy format +1 day
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::+1D]")         = current date in dd/MM/yyyy format +1 day
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::-1]")          = current date in dd/MM/yyyy format -1 day
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::-1D]")         = current date in dd/MM/yyyy format -1 day
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::2M]")          = current date in dd/MM/yyyy format +2 months
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::-10Y]")        = current date in dd/MM/yyyy format -10 years
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::1::FIRST]")    = first of the month of [DATE_dd/MM/yyyy::1]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::-2M::FIRST]")  = first of the month of [DATE_dd/MM/yyyy::-2M]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::3Y::FIRST]")   = first of the month of [DATE_dd/MM/yyyy::3Y]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::1::LAST]")     = end of the month of [DATE_dd/MM/yyyy::1]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::-2M::LAST]")   = end of the month of [DATE_dd/MM/yyyy::-2M]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::3Y::LAST]")    = end of the month of [DATE_dd/MM/yyyy::3Y]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::5::WKDY]")     = nearest possible weekday from [DATE_dd/MM/yyyy::5]
     * GherkinUtils.transformData("[DATE_dd/MM/yyyy::-2::WKED]")    = nearest possible weekend from [DATE_dd/MM/yyyy::-2]
     * </pre>
     *
     * @param data the {@code String} to transform
     * @return the transformed {@code String} that matched the pattern provided
     * OR the same string {@code data} if there are no matches found.
     * @see java.time.format.DateTimeFormatter
     */
    public static String transformData(String data) {
        try {
            Matcher matcher = PATTERN.matcher(data);
            if (matcher.matches()) {
                String[] parts = mid(data, 1, data.length() - 2).split(SEPARATOR);
                String left = parts[0];
                String right = parts[1];

                if (matcher.group(EXCEL.name()) != null) {
                    return new ExcelData(left).getLabelData(right);
                } else {
                    switch (CapGroupName.valueOf(left)) {
                        case EXCEL:
                            return data;
                        case ASCII:
                            return randStringAscii(Integer.valueOf(right));
                        case SPEC:
                            return randStringSpec(Integer.valueOf(right));
                        case ALPHA:
                            return randStringAlpha(Integer.valueOf(right));
                        case ANUM:
                            return randStringAlphaNum(Integer.valueOf(right));
                        case ASPEC:
                            return randStringAlphaSpec(Integer.valueOf(right));
                        case NUM:
                            return randStringNum(Integer.valueOf(right));
                        case NSPEC:
                            return randStringNumSpec(Integer.valueOf(right));
                        case DECI:
                            String[] dec = right.split(RND_PATTERN_3);
                            return randStringDecimal(Integer.valueOf(dec[0]), Integer.valueOf(dec[1]));
                        case EMAIL:
                            return randStringAlphaNum(Integer.valueOf(right)) + "@email.com";
                        case FILE:
                            return CORE_CONFIG.getTestFileDir() + right;
                        case DATE:
                            return dateFromPattern(right);
                        default:
                            return data;
                    }
                }
            } else
                return data;
        } catch (Exception e) {
            LOGGER.warn(format("Unable to transform %s. Please check logs for errors.", data));
            return data;
        }
    }

    private static DataTable transpose(DataTable table) {
        DataTable newTable = table.transpose();
        newTable.topCells().replaceAll(GherkinUtils::objectFieldDescriptor);
        return transformedDataTable(newTable);
    }

    public static String objectFieldDescriptor(String label) {
        String replaced = replaceAll(label, "/|-| ", "_");
        if (replaced.matches("([a-z]+[A-Z]+\\w+)+")) return replaced;
        else return UPPER_UNDERSCORE.to(LOWER_CAMEL, replaced);
    }

    public static Integer objectFieldIndexFromString(Class clazz, String label) throws NoSuchFieldException {
        List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
        Map<String, Integer> map = new LinkedHashMap<>();
        String descriptor = objectFieldDescriptor(label);

        fields.forEach(item -> map.put(item.getName(), fields.indexOf(item)));

        if (map.get(descriptor) != null) return map.get(descriptor);
        else throw new NoSuchFieldException(format("Unable to find %s in (%s) using the descriptor: [%s].", label,
                clazz.getSimpleName(),
                descriptor));
    }

    public static FieldDescriptor protoFieldFromString(Message message, String label) {
        return message.getDescriptorForType().findFieldByName(objectFieldDescriptor(label));
    }

    public static <T> List<T> transposeAndCast(DataTable table, Class<T> clazz) {
        return transpose(table).asList(clazz);
    }

    public static <T> List<T> transposeAndCast(DataTable table, Message prototype) {
        Message message = prototype.getDefaultInstanceForType();
        List<T> protoList = new ArrayList<>();

        transposeAsMap(table).forEach(item -> {
            Message.Builder builder = message.toBuilder();
            item.forEach((key, value) -> {
                FieldDescriptor temp = message.getDescriptorForType().findFieldByName(key);
                if (temp != null) {
                    switch (temp.getJavaType()) {
                        case BOOLEAN:
                            if (!value.equals(EMPTY))
                                builder.setField(temp, Boolean.valueOf(value));
                            break;
                        case INT:
                            builder.setField(temp, Integer.valueOf(value));
                            break;
                        case LONG:
                            builder.setField(temp, Long.valueOf(value));
                            break;
                        case FLOAT:
                            builder.setField(temp, Float.valueOf(value));
                            break;
                        case DOUBLE:
                            builder.setField(temp, Double.valueOf(value));
                            break;
                        default:
                            builder.setField(temp, value);
                            break;
                    }
                }
            });
            protoList.add((T) builder.build());
        });
        return protoList;
    }

    public static <T> T transposeAndCastOne(DataTable table, Class<T> clazz) {
        return transposeAndCast(table, clazz).get(0);
    }

    public static <T> T transposeAndCastOne(DataTable table, Message prototype) {
        return (T) transposeAndCast(table, prototype).get(0);
    }

    private static List<Map<String, String>> transposeAsMap(DataTable table) {
        return transpose(table).asMaps(String.class, String.class);
    }

    private static DataTable transformedDataTable(DataTable source) {
        List<List<String>> temp = source.raw().stream()
                .map(column -> column.stream().map(GherkinUtils::transformData).collect(Collectors.toList()))
                .collect(Collectors.toList());

        return DataTable.create(temp);
    }

    private static Pattern getPattern() {
        StringJoiner joiner = new StringJoiner("|", "^", "$");
        Arrays.asList(CapGroup.values()).forEach(item -> joiner.add(item.value));
        return compile(joiner.toString());
    }

    private static String dateFromPattern(String pattern) {
        LocalDate now = now();
        String[] mods = pattern.split(DT_MODIFIER_SEPARATOR);

        if (mods.length == 1) {
            return newStringDate(now, pattern, 0);
        } else if (mods.length == 2) {
            return newStringDate(dateFromModifier1(mods[1]), mods[0]);
        } else {
            return newStringDate(dateFromModifier2(mods[1], mods[2]), mods[0]);
        }
    }

    private static Integer offsetFromModifier1(String mod1) {
        String dmy = right(mod1, 1);
        if (isAlpha(dmy))
            return Integer.valueOf(substringBefore(mod1, dmy));
        else
            return Integer.valueOf(mod1);
    }

    private static LocalDate dateFromModifier1(String mod1) {
        String dmy = right(mod1, 1);
        if (isAlpha(dmy)) {
            switch (dmy) {
                case "D":
                    return now().plusDays(offsetFromModifier1(mod1));
                case "M":
                    return now().plusMonths(offsetFromModifier1(mod1));
                default:
                    return now().plusYears(offsetFromModifier1(mod1));
            }
        } else {
            return now().plusDays(offsetFromModifier1(mod1));
        }
    }

    private static LocalDate dateFromModifier2(String mod1, String mod2) {
        Integer offset = offsetFromModifier1(mod1);

        switch (mod2) {
            case "FIRST":
                return dateFromModifier1(mod1).with(TemporalAdjusters.firstDayOfMonth());
            case "LAST":
                return dateFromModifier1(mod1).with(TemporalAdjusters.lastDayOfMonth());
            case "WKDY":
                return nearestWeekdayWeekend(dateFromModifier1(mod1), offset, FALSE);
            case "WKED":
                return nearestWeekdayWeekend(dateFromModifier1(mod1), offset, TRUE);
            default:
                return dateFromModifier1(mod1);
        }

    }

    private static LocalDate nearestWeekdayWeekend(LocalDate date, Integer offset, Boolean isForWeekend) {
        List<DayOfWeek> weekends = newArrayList(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

        if (isForWeekend)
            while (!weekends.contains(date.plusDays(offset).getDayOfWeek())) {
                if (offset < 0) offset -= 1;
                else offset += 1;
            }
        else
            while (weekends.contains(date.plusDays(offset).getDayOfWeek())) {
                if (offset < 0) offset -= 1;
                else offset += 1;
            }
        return date.plusDays(offset);
    }

    /**
     * Enum helper for {@code GherkinUtils.transformData} consisting of several
     * capturing groups that forms a long regex pattern
     * <pre>
     * EXCEL: (?&lt;EXCEL&gt;\{[^_]+_[^_]+})
     * ASCII: (?&lt;ASCII&gt;\[ASCII_\d{1,4}])
     * SPEC: (?&lt;SPEC&gt;\[SPEC_\d{1,4}])
     * ALPHA: (?&lt;ALPHA&gt;\[ALPHA_\d{1,4}])
     * ANUM: (?&lt;ANUM&gt;\[ANUM_\d{1,4}])
     * ASPEC: (?&lt;ASPEC&gt;\[ASPEC_\d{1,4}])
     * NUM: (?&lt;NUM&gt;\[NUM_\d{1,4}])
     * NSPEC: (?&lt;NSPEC&gt;\[NSPEC_\d{1,4}])
     * DECI: (?&lt;DECI&gt;\[DECI_\d{2}\.\d{2}])
     * EMAIL: (?&lt;EMAIL&gt;\[EMAIL_\d{1,4}])
     * FILE: (?&lt;FILE&gt;\[FILE_.+[.][a-zA-Z]{3,4}])
     * DATE: (?&lt;DATE&gt;\[DATE_[^::]+(::(\+|\-)?[\d]{1,3}(D|M|Y)?)?(::(FIRST|LAST|WKDY|WKED))?])
     * </pre>
     */
    enum CapGroup {
        GRP_EXCEL(String.format("(?<%s>\\{[^%s]+%<s[^%<s]+})", EXCEL.name(), SEPARATOR)),
        GRP_ASCII(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, ASCII.name()), TO_REPLACE, ASCII.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_SPEC(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, SPEC.name()), TO_REPLACE, SPEC.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_ALPHA(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, ALPHA.name()), TO_REPLACE, ALPHA.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_ANUM(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, ANUM.name()), TO_REPLACE, ANUM.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_ASPEC(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, ASPEC.name()), TO_REPLACE, ASPEC.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_NUM(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, NUM.name()), TO_REPLACE, NUM.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_NSPEC(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, NSPEC.name()), TO_REPLACE, NSPEC.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_DECI(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, DECI.name()), TO_REPLACE, DECI.name() + SEPARATOR + RND_PATTERN_2 + RND_PATTERN_3 + RND_PATTERN_2)),
        GRP_EMAIL(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, EMAIL.name()), TO_REPLACE, EMAIL.name() + SEPARATOR + RND_PATTERN_1)),
        GRP_FILE(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, FILE.name()), TO_REPLACE, FILE.name() + SEPARATOR + ".+[.][a-zA-Z]{3,4}")),
        GRP_DATE(replace(replaceFirst(RND_GRP_WRAPPER, TO_REPLACE, DATE.name()), TO_REPLACE, DATE.name() + SEPARATOR + format("[^%s]+(%<s(\\+|\\-)?[\\d]{1,3}(D|M|Y)?)?(%<s(FIRST|LAST|WKDY|WKED))?", DT_MODIFIER_SEPARATOR)));

        final String value;

        CapGroup(final String value) {
            this.value = value;
        }
    }

    enum CapGroupName {
        EXCEL,
        ASCII,
        SPEC,
        ALPHA,
        ANUM,
        ASPEC,
        NUM,
        NSPEC,
        DECI,
        EMAIL,
        FILE,
        DATE
    }
}
