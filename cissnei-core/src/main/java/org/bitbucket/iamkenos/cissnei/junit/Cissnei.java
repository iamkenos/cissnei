package org.bitbucket.iamkenos.cissnei.junit;

import cucumber.api.junit.Cucumber;
import cucumber.runtime.Runtime;
import cucumber.runtime.RuntimeOptions;
import cucumber.runtime.io.ResourceLoader;
import org.junit.runners.model.InitializationError;

import java.io.IOException;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Cissnei extends Cucumber {
    static Runtime runtime;

    /**
     * Constructor called by JUnit.
     *
     * @param clazz the class with the @RunWith annotation.
     * @throws IOException         if there is a problem
     * @throws InitializationError if there is another problem
     */
    public Cissnei(Class clazz) throws InitializationError, IOException {
        super(clazz);
    }

    @Override
    protected Runtime createRuntime(ResourceLoader resourceLoader, ClassLoader classLoader, RuntimeOptions runtimeOptions) throws InitializationError, IOException {
        runtime = super.createRuntime(resourceLoader, classLoader, runtimeOptions);
        return runtime;
    }
}
