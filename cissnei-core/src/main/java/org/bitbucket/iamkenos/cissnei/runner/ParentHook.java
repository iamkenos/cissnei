package org.bitbucket.iamkenos.cissnei.runner;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.scenarioEnd;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.scenarioStart;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ParentHook {
    public static volatile BooleanProperty isStopped = new SimpleBooleanProperty();
    public static volatile BooleanProperty isFinished = new SimpleBooleanProperty();
    public static Scenario scenario;

    @Before
    public void beforeScenario(Scenario myScenario) {
        isFinished.setValue(FALSE);
        scenario = myScenario;
        scenarioStart(scenario);
    }

    @After
    public void afterScenario() {
        isFinished.setValue(TRUE);
        scenarioEnd(scenario);
    }
}
