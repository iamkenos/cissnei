package org.bitbucket.iamkenos.cissnei.junit;

import cucumber.api.event.TestRunFinished;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;

import static java.lang.Boolean.FALSE;
import static org.bitbucket.iamkenos.cissnei.junit.Cissnei.runtime;
import static org.bitbucket.iamkenos.cissnei.runner.ParentHook.isStopped;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class CissneiCore extends JUnitCore {
    private final RunNotifier notifier = new RunNotifier();

    @Override
    public Result run(Runner runner) {
        Result result = new Result();
        RunListener listener = result.createListener();

        notifier.addFirstListener(listener);
        isStopped.addListener((obs, old, nu) -> {
            if (nu) {
                try {
                    notifier.pleaseStop();
                    runtime.getEventBus().send(new TestRunFinished(runtime.getEventBus().getTime()));
                    LOGGER.warn("::TEST PACK STOPPED:::::::::::::::::::::::::::::::::::::::::");
                }
                catch (Exception e) {
                }
            }
        });

        try {
            notifier.fireTestRunStarted(runner.getDescription());
            runner.run(notifier);
            notifier.fireTestRunFinished(result);
        } catch (Exception e) {
        } finally {
            isStopped.setValue(FALSE);
            removeListener(listener);
        }
        return result;
    }
}
