package org.bitbucket.iamkenos.cissnei.adapter;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.apache.commons.lang3.StringUtils.remove;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class OptionsAdapter implements CucumberOptions {
    private static final String CUSTOM_OR = ",";
    private static final String CUSTOM_AND = "&&";
    private static final String[] DEFAULT_TAGS = new String[]{"not @IGNORE"};

    private boolean dryRun;
    private boolean strict;
    private String[] features;
    private String[] glue;
    private String[] tags;
    private String[] format;
    private String[] plugin;
    private boolean monochrome;
    private String[] name;
    private SnippetType snippets;
    private String[] junit;

    public OptionsAdapter(CucumberOptions options) {
        dryRun = options.dryRun();
        strict = options.strict();
        features = options.features();
        glue = options.glue();
        tags = options.tags();
        format = options.format();
        plugin = options.plugin();
        monochrome = options.monochrome();
        name = options.name();
        snippets = options.snippets();
        junit = options.junit();
    }

    @Override
    public boolean dryRun() {
        return dryRun;
    }

    @Override
    public boolean strict() {
        return strict;
    }

    @Override
    public String[] features() {
        return features;
    }

    @Override
    public String[] glue() {
        return glue;
    }

    @Override
    public String[] tags() {
        return tags;
    }

    @Override
    public String[] format() {
        return format;
    }

    @Override
    public String[] plugin() {
        return plugin;
    }

    @Override
    public boolean monochrome() {
        return monochrome;
    }

    @Override
    public String[] name() {
        return name;
    }

    @Override
    public SnippetType snippets() {
        return snippets;
    }

    @Override
    public String[] junit() {
        return junit;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return OptionsAdapter.class;
    }

    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    public void setStrict(boolean strict) {
        this.strict = strict;
    }

    public void setFeatures(String[] features) {
        this.features = features;
    }

    public void setGlue(String[] glue) {
        this.glue = glue;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public void setFormat(String[] format) {
        this.format = format;
    }

    public void setPlugin(String[] plugin) {
        this.plugin = plugin;
    }

    public void setMonochrome(boolean monochrome) {
        this.monochrome = monochrome;
    }

    public void setName(String[] name) {
        this.name = name;
    }

    public void setSnippets(SnippetType snippets) {
        this.snippets = snippets;
    }

    public void setJunit(String[] junit) {
        this.junit = junit;
    }

    /*
     *************************************************************************
     * UTILITIES
     *************************************************************************
     */

    /**
     * Used to return an array of "--tags" option by taking in a single string and transforming it based on
     * the custom AND and custom OR delimiters.
     *
     * @param tags the user provided tags
     * @return the --tags array
     * @deprecated Cucumber tagging syntax revamped as of cucumber-java 2.0.0
     * @see <a href=https://cucumber.io/blog/2017/08/29/announcing-cucumber-jvm-2-0-0>Announcing Cucumber-JVM v2.0.0</a>
     */
    @Deprecated
    public String[] formatTags(String tags) {
        if (tags.isEmpty())
            return DEFAULT_TAGS;
        else {
            if (tags.contains(CUSTOM_AND)) {
                List<String> andTags = asList(tags.split(CUSTOM_AND))
                        .stream().map(String::trim)
                        .map(item -> remove(item, SPACE))
                        .collect(Collectors.toList());

                return andTags.toArray(new String[andTags.size()]);
            } else {
                String orTags = asList(tags.split(CUSTOM_OR))
                        .stream().map(String::trim)
                        .collect(Collectors.joining(CUSTOM_OR));

                return new String[]{orTags};
            }
        }
    }
}
