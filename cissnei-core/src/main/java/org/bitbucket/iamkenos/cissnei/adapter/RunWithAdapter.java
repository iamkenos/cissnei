package org.bitbucket.iamkenos.cissnei.adapter;

import org.junit.runner.RunWith;
import org.junit.runner.Runner;

import java.lang.annotation.Annotation;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class RunWithAdapter implements RunWith {
    private Class<? extends Runner> runner;
    private Class<? extends Annotation> annotations;

    public RunWithAdapter(RunWith runWith) {
        this.runner = runWith.value();
        this.annotations = runWith.annotationType();
    }

    @Override
    public Class<? extends Runner> value() {
        return runner;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return annotations;
    }

    public void setRunner(Class<? extends Runner> clazz) {
        runner = clazz;
    }
}
