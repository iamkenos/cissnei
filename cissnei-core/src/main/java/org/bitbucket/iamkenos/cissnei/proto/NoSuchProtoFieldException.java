package org.bitbucket.iamkenos.cissnei.proto;

import com.google.protobuf.Message;

import static java.lang.String.format;
import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.objectFieldDescriptor;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class NoSuchProtoFieldException extends Exception {

    private NoSuchProtoFieldException() {
    }

    public NoSuchProtoFieldException(Message message, String field) {
        super(format("Unable to find %s in (%s) using the descriptor: [%s].", field,
                message.getDescriptorForType().getName(),
                objectFieldDescriptor(field)));
    }
}
