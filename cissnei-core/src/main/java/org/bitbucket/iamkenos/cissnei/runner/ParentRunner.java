package org.bitbucket.iamkenos.cissnei.runner;

import cucumber.api.CucumberOptions;
import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.File;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.bitbucket.iamkenos.cissnei.masterthought.ReportBuilder.BASE_DIRECTORY;
import static org.bitbucket.iamkenos.cissnei.setup.CoreSetup.DEF_HOOK_GLUE_ARG;
import static org.bitbucket.iamkenos.cissnei.setup.ReportSetup.*;
import static org.bitbucket.iamkenos.cissnei.utils.FileUtils.launchFile;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
@CucumberOptions(
        glue = {DEF_HOOK_GLUE_ARG},
        plugin = {DEF_PLUGIN_ARG}
)
public class ParentRunner {

    @BeforeClass
    public static void parentSetup() {
        LOGGER.info("::TEST PACK STARTED:::::::::::::::::::::::::::::::::::::::::");
    }

    @AfterClass
    public static void parentTeardown() {
        File outputDir = new File(DEF_REPORT_ROOT_DIR);

        Configuration configuration = new Configuration(outputDir, REPORT_CONFIG.getReportProjectName());
        List<String> jsonFiles = singletonList(DEF_REPORT_ROOT_DIR + BASE_DIRECTORY + DEF_JSON_FILE);

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();

        if (REPORT_CONFIG.getReportAutoLaunch())
            launchFile(DEF_REPORT_HTML_PATH);

        LOGGER.info("::TEST PACK COMPLETE::::::::::::::::::::::::::::::::::::::::");
    }
}
