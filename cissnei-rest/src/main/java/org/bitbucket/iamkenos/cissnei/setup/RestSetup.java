package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.config.RestConfig;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class RestSetup {
    public static final RestConfig REST_CONFIG = new RestConfig();

    private RestSetup() {

    }
}
