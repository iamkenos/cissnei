package org.bitbucket.iamkenos.cissnei.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import cucumber.api.Scenario;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import org.bitbucket.iamkenos.cissnei.setup.RestSetup;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.removeAll;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
final class ClientFilter implements Filter {
    private Scenario scenario;

    public ClientFilter(Scenario scenario) {
        this.scenario = scenario;
    }

    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext context) {
        if (RestSetup.REST_CONFIG.getWriteRequest())
            logRequest(requestSpec);
        Response response = context.next(requestSpec, responseSpec);
        if (RestSetup.REST_CONFIG.getWriteResponse())
            logResponse(response);
        return response;
    }

    private void logRequest(FilterableRequestSpecification requestSpec) {
        StringBuilder builder = new StringBuilder("REQUEST:\n\n");
        Map<String, String> queryParams = requestSpec.getQueryParams();
        String multiPartParams = requestSpec.getMultiPartParams().toString().replaceAll("[\\[\\]]", EMPTY).replaceAll(", ", ",");
        String body = prettyRequest(requestSpec);

        createLog(builder, "METHOD", requestSpec.getMethod(), false);
        createLog(builder, "REQUEST URL", requestSpec.getURI(), false);
        createLog(builder, "HEADERS", requestSpec.getHeaders().toString(), true);
        if (queryParams.size() > 0)
            createLog(builder, "QUERY PARAMS", removeAll(queryParams.toString(), "\\{|}"), false);
        if (!multiPartParams.isEmpty())
            createLog(builder, "MULTI PART FORM DATA PARAMS", Arrays.stream(multiPartParams.split(",")).collect(Collectors.joining("<br>")), true);
        if (body.length() > 0) createLog(builder, "BODY", body, true);

        scenarioWrite(builder.toString());
    }

    private void logResponse(Response response) {
        StringBuilder builder = new StringBuilder("RESPONSE:\n\n");

        String body = response.getBody().prettyPrint();

        createLog(builder, "STATUS", String.valueOf(response.getStatusCode()), false);
        createLog(builder, "HEADERS", response.getHeaders().toString(), true);
        if (body.length() > 0) createLog(builder, "BODY", response.getBody().prettyPrint(), true);

        scenarioWrite(builder.toString());
    }

    private void scenarioWrite(String content) {
        if (scenario != null) scenario.write(content);
    }

    private StringBuilder createLog(StringBuilder builder, String label, String value, Boolean toBreak) {
        if (!toBreak)
            builder.append(format("%s: %s%s", label, value, lineSeparator()));
        else
            builder.append(format("%s:%s%s%s%s", label, lineSeparator(), value, lineSeparator(), lineSeparator()));
        return builder;
    }

    private String prettyRequest(FilterableRequestSpecification requestSpec) {
        if (requestSpec.getBody() != null) {
            Gson json = new GsonBuilder().setPrettyPrinting().create();
            String requestBody = requestSpec.getBody();
            try {
                if (requestBody.isEmpty())
                    return requestBody;
                else
                    return json.toJson(json.fromJson(requestBody, JsonElement.class));
            } catch (Exception e) {
                return requestBody;
            }
        } else return EMPTY;
    }
}
