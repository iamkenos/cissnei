package org.bitbucket.iamkenos.cissnei.config;

import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class RestConfig extends CommonsConfig {

    /*
     *************************************************************************
     * CLIENT PROPERTIES
     *************************************************************************
     */
    private static final String C_WRITE_REQUEST = "client.write.request";
    private static final String C_WRITE_RESPONSE = "client.write.response";
    private static final String C_PROXY_ENABLED = "client.proxy.enabled";
    private static final String C_PROXY_HOST = "client.proxy.host";
    private static final String C_PROXY_PORT = "client.proxy.port";

    private static final String PROP_FILE = "cissnei-rest.properties";
    private ConfigAdapter properties;

    public RestConfig() {
        properties = getProperties(PROP_FILE, this.getClass());
    }

    public Boolean getWriteRequest() {
        return Boolean.valueOf(getValue(properties, C_WRITE_REQUEST));
    }

    public Boolean getWriteResponse() {
        return Boolean.valueOf(getValue(properties, C_WRITE_RESPONSE));
    }

    public Boolean getProxyEnabled() {
        return Boolean.valueOf(getValue(properties, C_PROXY_ENABLED));
    }

    public String getProxyHost() {
        return getValue(properties, C_PROXY_HOST);
    }

    public Integer getProxyPort() {
        return Integer.valueOf(getValue(properties, C_PROXY_PORT));
    }
}
