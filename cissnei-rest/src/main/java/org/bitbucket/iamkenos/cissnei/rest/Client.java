package org.bitbucket.iamkenos.cissnei.rest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.Scenario;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Cookies;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.MultiPartSpecification;
import io.restassured.specification.RequestSpecification;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.cissnei.setup.RestSetup;

import java.io.File;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.http.HttpStatus.*;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public class Client {
    private RequestSpecBuilder specBuilder;
    private RequestSpecification spec;
    private Scenario scenario;
    private Response response;

    public Client() {
        newInstanceConfig();

        if (RestSetup.REST_CONFIG.getProxyEnabled())
            specBuilder.setProxy(RestSetup.REST_CONFIG.getProxyHost(), RestSetup.REST_CONFIG.getProxyPort());
    }

    public Client(Scenario scenario) {
        this();
        this.scenario = scenario;
    }

    /*
     *************************************************************************
     * REQUEST CONFIG
     *************************************************************************
     */
    private RequestSpecification buildRequest() {
        RequestSpecification requestSpec = myRequest().filter(new ClientFilter(scenario));

        response = null;
        spec = null;
        return requestSpec;
    }

    private RequestSpecification myRequest() {
        if (spec == null)
            spec = RestAssured.given().spec(specBuilder.build());
        return spec;
    }

    private void newInstanceConfig() {
        specBuilder = new RequestSpecBuilder();
    }

    public void clearInstanceConfig() {
        newInstanceConfig();
    }

    public RequestSpecBuilder getInstanceConfig() {
        return specBuilder;
    }

    public Client baseUri(String baseUri) {
        myRequest().baseUri(baseUri);
        return this;
    }

    public Client basePath(String basePath) {
        myRequest().basePath(basePath);
        return this;
    }

    public Client sessionId(String sessionIdName, String sessionIdValue) {
        myRequest().sessionId(sessionIdName, sessionIdValue);
        return this;
    }

    public Client proxy(String host, int port) {
        myRequest().proxy(host, port);
        return this;
    }

    public Client multiPart(File file) {
        myRequest().multiPart(file);
        return this;
    }

    public Client multiPart(MultiPartSpecification multiPartSpecification) {
        myRequest().multiPart(multiPartSpecification);
        return this;
    }

    public Client multiPart(String controlName, String fileName, InputStream stream) {
        myRequest().multiPart(controlName, fileName, stream);
        return this;
    }

    public Client headers(Map<String, String> headerParams) {
        myRequest().headers(headerParams);
        return this;
    }

    public Client pathParams(Map<String, String> pathParams) {
        myRequest().pathParams(pathParams);
        return this;
    }

    public Client queryParams(Map<String, String> queryParames) {
        myRequest().queryParams(queryParames);
        return this;
    }

    public Client contentType(ContentType contentType) {
        myRequest().contentType(contentType);
        return this;
    }

    public Client cookies(Cookies cookies) {
        myRequest().cookies(cookies);
        return this;
    }

    public Client keyStore(KeyStore keyStore) {
        myRequest().keyStore(keyStore);
        return this;
    }

    public Client body(String body) {
        myRequest().body(body);
        return this;
    }

    /*
     *************************************************************************
     * GETTERS
     *************************************************************************
     */
    public Response response() {
        return response;
    }

    public JsonPath responseJsonPath() {
        return response.jsonPath();
    }

    public Integer responseStatus() {
        return response.statusCode();
    }

    public Headers responseHeaders() {
        return response.headers();
    }

    public ResponseBody responseBody() {
        return response.getBody();
    }

    public String responseHeadersAsString() {
        return responseHeaders().toString();
    }

    public String responseBodyAsString() {
        return responseBody().prettyPrint();
    }

    public <T> T responseBodyAsObject(Class<T> clazz) {
        return new Gson().fromJson(responseBodyAsString(), clazz);
    }

    public JsonObject responseBodyAsJsonObject() {
        return responseBodyAsObject(JsonObject.class);
    }

    public JsonArray responseBodyAsJsonArray() {
        return responseBodyAsObject(JsonArray.class);
    }

    /*
     *************************************************************************
     * OPERATIONS
     *************************************************************************
     */
    public Response delete() {
        response = buildRequest().delete();
        return response;
    }

    public Response delete(String url) {
        response = buildRequest().delete(url);
        return response;
    }

    public Response get() {
        response = buildRequest().get();
        return response;
    }

    public Response get(String url) {
        response = buildRequest().get(url);
        return response;
    }

    public Response patch() {
        response = buildRequest().patch();
        return response;
    }

    public Response patch(String url) {
        response = buildRequest().patch(url);
        return response;
    }

    public Response post() {
        response = buildRequest().post();
        return response;
    }

    public Response post(String url) {
        response = buildRequest().post(url);
        return response;
    }

    public Response put() {
        response = buildRequest().put();
        return response;
    }

    public Response put(String url) {
        response = buildRequest().put(url);
        return response;
    }

    public void embedText(String content) {
        if (scenario != null) scenario.write(content);
    }

    /*
     *************************************************************************
     * VALIDATIONS
     *************************************************************************
     */
    public Boolean isBodyContaining(String string) {
        return responseBodyAsString().contains(string);
    }

    public void thenCreated() {
        Validate.equals(responseStatus(), SC_CREATED);
    }

    public Boolean isCreated() {
        return responseStatus().equals(SC_CREATED);
    }

    public void thenOk() {
        Validate.equals(responseStatus(), SC_OK);
    }

    public Boolean isOk() {
        return responseStatus().equals(SC_OK);
    }

    public void thenAccepted() {
        Validate.equals(responseStatus(), SC_ACCEPTED);
    }

    public Boolean isAccepted() {
        return responseStatus().equals(SC_ACCEPTED);
    }

    public void thenNoContent() {
        Validate.equals(responseStatus(), SC_NO_CONTENT);
    }

    public Boolean isNoContent() {
        return responseStatus().equals(SC_NO_CONTENT);
    }

    public void thenBadRequest() {
        Validate.equals(responseStatus(), SC_BAD_REQUEST);
    }

    public Boolean isBadRequest() {
        return responseStatus().equals(SC_BAD_REQUEST);
    }

    public void thenForbidden() {
        Validate.equals(responseStatus(), SC_FORBIDDEN);
    }

    public Boolean isForbbiden() {
        return responseStatus().equals(SC_BAD_REQUEST);
    }

    public void thenNotFound() {
        Validate.equals(responseStatus(), SC_NOT_FOUND);
    }

    public Boolean isNotFound() {
        return responseStatus().equals(SC_NOT_FOUND);
    }

    public void thenNotAllowed() {
        Validate.equals(responseStatus(), SC_METHOD_NOT_ALLOWED);
    }

    public Boolean isNotAllowed() {
        return responseStatus().equals(SC_METHOD_NOT_ALLOWED);
    }

    public void thenServerError() {
        Validate.equals(responseStatus(), SC_INTERNAL_SERVER_ERROR);
    }

    public Boolean isServerError() {
        return responseStatus().equals(SC_INTERNAL_SERVER_ERROR);
    }

    public void thenUnavailable() {
        Validate.equals(responseStatus(), SC_SERVICE_UNAVAILABLE);
    }

    public Boolean isUnavailable() {
        return responseStatus().equals(SC_SERVICE_UNAVAILABLE);
    }

    public void thenBodyEmpty() {
        Validate.equals(responseBodyAsString(), EMPTY);
    }

    public Boolean isBodyEmpty() {
        return responseBodyAsString().equals(EMPTY);
    }

    public void thenBodyNotEmpty() {
        Validate.notEquals(responseBodyAsString(), EMPTY);
    }

    public Boolean isBodyNotEmpty() {
        return !responseBodyAsString().equals(EMPTY);
    }

}
