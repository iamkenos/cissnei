package org.bitbucket.iamkenos.cissnei.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class DateUtils {

    private DateUtils() {

    }

    public static String newStringDate(String format, int offset) {
        return newStringDate(LocalDate.now(), format, offset);
    }

    public static String newStringDate(LocalDate date, String format) {
        return ofPattern(format).format(date);
    }

    public static String newStringDate(LocalDate date, String format, int offset) {
        return ofPattern(format).format(date.plusDays(offset));
    }

    public static String newStringDate(String date, String format, int offset) {
        return ofPattern(format).format(newLocalDate(date, format).plusDays(offset));
    }

    public static String newStringDate(String date, DateTimeFormatter oldFormat, DateTimeFormatter newFormat) {
        return newLocalDate(date, oldFormat).format(newFormat);
    }

    public static String newStringDate(String date, String oldFormat, String newFormat) {
        return newStringDate(date, ofPattern(oldFormat), ofPattern(newFormat));
    }

    public static LocalDate newLocalDate(String date) {
        return LocalDate.parse(date);
    }

    public static LocalDate newLocalDate(String date, DateTimeFormatter format) {
        return LocalDate.parse(date, format);
    }

    public static LocalDate newLocalDate(String date, DateTimeFormatter format, int offset) {
        return LocalDate.parse(date, format).plusDays(offset);
    }

    public static LocalDate newLocalDate(String date, String format) {
        return newLocalDate(date, ofPattern(format));
    }

    public static LocalDate newLocalDate(String date, String format, int offset) {
        return newLocalDate(date, ofPattern(format), offset);
    }

    public static LocalDateTime newLocalDateTime(String date, DateTimeFormatter format) {
        return LocalDateTime.parse(date, format);
    }

    public static LocalDateTime newLocalDateTime(String format, String date) {
        return newLocalDateTime(date, ofPattern(format));
    }

    public static Long dateDayDifference(LocalDate date1, LocalDate date2) {
        return DAYS.between(date1, date2);
    }

    public static Boolean isPastDate(String date, String format) {
        LocalDate myDate = newLocalDate(date, format);
        Long difference = dateDayDifference(LocalDate.now(), myDate);

        return difference < 0;
    }

    public static Boolean isDateOfPattern(String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        formatter.setLenient(false);
        try {
            formatter.parse(date);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
