package org.bitbucket.iamkenos.cissnei;

import org.bitbucket.iamkenos.cissnei.utils.DateUtils;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.util.Comparator;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Comparator.reverseOrder;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Validate {

    private Validate() {

    }

    public static <T> void isNull(final T actual) {
        try {
            assertThat(actual).isNull();
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void isNotNull(final T actual) {
        try {
            assertThat(actual).isNotNull();
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void isTrue(final T actual) {
        equals(actual, TRUE);
    }

    public static <T> void isFalse(final T actual) {
        equals(actual, FALSE);
    }

    public static <T> void equals(final T actual, final T expected) {
        try {
            assertThat(actual).isEqualTo(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void notEquals(final T actual, final T expected) {
        try {
            assertThat(actual).isNotEqualTo(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void containsOnly(final Iterable<T> actual, final T... expected) {
        try {
            assertThat(actual).containsOnly(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void contains(final Iterable<T> actual, final T expected) {
        try {
            assertThat(actual).contains(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void notContains(final Iterable<T> actual, final T expected) {
        try {
            assertThat(actual).doesNotContain(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void contains(final Iterable<T> actual, final Iterable<T> expected) {
        try {
            assertThat(actual).containsAll(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T> void notContains(final Iterable<T> actual, final Iterable<T> expected) {
        try {
            assertThat(actual).doesNotContainAnyElementsOf(expected);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T extends Comparable<T>> void sortedAsc(final List<T> actual) {
        try {
            assertThat(actual).isSorted();
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T extends Comparable<T>> void sortedDsc(final List<T> actual) {
        try {
            assertThat(actual).isSortedAccordingTo(reverseOrder());
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static <T extends Comparable<T>> void sortedBy(final List<T> actual, Comparator comparator) {
        try {
            assertThat(actual).isSortedAccordingTo(comparator);
        } catch (AssertionError e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    public static void dateOfPattern(String date, String format) {
        equals(DateUtils.isDateOfPattern(date, format), TRUE);
    }

    public static void pastDate(String date, String format) {
        equals(DateUtils.isPastDate(date, format), TRUE);
    }

    public static void notPastDate(String date, String format) {
        equals(DateUtils.isPastDate(date, format), FALSE);
    }
}
