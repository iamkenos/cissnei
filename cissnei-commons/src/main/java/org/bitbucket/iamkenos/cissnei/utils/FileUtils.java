package org.bitbucket.iamkenos.cissnei.utils;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static java.nio.charset.Charset.defaultCharset;
import static java.nio.file.Files.deleteIfExists;
import static java.nio.file.Paths.get;
import static org.apache.commons.io.FileUtils.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class FileUtils {
    private FileUtils() {

    }

    public static boolean isExisting(File file) {
        boolean result = false;

        try {
            if (file != null) {
                result = file.exists();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean isExisting(String file) {
        return isExisting(new File(file));
    }

    public static boolean isDirectory(File path) {
        boolean result = false;

        try {
            if (isExisting(path)) {
                result = path.isDirectory();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean isDirectory(String path) {
        return isDirectory(new File(path));
    }

    public static boolean isPathWritable(Path path) {
        boolean result = false;

        try {
            result = Files.isWritable(path);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean isPathWritable(String path) {
        return isPathWritable(get(path));
    }

    public static boolean isLocked(File file) {
        boolean result = true;

        try {
            if (isExisting(file)) {
                result = !file.renameTo(file);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean isLocked(String file) {
        return isLocked(new File(file));
    }

    public static boolean createFile(File file) {
        boolean result = false;

        try {
            file.getParentFile().mkdirs();
            result = file.createNewFile();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean createFile(String file) {
        return createFile(new File(file));
    }

    public static boolean createDirs(File directory) {
        boolean result = false;

        try {
            if (!isExisting(directory)) {
                Files.createDirectories(directory.toPath());
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean createDirs(String directory) {
        return createDirs(new File(directory));
    }

    public static boolean deleteFile(File file) {
        boolean result = false;

        try {
            if (!isDirectory(file)) {
                result = deleteIfExists(file.toPath());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean deleteFile(String file) {
        return deleteFile(new File(file));
    }

    public static boolean deleteFiles(File directory) {
        boolean result = false;

        try {
            if (isDirectory(directory)) {
                cleanDirectory(directory);
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean deleteFiles(String directory) {
        return deleteFiles(new File(directory));
    }

    public static boolean deleteDir(File directory) {
        try {
            deleteDirectory(directory);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    public static boolean deleteDir(String directory) {
        return deleteDir(new File(directory));
    }

    public static boolean copyFile(File source, File dest) {
        try {
            org.apache.commons.io.FileUtils.copyFile(source, dest);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean copyFile(String source, String dest) {
        return copyFile(new File(source), new File(dest));
    }

    public static boolean copyFileTo(File source, File dest) {
        try {
            org.apache.commons.io.FileUtils.copyFileToDirectory(source, dest);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean copyFileTo(String source, String dest) {
        return copyFileTo(new File(source), new File(dest));
    }

    public static boolean copyFiles(File sourceDirectory, File destDirectory) {
        try {
            org.apache.commons.io.FileUtils.copyDirectory(sourceDirectory, destDirectory);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean copyFiles(String sourceDirectory, String destDirectory) {
        return copyFiles(new File(sourceDirectory), new File(destDirectory));
    }

    public static boolean copyDirs(File source, File dest) {
        try {
            org.apache.commons.io.FileUtils.copyDirectoryToDirectory(source, dest);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean copyDirs(String source, String dest) {
        return copyDirs(new File(source), new File(dest));
    }

    public static boolean moveFile(File source, File dest) {
        try {
            org.apache.commons.io.FileUtils.moveFile(source, dest);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean moveFile(String source, String dest) {
        return moveFile(new File(source), new File(dest));
    }

    public static boolean moveFileTo(File source, File dest) {
        try {
            org.apache.commons.io.FileUtils.moveFileToDirectory(source, dest, true);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean moveFileTo(String source, String dest) {
        return moveFileTo(new File(source), new File(dest));
    }

    public static boolean moveFiles(File sourceDirectory, File destDirectory) {
        try {
            org.apache.commons.io.FileUtils.moveDirectory(sourceDirectory, destDirectory);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean moveFiles(String sourceDirectory, String destDirectory) {
        return moveFiles(new File(sourceDirectory), new File(destDirectory));
    }

    public static boolean moveDirs(File source, File dest) {
        try {
            org.apache.commons.io.FileUtils.moveDirectoryToDirectory(source, dest, true);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean moveDirs(String source, String dest) {
        return moveDirs(new File(source), new File(dest));
    }

    public static boolean launchFile(File file) {
        boolean result = false;

        try {
            if (isExisting(file)) {
                Desktop.getDesktop().open(file);
                result = true;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    public static boolean launchFile(String file) {
        return launchFile(new File(file));
    }

    public static boolean launchLink(String uri) {
        try {
            Desktop.getDesktop().browse(new URI(uri));
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean writeToFile(File file, String content) {
        try {
            writeStringToFile(file, content, defaultCharset());
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean writeToFile(String file, String content) {
        return writeToFile(new File(file), content);
    }

    public static boolean appendToFile(File file, String content) {
        try {
            writeStringToFile(file, content, defaultCharset(), true);
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return false;
    }

    public static boolean appendToFile(String file, String content) {
        return appendToFile(new File(file), content);
    }

    public static String readFile(File file) {
        try {
            return readFileToString(file, defaultCharset());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return EMPTY;
        }
    }

    public static String readFile(String file) {
        return readFile(new File(file));
    }

    public static List<String> readFileLines(File file) {
        try {
            return readLines(file, defaultCharset());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public static List<String> readFileLines(String file) {
        return readFileLines(new File(file));
    }

    public static List<String> getFileNames(File directory) {
        try (Stream<Path> paths = Files.list(directory.toPath())) {
            return paths.filter(Files::isRegularFile)
                    .map(item -> item.toFile().getName())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public static List<String> getFileNames(String directory) {
        return getFileNames(new File(directory));
    }

    public static List<File> getFiles(File directory) {
        try (Stream<Path> paths = Files.list(directory.toPath())) {
            return paths.filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    public static List<File> getFiles(String directory) {
        return getFiles(new File(directory));
    }

    public static boolean extractZip(File source, File dest) {
        String parent = dest.getPath().concat("/");
        try (ZipInputStream inStream = new ZipInputStream(new FileInputStream(source))) {
            ZipEntry zipEntry = inStream.getNextEntry();
            byte[] buffer = new byte[1024];
            while (zipEntry != null) {
                String child = zipEntry.getName();
                File item = new File(parent.concat(child));
                createDirs(item.getParent());
                try (FileOutputStream outStream = new FileOutputStream(item)) {
                    int len;
                    while ((len = inStream.read(buffer)) > 0) {
                        outStream.write(buffer, 0, len);
                    }
                }
                zipEntry = inStream.getNextEntry();
            }
            inStream.closeEntry();
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    public static boolean extractZip(String source, String dest) {
        return extractZip(new File(source), new File(dest));
    }

    public static boolean createZip(File file, String zipName) {
        createDirs(new File(zipName).getParent());
        try (FileOutputStream outStream = new FileOutputStream(zipName)) {
            ZipOutputStream zipOut = new ZipOutputStream(outStream);
            createZip(file, file.getName(), zipName, zipOut);
            zipOut.close();
            return isExisting(zipName);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    public static boolean createZip(String file, String zipName) {
        return createZip(new File(file), zipName);
    }

    private static void createZip(File file, String zipEntry, String zipName, ZipOutputStream zipOut) {
        try {
            if (file.isHidden()) {
                return;
            } else {
                if (isDirectory(file)) {
                    Arrays.stream(Objects.requireNonNull(file.listFiles()))
                            .forEach(child -> createZip(child, zipEntry + "/" + child.getName(), zipName, zipOut));
                    return;
                }

                ZipEntry zipIn = new ZipEntry(zipEntry);
                zipOut.putNextEntry(zipIn);
                zipOut.write(readFileToByteArray(file));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
