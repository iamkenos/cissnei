package org.bitbucket.iamkenos.cissnei.utils;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class RandUtils {

    private RandUtils() {

    }

    public static String randStringAscii(int length) {
        return randString(length, asciiCharacters());
    }

    public static String randStringSpec(int length) {
        return randString(length, asciiSpecial());
    }

    public static String randStringAlpha(int length) {
        return randString(length, asciiLetters());
    }

    public static String randStringAlphaSpec(int length) {
        return randString(length, asciiLettersOrSpecial());
    }

    public static String randStringAlphaNum(int length) {
        return randString(length, asciiLettersOrDigit());
    }

    public static String randStringNum(int length) {
        return randString(length, asciiDigit());
    }

    public static String randStringNumSpec(int length) {
        return randString(length, asciiDigitOrSpecial());
    }

    public static String randStringDecimal(int lengthLeft, int lengthRight) {
        return randString(lengthLeft, asciiDigit()) + "." + randString(lengthRight, asciiDigit());
    }

    private static String randString(int size, CharSequence validChars) {
        return ThreadLocalRandom.current().ints(size, 0, validChars.length())
                .map(validChars::charAt)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private static String randString(int minSizeIncl, int maxSizeIncl, CharSequence validChars) {
        return randString(ThreadLocalRandom.current().nextInt(minSizeIncl, maxSizeIncl), validChars);
    }

    private static CharSequence asciiCharacters() {
        return IntStream.rangeClosed('!', '~')
                .collect(StringBuilder::new,StringBuilder::appendCodePoint,StringBuilder::append);
    }

    private static CharSequence asciiSpecial() {
        return asciiCharacters().toString().replaceAll("(\\d|[A-z])", EMPTY);
    }

    private static CharSequence asciiLetters() {
        return IntStream.concat(IntStream.rangeClosed('A','Z'), IntStream.rangeClosed('a','z'))
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append);
    }

    private static CharSequence asciiLettersOrSpecial() {
        return new StringBuilder(asciiLetters()).append(asciiSpecial());
    }

    private static CharSequence asciiLettersOrDigit() {
        return new StringBuilder(asciiLetters()).append(asciiDigit());
    }

    private static CharSequence asciiDigit() {
        return IntStream.rangeClosed('0', '9')
                .collect(StringBuilder::new,StringBuilder::appendCodePoint,StringBuilder::append);
    }

    private static CharSequence asciiDigitOrSpecial() {
        return new StringBuilder(asciiDigit()).append(asciiSpecial());
    }
}
