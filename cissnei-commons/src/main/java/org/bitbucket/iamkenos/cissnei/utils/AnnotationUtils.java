package org.bitbucket.iamkenos.cissnei.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class AnnotationUtils {
    private static final String ANNOTATIONS = "annotations";
    private static final String ANNOTATION_DATA = "annotationData";

    private AnnotationUtils() {
        
    }

    public static boolean isJDKLess8() {
        try {
            Class.class.getDeclaredField(ANNOTATIONS);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public static void modifyOn(Class toModify, Class<? extends Annotation> oldAnnotation, Annotation newAnnotation) {
        try {
            if (isJDKLess8()) {
                Field annotations = Class.class.getDeclaredField(ANNOTATIONS);
                annotations.setAccessible(true);

                Map<Class<? extends Annotation>, Annotation> map = (Map<Class<? extends Annotation>, Annotation>) annotations.get(toModify);
                map.put(oldAnnotation, newAnnotation);
            } else {
                Method method = Class.class.getDeclaredMethod(ANNOTATION_DATA);
                method.setAccessible(true);

                Object annotationData = method.invoke(toModify);
                Field annotations = annotationData.getClass().getDeclaredField(ANNOTATIONS);

                annotations.setAccessible(true);
                Map<Class<? extends Annotation>, Annotation> map = (Map<Class<? extends Annotation>, Annotation>) annotations.get(annotationData);
                map.put(oldAnnotation, newAnnotation);
            }
        } catch (Exception e) {
            LogUtils.LOGGER.error(String.format("Exception on modifying annotation data for %s.", toModify.getSimpleName()), e);
        }
    }
}