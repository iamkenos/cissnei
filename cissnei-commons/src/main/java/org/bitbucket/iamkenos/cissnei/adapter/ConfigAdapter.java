package org.bitbucket.iamkenos.cissnei.adapter;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ConfigAdapter {
    private File props;

    public ConfigAdapter(File props) {
        this.props = props;
    }

    public ConfigAdapter(String props) {
        this(new File(props));
    }

    public Map<String, String> getKeysAndValues() {
        Map<String, String> kvp = newLinkedHashMap();
        PropertiesConfiguration config = new PropertiesConfiguration();
        PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();

        try (InputStream input = new FileInputStream(props)) {
            layout.load(config, new InputStreamReader(input, UTF_8));

            config.getKeys().forEachRemaining(item -> kvp.put(item, config.getString(item)));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return kvp;
    }

    public String getValue(String property) {
        String propertyValue = EMPTY;
        PropertiesConfiguration config = new PropertiesConfiguration();
        PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();

        try (InputStream input = new FileInputStream(props)) {
            layout.load(config, new InputStreamReader(input, UTF_8));
            propertyValue = config.getString(property);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return propertyValue;
    }

    public List<String> getValues(String property) {
        List<String> values = new ArrayList<>();

        try {
            values = Arrays.asList(getValue(property).split(","));
            values.forEach(String::trim);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return values;
    }

    public void setValue(String property, String propertyValue) {
        PropertiesConfiguration config = new PropertiesConfiguration();
        PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout();

        try (InputStream input = new FileInputStream(props)) {
            layout.load(config, new InputStreamReader(input, UTF_8));
            config.setProperty(property, propertyValue);
            layout.save(config, new FileWriter(props, false));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
