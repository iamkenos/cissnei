package org.bitbucket.iamkenos.cissnei.utils;

import cucumber.api.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.String.format;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class LogUtils {
    public static final Logger LOGGER = LogManager.getLogger();

    private LogUtils() {

    }

    public static void scenarioStart(Scenario scenario) {
        LOGGER.info(format("STARTING TEST: %s", scenario.getName()));
    }

    public static void scenarioEnd(Scenario scenario) {
        LOGGER.info(format("ENDING TEST: [%s] %s ", scenario.getStatus().toString().toUpperCase(), scenario.getName()));
    }
}
