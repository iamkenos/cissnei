package org.bitbucket.iamkenos.cissnei.adapter;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import org.bitbucket.iamkenos.cissnei.utils.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bitbucket.iamkenos.cissnei.utils.LogUtils.LOGGER;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ExcelAdapter {
    private File file;
    private Workbook workbook;
    private Sheet worksheet;
    private int labelCol;
    private int valueCol;
    private int startRow;

    public ExcelAdapter(File file, String labelCol, String valueCol, int startRow) throws IOException {
        this.file = file;
        this.workbook = getWorkBook();
        this.labelCol = getColIndex(labelCol);
        this.valueCol = getColIndex(valueCol);
        this.startRow = startRow;
    }

    public Sheet getWorksheet() {
        return worksheet;
    }

    public void setWorksheet(String sheetName) {
        worksheet = workbook.getSheet(sheetName);

        if (worksheet == null) {
            IllegalArgumentException e = new IllegalArgumentException(format("Worksheet %s not available in %s.", sheetName, file.getName()));
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    public String getCellData(String cellAddress) {
        return focusCellAndDo(Action.GET, cellAddress);
    }

    public void setCellData(String cellAddress, String value) {
        focusCellAndDo(Action.SET, cellAddress, value);
    }

    public String getLabelData(String label) {
        return iterateAndDo(Action.GET, label);
    }

    public void setLabelData(String label, String value) {
        iterateAndDo(Action.SET, label, value);
    }

    public int getColIndex(String colString) {
        return CellReference.convertColStringToIndex(colString);
    }

    public Cell getCell(int rowNum, int colNum) {
        Row row = worksheet.getRow(rowNum);
        if (!Optional.ofNullable(row).isPresent()) {
            row = worksheet.createRow(rowNum);
        }

        Cell cell = row.getCell(colNum);
        if (!Optional.ofNullable(cell).isPresent()) {
            cell = row.createCell(colNum);
        }

        return cell;
    }

    private Workbook getWorkBook() throws IOException {
        Workbook excelWorkbook;
        String extension = file.getName().toUpperCase(ENGLISH);

        try (FileInputStream excelFile = new FileInputStream(file)) {
            if (extension.endsWith(Extension.XLSX.value) || extension.endsWith(Extension.XLSM.value)) {
                excelWorkbook = new XSSFWorkbook(excelFile);
                XSSFFormulaEvaluator.evaluateAllFormulaCells(excelWorkbook);
            } else if (extension.endsWith(Extension.XLS.value)) {
                excelWorkbook = new HSSFWorkbook(excelFile);
                HSSFFormulaEvaluator.evaluateAllFormulaCells(excelWorkbook);
            } else {
                FileFormatException e = new FileFormatException(format("%s is not a valid excel file.", file.getName()));
                LOGGER.error(e.getMessage());
                throw e;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
        return excelWorkbook;
    }

    private void saveExcelFile() {
        try (FileOutputStream excelFile = new FileOutputStream(file)) {
            workbook.write(excelFile);
            excelFile.flush();
            LOGGER.debug(format("Excel Data Updated: [%s]", file.getName()));
        } catch (Exception e) {
            if (e.getMessage().contains("being used by another process"))
                LOGGER.warn(format("The file: %s is in use. Please close the file and retry. %s", file.getName(), e));
        }
    }

    private String focusCellAndDo(Action action, String cellAddress, String... value) {
        String data = null;
        try {
            CellReference cellRef = new CellReference(cellAddress);
            Cell cell = getCell(cellRef.getRow(), cellRef.getCol());
            CellType cellTyp = cell.getCellTypeEnum();
            CellAddress cellAd = cell.getAddress();

            if (action == Action.GET) {
                data = getCellStringValue(cell, cell.getCellTypeEnum());
                LOGGER.debug(format("Getting value of [%s] (%s) as [%s]", cellAd, cellTyp, data));

            } else if (action == Action.SET) {
                setCellStringValue(cell, value[0]);
                LOGGER.debug(format("Setting value of [%s] to [%s]", cellAd, value[0]));
                FileUtils.deleteFile(file.getPath());
                saveExcelFile();

            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return data;
    }

    private String iterateAndDo(Action action, String label, String... value) {
        String data = EMPTY;
        try {
            for (int dataRow = startRow; dataRow < worksheet.getLastRowNum() + 1; dataRow++) {
                Cell lblCell = getCell(dataRow, labelCol);
                Cell valCell = getCell(dataRow, valueCol);
                CellType lblCellTyp = lblCell.getCellTypeEnum();
                CellType valCellTyp = valCell.getCellTypeEnum();
                CellAddress valCellAd = valCell.getAddress();

                if (label.equals(getCellStringValue(lblCell, lblCellTyp))) {
                    if (action == Action.GET) {
                        data = getCellStringValue(valCell, valCellTyp);
                        LOGGER.debug(format("Getting value of [%s] (%s) as [%s]", valCellAd, valCellTyp, data));
                    } else if (action == Action.SET) {
                        setCellStringValue(valCell, value[0]);
                        LOGGER.debug(format("Setting value of [%s] to [%s]", valCellAd, value[0]));
                        FileUtils.deleteFile(file.getPath());
                        saveExcelFile();
                    }
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return data;
    }

    private String getCellStringValue(Cell cell, CellType cellType) {
        String data = EMPTY;

        try {
            switch (cellType) {
                case STRING:
                    data = cell.getRichStringCellValue().toString();
                    break;
                case NUMERIC:
                    data = String.valueOf(cell.getNumericCellValue());
                    break;
                case BOOLEAN:
                    data = String.valueOf(cell.getBooleanCellValue());
                    break;
                case FORMULA:
                    data = getCellStringValue(cell, cell.getCachedFormulaResultTypeEnum());
                    break;
                case BLANK:
                case ERROR:
                case _NONE:
                    break;
            }
        } catch (Exception e) {
            LOGGER.error(format("Unable to get string value for cell %s.", cell.getAddress()), e);
        }
        return data;
    }

    private void setCellStringValue(Cell cell, String value) {
        try {
            cell.setCellValue(value);
        } catch (Exception e) {
            LOGGER.error(format("Unable to set string value for cell %s.", cell.getAddress()), e);
        }
    }

    private enum Action {
        GET,
        SET
    }

    private enum Extension {
        XLS(".XLS"),
        XLSX(".XLSX"),
        XLSM(".XLSM");

        private final String value;

        Extension(final String value) {
            this.value = value;
        }
    }
}
