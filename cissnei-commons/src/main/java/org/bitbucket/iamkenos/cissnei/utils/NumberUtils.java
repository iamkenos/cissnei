package org.bitbucket.iamkenos.cissnei.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static java.lang.Boolean.FALSE;

/**
 * @author Alexis Matunog (https://bitbucket.org/iamkenos/)
 */
public final class NumberUtils {

    private NumberUtils() {

    }

    public static BigDecimal bigDecimalStripZeros(BigDecimal toStrip) {
        return toStrip.stripTrailingZeros();
    }

    public static String bigDecimalAsStringStripZeros(String toStrip) {
        return bigDecimalAsStringStripZeros(new BigDecimal(toStrip));
    }

    public static String bigDecimalAsStringStripZeros(BigDecimal toStrip) {
        return bigDecimalStripZeros(toStrip).toPlainString();
    }

    public static BigDecimal addBigDecimal(BigDecimal... addends) {
        return addBigDecimal(FALSE, addends);
    }

    public static BigDecimal addBigDecimal(Boolean stripTrailingZeros, BigDecimal... addends) {
        BigDecimal sum = addBigDecimal(Arrays.asList(addends));
        if (stripTrailingZeros) return bigDecimalStripZeros(sum);
        else return sum;
    }

    public static BigDecimal addBigDecimal(List<BigDecimal> addends) {
        return addends.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static String addBigDecimalAsString(String... addends) {
        return addBigDecimalAsString(FALSE, addends);
    }

    public static String addBigDecimalAsString(Boolean stripTrailingZeros, String... addends) {
        BigDecimal sum = addBigDecimal(Arrays.asList(addends).stream().map(BigDecimal::new).collect(Collectors.toList()));
        if (stripTrailingZeros) return bigDecimalAsStringStripZeros(sum);
        else return sum.toPlainString();
    }

    public static BigDecimal subtractBigDecimal(BigDecimal minuend, BigDecimal subtrahend) {
        return subtractBigDecimal(FALSE, minuend, subtrahend);
    }

    public static BigDecimal subtractBigDecimal(Boolean stripTrailingZeros, BigDecimal minuend, BigDecimal subtrahend) {
        BigDecimal diff = minuend.subtract(subtrahend);
        if (stripTrailingZeros) return bigDecimalStripZeros(diff);
        else return diff;
    }

    public static String subtractBigDecimalAsString(String minuend, String subtrahend) {
        return subtractBigDecimalAsString(FALSE, minuend, subtrahend);
    }

    public static String subtractBigDecimalAsString(Boolean stripTrailingZeros, String minuend, String subtrahend) {
        BigDecimal diff = subtractBigDecimal(new BigDecimal(minuend), new BigDecimal(subtrahend));
        if (stripTrailingZeros) return bigDecimalAsStringStripZeros(diff);
        else return diff.toPlainString();
    }

    public static BigDecimal multiplyBigDecimal(BigDecimal... multipliers) {
        return multiplyBigDecimal(FALSE, multipliers);
    }

    public static BigDecimal multiplyBigDecimal(Boolean stripTrailingZeros, BigDecimal... multipliers) {
        BigDecimal product = multiplyBigDecimal(Arrays.asList(multipliers));
        if (stripTrailingZeros) return bigDecimalStripZeros(product);
        else return product;
    }

    public static BigDecimal multiplyBigDecimal(List<BigDecimal> multipliers) {
        return multipliers.stream().reduce(BigDecimal.ONE, BigDecimal::multiply);
    }

    public static String multiplyBigDecimalAsString(String... multipliers) {
        return multiplyBigDecimalAsString(FALSE, multipliers);
    }

    public static String multiplyBigDecimalAsString(Boolean stripTrailingZeros, String... multipliers) {
        BigDecimal product = multiplyBigDecimal(Arrays.asList(multipliers).stream().map(BigDecimal::new).collect(Collectors.toList()));
        if (stripTrailingZeros) return bigDecimalAsStringStripZeros(product);
        else return product.toPlainString();
    }

    public static BigDecimal removeCommas(String arg1) {
        NumberFormat num = NumberFormat.getNumberInstance(Locale.getDefault());
        ((DecimalFormat) num).setParseBigDecimal(true);
        try {
            return (BigDecimal) num.parse(arg1);
        } catch (ParseException e) {
            LogUtils.LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static String addCommas(String arg1) {
        DecimalFormat decimalFormat = new DecimalFormat("###,###.######");
        return decimalFormat.format(new BigDecimal(arg1));
    }
}
