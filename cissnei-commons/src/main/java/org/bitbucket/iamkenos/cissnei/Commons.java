package org.bitbucket.iamkenos.cissnei;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class Commons {

    public static final String DEF_RESOURCES_DIR = "resources/";
    public static final String DEF_TEMPLATES_DIR = "templates/";

    private Commons() {

    }
}
