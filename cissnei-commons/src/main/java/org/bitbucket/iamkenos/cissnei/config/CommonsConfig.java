package org.bitbucket.iamkenos.cissnei.config;

import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;
import org.bitbucket.iamkenos.cissnei.utils.FileUtils;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bitbucket.iamkenos.cissnei.Commons.DEF_RESOURCES_DIR;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
abstract class CommonsConfig {
    static final String DEF_PROP_DIR = "properties/";

    ConfigAdapter getProperties(String propertyFile, Class clazz) {
        String myProperty = DEF_RESOURCES_DIR + DEF_PROP_DIR + propertyFile;

        if (FileUtils.isExisting(myProperty)) {
            return new ConfigAdapter(myProperty);
        } else {
            try {
                File file = new File(myProperty);
                copyInputStreamToFile(clazz.getResourceAsStream("/" + DEF_PROP_DIR + propertyFile), file);
                return new ConfigAdapter(file);
            } catch (IOException e) {
                LogUtils.LOGGER.error(e.getMessage());
                return null;
            }
        }
    }

    String copyToResources(Class clazz, String... paths) {
        String filePath = EMPTY;
        for (String path : paths) {
            try {
                filePath = DEF_RESOURCES_DIR + path;
                if (!FileUtils.isExisting(filePath)) {
                    File file = new File(filePath);
                    copyInputStreamToFile(clazz.getResourceAsStream("/" + path), file);
                } else return filePath;
            } catch (IOException e) {
                LogUtils.LOGGER.error(e.getMessage());
                return EMPTY;
            }
        }
        return filePath;
    }

    String getValue(ConfigAdapter properties, String key) {
        return properties.getValue(key);
    }

    List<String> getValues(ConfigAdapter properties, String key) {
        return properties.getValues(key);
    }

    void setValue(ConfigAdapter properties, String key, String value) {
        properties.setValue(key, value);
    }
}
