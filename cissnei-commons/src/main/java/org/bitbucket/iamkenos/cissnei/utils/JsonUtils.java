package org.bitbucket.iamkenos.cissnei.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.protobuf.Message;

import static org.bitbucket.iamkenos.cissnei.utils.FileUtils.readFile;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class JsonUtils {

    private JsonUtils() {

    }

    public static <T> T jsonTemplate(String path, Class<T> clazz) {
        return new Gson().fromJson(readFile(path), clazz);
    }

    public static JsonObject protoAsJson(JsonObject template, Message source) {
        JsonObject body = template.deepCopy();

        source.getDescriptorForType().getFields().forEach(item ->
                body.add(item.getJsonName(), new Gson().toJsonTree(source.getField(item)).getAsJsonPrimitive()));

        return body;
    }

    public static String objectAsJsonString(Object source) {
        return new Gson().toJson(source);
    }

    public static JsonObject objectAsJsonObject(Object source) {
        return new Gson().toJsonTree(source).getAsJsonObject();
    }
}
