package org.bitbucket.iamkenos.cissnei.masterthought.sorting;

import org.bitbucket.iamkenos.cissnei.masterthought.json.support.TagObject;

import java.util.Comparator;

/**
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class TagObjectAlphabeticalComparator implements Comparator<TagObject> {

    @Override
    public int compare(TagObject tagObject1, TagObject tagObject2) {
        return Integer.signum(tagObject1.getName().compareTo(tagObject2.getName()));
    }
}
