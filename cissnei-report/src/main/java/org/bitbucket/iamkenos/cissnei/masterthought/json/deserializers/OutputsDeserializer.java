package org.bitbucket.iamkenos.cissnei.masterthought.json.deserializers;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Output;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class OutputsDeserializer extends CucumberJsonDeserializer<Output[]> {

    @Override
    protected Output[] deserialize(JsonNode rootNode, Configuration configuration) throws IOException {
        List<Output> outputs = new ArrayList<>();
        if (rootNode.get(0).isArray()) {
            for (JsonNode outputNode : rootNode) {
                outputs.add(getOutput(outputNode));
            }
        } else {
            outputs.add(getOutput(rootNode));
        }

        return outputs.toArray(new Output[outputs.size()]);
    }

    private Output getOutput(JsonNode outputNode) {
        List<String> messages = new ArrayList<>();
        for (JsonNode messageNode : outputNode) {
            messages.add(messageNode.asText());
        }
        return new Output(messages.toArray(new String[messages.size()]));
    }
}
