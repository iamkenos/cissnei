package org.bitbucket.iamkenos.cissnei.masterthought.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Durationable;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Status;
import org.bitbucket.iamkenos.cissnei.masterthought.utils.ReportUtils;

public final class Result implements Durationable {

    private final Status status = Status.UNDEFINED;
    @JsonProperty("error_message")
    private final String errorMessage = null;
    private final Long duration = 0L;

    public Status getStatus() {
        return status;
    }

    @Override
    public long getDuration() {
        return duration;
    }

    @Override
    public String getFormattedDuration() {
        return ReportUtils.formatDuration(duration);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public final String getErrorMessageTitle() {
        return errorMessage.split("[\\r\\n]+")[0];
    }
}
