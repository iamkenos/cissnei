package org.bitbucket.iamkenos.cissnei.masterthought.json;

public final class Row {

    private final String[] cells = new String[0];

    public String[] getCells() {
        return cells;
    }
}
