package org.bitbucket.iamkenos.cissnei.masterthought.utils;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.text.StringEscapeUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static org.apache.commons.lang3.StringUtils.removeAll;
import static org.apache.commons.lang3.StringUtils.replaceFirst;

public final class ReportUtils {

    public static final NumberFormat PERCENT_FORMATTER = NumberFormat.getPercentInstance(Locale.US);

    static {
        PERCENT_FORMATTER.setMinimumFractionDigits(2);
        PERCENT_FORMATTER.setMaximumFractionDigits(2);
    }

    private static final NumberFormat DECIMAL_FORMATTER = DecimalFormat.getInstance(Locale.US);

    static {
        DECIMAL_FORMATTER.setMinimumFractionDigits(2);
        DECIMAL_FORMATTER.setMaximumFractionDigits(2);
    }

    public static final ReportUtils INSTANCE = new ReportUtils();

    private ReportUtils() {
    }

    public static String formatDuration(long duration) {
        final String pattern = "(00)[hm]( )+";
        final List<String> hm = new ArrayList<>(asList("h ", "m "));
        String strDuration = DurationFormatUtils.formatDurationHMS(NANOSECONDS.toMillis(duration)) + "s";

        for (String str : hm) {
            strDuration = replaceFirst(strDuration, ":", str);
        }

        return removeAll(strDuration, pattern);
    }

    /**
     * Returns value converted to percentage format.
     *
     * @param value value to convert
     * @param total sum of all values
     * @return converted values including '%' character
     */
    public static String formatAsPercentage(int value, int total) {
        float average = total == 0 ? 0 : 1F * value / total;
        return PERCENT_FORMATTER.format(average);
    }

    public static String formatAsDecimal(int value, int total) {
        float average = total == 0 ? 0 : 100F * value / total;
        return DECIMAL_FORMATTER.format(average);
    }

    /**
     * Converts characters of passed string by replacing to dash (-) each character that might not be accepted as file
     * name such as / ? or &gt;.
     *
     * @param fileName sequence that should be converted
     * @return converted string
     */
    public static String toValidFileName(String fileName) {
        return StringEscapeUtils.escapeJava(fileName).replaceAll("[^\\d\\w]", "-");
    }
}