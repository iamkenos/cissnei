package org.bitbucket.iamkenos.cissnei.masterthought;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Feature;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class ReportParser {
    private final ObjectMapper mapper = new ObjectMapper();
    private final Configuration configuration;

    public ReportParser(Configuration configuration) {
        this.configuration = configuration;

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        InjectableValues values = new InjectableValues.Std().addValue(Configuration.class, configuration);
        mapper.setInjectableValues(values);
    }

    /**
     * Parsed passed files and extracts features files.
     *
     * @param jsonFiles JSON files to read
     * @return array of parsed features
     */
    public List<Feature> parseJsonFiles(List<String> jsonFiles) {
        if (jsonFiles.isEmpty()) {
            throw new ValidationException("No report file was added!");
        }

        List<Feature> featureResults = new ArrayList<>();
        for (int i = 0; i < jsonFiles.size(); i++) {
            String jsonFile = jsonFiles.get(i);
            Feature[] features = parseForFeature(jsonFile);
            LogUtils.LOGGER.info("File '{}' contain {} features", jsonFile, features.length);
            setMetadata(features, jsonFile, i);
            featureResults.addAll(Arrays.asList(features));
        }

        if (featureResults.isEmpty()) {
            throw new ValidationException("Passed files have no features!");
        }

        return featureResults;
    }

    /**
     * Reads passed file and returns parsed features.
     *
     * @param jsonFile JSON file that should be read
     * @return array of parsed features
     */
    private Feature[] parseForFeature(String jsonFile) {
        try (Reader reader = new InputStreamReader(new FileInputStream(jsonFile), StandardCharsets.UTF_8)) {
            Feature[] features = mapper.readValue(reader, Feature[].class);
            if (ArrayUtils.isEmpty(features)) {
                LogUtils.LOGGER.info("File '{}' does not contain features", jsonFile);
            }
            return features;
        } catch (JsonMappingException e) {
            throw new ValidationException(String.format("File '%s' is not proper Cucumber report!", jsonFile), e);
        } catch (IOException e) {
            throw new ValidationException(e);
        }
    }

    /**
     * Sets additional information and calculates values which should be calculated during object creation.
     */
    private void setMetadata(Feature[] features, String jsonFile, int jsonFileNo) {
        for (Feature feature : features) {
            feature.setMetaData(jsonFile, jsonFileNo, configuration);
        }
    }

    /**
     * Parses passed properties files for classifications. These classifications within each file get added to the overview-features page as metadata.
     * File and metadata order within the individual files are preserved when classifications are added.
     *
     * @param propertiesFiles property files to read
     */
    public void parseClassificationsFiles(List<String> propertiesFiles) {
        if (isNotEmpty(propertiesFiles)) {
            propertiesFiles.stream().filter(StringUtils::isNotEmpty).forEach(this::processClassificationFile);
        }
    }

    private void processClassificationFile(String file) {
        try {
            ConfigAdapter prop = new ConfigAdapter(new File(file));
            prop.getKeysAndValues().entrySet()
                    .forEach(item -> configuration.addClassifications(item.getKey(), item.getValue()));

        } catch (Exception e) {
            throw new ValidationException(String.format("File '%s' doesn't exist or the properties file is invalid!", file), e);
        }
    }
}
