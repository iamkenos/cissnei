package org.bitbucket.iamkenos.cissnei.masterthought.generators;

import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportResult;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.TagObject;

public final class TagReportPage extends AbstractPage {

    private final TagObject tagObject;

    public TagReportPage(ReportResult reportResult, Configuration configuration, TagObject tagObject) {
        super(reportResult, "reportTag.vm", configuration);
        this.tagObject = tagObject;
    }

    @Override
    public String getWebPage() {
        return tagObject.getReportFileName();
    }

    @Override
    public void prepareReport() {
        context.put("tag", tagObject);
    }

}
