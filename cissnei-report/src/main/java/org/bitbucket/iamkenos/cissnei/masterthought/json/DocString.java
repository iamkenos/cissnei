package org.bitbucket.iamkenos.cissnei.masterthought.json;

/**
 * Doc Strings are handy for specifying a larger piece of text. This is inspired from Python’s Docstring syntax.
 * <p>
 * In your step definition, there’s no need to find this text and match it in your Regexp. It will automatically be
 * passed as the last parameter in the step definition.
 */
public final class DocString {

    private final String value = null;

    public String getValue() {
        return value;
    }
}
