package org.bitbucket.iamkenos.cissnei.masterthought.json.support;

import org.bitbucket.iamkenos.cissnei.masterthought.json.Row;

/**
 * Protractor implementation uses this for the step parameter.
 *
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class Argument {

    private final Row[] rows = new Row[0];

    public Row[] getRows() {
        return rows;
    }
}
