package org.bitbucket.iamkenos.cissnei.masterthought;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.bitbucket.iamkenos.cissnei.masterthought.generators.OverviewReport;
import org.bitbucket.iamkenos.cissnei.masterthought.json.*;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Resultsable;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Status;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.StepObject;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.TagObject;
import org.bitbucket.iamkenos.cissnei.masterthought.sorting.SortingFactory;
import org.bitbucket.iamkenos.cissnei.masterthought.sorting.SortingMethod;

import java.text.SimpleDateFormat;
import java.util.*;

public final class ReportResult {

    private final List<Feature> allFeatures = new ArrayList<>();
    private final Map<String, TagObject> allTags = new TreeMap<>();
    private final Map<String, StepObject> allSteps = new TreeMap<>();

    /**
     * Time when this report was created.
     */
    private final String buildTime;
    private final SortingFactory sortingFactory;

    private final OverviewReport featuresReport = new OverviewReport();
    private final OverviewReport tagsReport = new OverviewReport();

    public ReportResult(List<Feature> features, SortingMethod sortingMethod) {
        this.buildTime = getCurrentTime();
        this.sortingFactory = new SortingFactory(sortingMethod);

        for (Feature feature : features) {
            processFeature(feature);
        }
    }

    public List<Feature> getAllFeatures() {
        return sortingFactory.sortFeatures(allFeatures);
    }

    public List<TagObject> getAllTags() {
        return sortingFactory.sortTags(allTags.values());
    }

    public List<StepObject> getAllSteps() {
        return sortingFactory.sortSteps(allSteps.values());
    }

    public Reportable getFeatureReport() {
        return featuresReport;
    }

    public Reportable getTagReport() {
        return tagsReport;
    }

    public String getBuildTime() {
        return buildTime;
    }

    private void processFeature(Feature feature) {
        allFeatures.add(feature);

        for (Element element : feature.getElements()) {
            if (element.isScenario()) {
                featuresReport.incScenarioFor(element.getStatus());

                for (Tag tag : feature.getTags()) {
                    processTag(tag, element, feature.getStatus());
                }
            }

            for (Tag tag : element.getTags()) {
                if (!ArrayUtils.contains(feature.getTags(), tag)) {
                    processTag(tag, element, element.getStatus());
                }
            }

            Step[] steps = element.getSteps();
            for (Step step : steps) {
                featuresReport.incStepsFor(step.getResult().getStatus());
                featuresReport.incDurationBy(step.getDuration());
            }
            countSteps(steps);

            countSteps(element.getBefore());
            countSteps(element.getAfter());
        }

        featuresReport.incFeaturesFor(feature.getStatus());
        tagsReport.incFeaturesFor(feature.getStatus());
    }

    private void processTag(Tag tag, Element element, Status status) {

        TagObject tagObject = addTagObject(tag.getName());

        if (tagObject.addElement(element)) {
            tagsReport.incScenarioFor(status);

            Step[] steps = element.getSteps();
            for (Step step : steps) {
                tagsReport.incStepsFor(step.getResult().getStatus());
                tagsReport.incDurationBy(step.getDuration());
            }
        }
    }

    private void countSteps(Resultsable[] steps) {
        for (Resultsable step : steps) {

            Match match = step.getMatch();
            if (match != null) {
                String methodName = match.getLocation();
                if (StringUtils.isNotEmpty(methodName)) {
                    addNewStep(step.getResult(), methodName);
                }
            }
        }
    }

    private void addNewStep(Result result, String methodName) {
        StepObject stepObject = allSteps.get(methodName);
        if (stepObject == null) {
            stepObject = new StepObject(methodName);
        }

        stepObject.addDuration(result.getDuration(), result.getStatus());
        allSteps.put(methodName, stepObject);
    }

    private TagObject addTagObject(String name) {
        TagObject tagObject = allTags.get(name);
        if (tagObject == null) {
            tagObject = new TagObject(name);
            allTags.put(tagObject.getName(), tagObject);
        }
        return tagObject;
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat("dd MMM yyyy, HH:mm").format(new Date());
    }
}
