package org.bitbucket.iamkenos.cissnei.masterthought.generators;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportBuilder;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportResult;

import java.util.List;

public final class ErrorPage extends AbstractPage {

    private final Exception exception;
    private final List<String> jsonFiles;

    public ErrorPage(ReportResult reportResult, Configuration configuration, Exception exception,
                     List<String> jsonFiles) {
        super(reportResult, "errorpage.vm", configuration);
        this.exception = exception;
        this.jsonFiles = jsonFiles;
    }

    @Override
    public String getWebPage() {
        return ReportBuilder.HOME_PAGE;
    }

    @Override
    public void prepareReport() {
        context.put("classifications", configuration.getClassifications());
        context.put("output_message", ExceptionUtils.getStackTrace(exception));
        context.put("json_files", jsonFiles);
    }
}
