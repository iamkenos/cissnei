package org.bitbucket.iamkenos.cissnei.masterthought.sorting;

import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Feature;

import java.util.Comparator;

/**
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class FeaturesAlphabeticalComparator implements Comparator<Feature> {

    @Override
    public int compare(Feature feature1, Feature feature2) {
        int nameCompare = ObjectUtils.compare(feature1.getName(), feature2.getName());
        if (nameCompare != 0) {
            return nameCompare;
        }

        int idCompare = ObjectUtils.compare(feature1.getId(), feature2.getId());
        if (idCompare != 0) {
            return idCompare;
        }

        return ObjectUtils.compare(feature1.getJsonFile(), feature2.getJsonFile());
    }
}
