package org.bitbucket.iamkenos.cissnei.masterthought.generators;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportBuilder;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportResult;
import org.bitbucket.iamkenos.cissnei.masterthought.ValidationException;
import org.bitbucket.iamkenos.cissnei.masterthought.utils.Counter;
import org.bitbucket.iamkenos.cissnei.masterthought.utils.ReportUtils;
import org.bitbucket.iamkenos.cissnei.utils.LogUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * Delivers common methods for page generation.
 *
 * @author Damian Szczepanik (damianszczepanik@github)
 */
abstract class AbstractPage {
    private final VelocityEngine engine = new VelocityEngine();
    protected final VelocityContext context = new VelocityContext();

    /**
     * Name of the HTML file which will be generated.
     */
    private final String templateFileName;
    /**
     * Results of the report.
     */
    protected final ReportResult reportResult;
    /**
     * Configuration used for this report execution.
     */
    protected final Configuration configuration;

    protected AbstractPage(ReportResult reportResult, String templateFileName, Configuration configuration) {
        this.templateFileName = templateFileName;
        this.reportResult = reportResult;
        this.configuration = configuration;

        this.engine.init(buildProperties());
        buildGeneralParameters();
    }

    public void generatePage() {
        prepareReport();
        generateReport();
    }

    /**
     * Returns HTML file name (with extension) for this report.
     *
     * @return HTML file for the report
     */
    public abstract String getWebPage();

    protected abstract void prepareReport();

    private void generateReport() {
        context.put("report_file", getWebPage());

        Template template = engine.getTemplate("templates/generators/" + templateFileName);
        File reportFile = new File(configuration.getReportDirectory(),
                ReportBuilder.BASE_DIRECTORY + getWebPage());
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(reportFile), StandardCharsets.UTF_8)) {
            template.merge(context, writer);
        } catch (IOException e) {
            throw new ValidationException(e);
        }
    }

    private Properties buildProperties() {
        Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getCanonicalName());
        props.setProperty("runtime.log", new File(configuration.getReportDirectory(), "velocity.log").getPath());

        return props;
    }

    private void buildGeneralParameters() {
        // to escape html and xml
        EventCartridge ec = new EventCartridge();
        ec.addEventHandler(new EscapeHtmlReference());
        context.attachEventCartridge(ec);

        // to provide unique ids for elements on each page
        context.put("counter", new Counter());
        context.put("util", ReportUtils.INSTANCE);

        context.put("run_with_jenkins", configuration.isRunWithJenkins());
        context.put("trends_present", configuration.getTrendsStatsFile() != null);
        context.put("build_project_name", configuration.getProjectName());
        context.put("build_number", configuration.getBuildNumber());

        // if report generation fails then report is null
        String formattedTime = reportResult != null ? reportResult.getBuildTime() : ReportResult.getCurrentTime();
        context.put("build_time", formattedTime);

        // build number is not mandatory
        String buildNumber = configuration.getBuildNumber();
        if (StringUtils.isNotBlank(buildNumber) && configuration.isRunWithJenkins()) {
            if (NumberUtils.isCreatable(buildNumber)) {
                context.put("build_previous_number", Integer.parseInt(buildNumber) - 1);
            } else {
                LogUtils.LOGGER.info("Could not parse build number: {}.", configuration.getBuildNumber());
            }
        }
    }
}
