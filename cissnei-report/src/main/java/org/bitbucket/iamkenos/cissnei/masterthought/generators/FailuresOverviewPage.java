package org.bitbucket.iamkenos.cissnei.masterthought.generators;


import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportResult;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Element;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Feature;

import java.util.ArrayList;
import java.util.List;

public final class FailuresOverviewPage extends AbstractPage {

    public static final String WEB_PAGE = "overview-failures.html";

    public FailuresOverviewPage(ReportResult reportResult, Configuration configuration) {
        super(reportResult, "overviewFailures.vm", configuration);
    }

    @Override
    public String getWebPage() {
        return WEB_PAGE;
    }

    @Override
    public void prepareReport() {
        context.put("failures", collectFailures());
    }

    private List<Element> collectFailures() {
        List<Element> failures = new ArrayList<>();
        for (Feature feature : reportResult.getAllFeatures()) {
            if (feature.getStatus().isPassed()) {
                continue;
            }

            for (Element element : feature.getElements()) {
                if (!element.getStepsStatus().isPassed()) {
                    failures.add(element);
                }
            }
        }
        return failures;
    }
}
