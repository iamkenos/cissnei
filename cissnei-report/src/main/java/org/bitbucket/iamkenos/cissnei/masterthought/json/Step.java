package org.bitbucket.iamkenos.cissnei.masterthought.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang.ArrayUtils;
import org.bitbucket.iamkenos.cissnei.masterthought.json.deserializers.OutputsDeserializer;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Argument;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Resultsable;

public final class Step implements Resultsable {

    private String name = null;
    private final String keyword = null;
    private final Result result = new Result();
    private final Row[] rows = new Row[0];
    @JsonProperty("arguments")
    private final Argument[] arguments = new Argument[0];
    private final Match match = null;
    private final Embedding[] embeddings = new Embedding[0];

    @JsonDeserialize(using = OutputsDeserializer.class)
    @JsonProperty("output")
    private final Output[] outputs = new Output[0];

    @JsonProperty("doc_string")
    private final DocString docString = null;

    public Row[] getRows() {
        if (ArrayUtils.getLength(arguments) == 1) {
            return arguments[0].getRows();
        } else if (ArrayUtils.getLength(arguments) > 1) {
            throw new UnsupportedOperationException("'arguments' length should be equal to 1");
        } else {
            return rows;
        }
    }

    public String getName() {
        return name;
    }

    public String getKeyword() {
        return keyword.trim();
    }

    @Override
    public Output[] getOutputs() {
        return outputs;
    }

    @Override
    public Match getMatch() {
        return match;
    }

    public Embedding[] getEmbeddings() {
        return embeddings;
    }

    @Override
    public Result getResult() {
        return result;
    }

    public long getDuration() {
        return result.getDuration();
    }

    public DocString getDocString() {
        return docString;
    }
}
