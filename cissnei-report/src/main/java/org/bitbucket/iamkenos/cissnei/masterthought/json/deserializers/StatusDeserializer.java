package org.bitbucket.iamkenos.cissnei.masterthought.json.deserializers;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Status;

import java.util.Collections;
import java.util.List;

/**
 * Deserializes Status and maps all known but not supported into UNDEFINED status.
 *
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class StatusDeserializer extends CucumberJsonDeserializer<Status> {

    static final List<String> UNKNOWN_STATUSES = Collections.singletonList("ambiguous");

    @Override
    public Status deserialize(JsonNode rootNode, Configuration configuration) {

        String status = rootNode.asText();
        if (UNKNOWN_STATUSES.contains(status)) {
            return Status.UNDEFINED;
        } else {
            return Status.valueOf(status.toUpperCase());
        }
    }
}
