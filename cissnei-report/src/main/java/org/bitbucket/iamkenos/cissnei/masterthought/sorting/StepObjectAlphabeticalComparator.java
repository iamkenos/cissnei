package org.bitbucket.iamkenos.cissnei.masterthought.sorting;

import org.bitbucket.iamkenos.cissnei.masterthought.json.support.StepObject;

import java.util.Comparator;

/**
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class StepObjectAlphabeticalComparator implements Comparator<StepObject> {

    @Override
    public int compare(StepObject stepObject1, StepObject stepObject2) {
        return Integer.signum(stepObject1.getLocation().compareTo(stepObject2.getLocation()));
    }
}
