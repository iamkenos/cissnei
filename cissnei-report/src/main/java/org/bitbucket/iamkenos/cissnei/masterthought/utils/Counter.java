package org.bitbucket.iamkenos.cissnei.masterthought.utils;

import org.apache.commons.lang3.mutable.MutableInt;

/**
 * Simple counter to give elements on a page a unique ID. Using object hashes
 * doesn't guarantee uniqueness.
 */
public final class Counter extends MutableInt {
    /**
     * @return The next integer
     */
    public int next() {
        increment();
        return intValue();
    }
}
