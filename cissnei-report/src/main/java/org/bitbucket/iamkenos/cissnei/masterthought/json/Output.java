package org.bitbucket.iamkenos.cissnei.masterthought.json;

/**
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public final class Output {

    private final String[] messages;

    public Output(String[] messages) {
        this.messages = messages;
    }

    public String[] getMessages() {
        return messages;
    }
}
