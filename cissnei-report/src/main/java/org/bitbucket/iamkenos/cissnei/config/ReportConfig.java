package org.bitbucket.iamkenos.cissnei.config;

import org.bitbucket.iamkenos.cissnei.adapter.ConfigAdapter;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ReportConfig extends CommonsConfig {

    private static final String RP_PROJECT_NAME = "report.project.name";
    private static final String RP_AUTO_LAUNCH = "report.auto.launch";

    private static final String PROP_FILE = "cissnei-report.properties";
    private ConfigAdapter properties;

    public ReportConfig() {
        properties = getProperties(PROP_FILE, this.getClass());
    }

    public String getReportProjectName() {
        return getValue(properties, RP_PROJECT_NAME);
    }

    public Boolean getReportAutoLaunch() {
        return Boolean.valueOf(getValue(properties, RP_AUTO_LAUNCH));
    }
}
