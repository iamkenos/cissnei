package org.bitbucket.iamkenos.cissnei.masterthought.json.support;

import org.bitbucket.iamkenos.cissnei.masterthought.json.Match;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Output;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Result;

/**
 * Ensures that class delivers method for counting results and matches.
 *
 * @author Damian Szczepanik (damianszczepanik@github)
 */
public interface Resultsable {

    Result getResult();

    Match getMatch();

    Output[] getOutputs();
}
