package org.bitbucket.iamkenos.cissnei.masterthought.json;

import org.bitbucket.iamkenos.cissnei.masterthought.utils.ReportUtils;

public final class Tag {

    private final String name = null;

    public String getName() {
        return name;
    }

    public String getFileName() {
        return generateFileName(name);
    }

    public static String generateFileName(String tagName) {
        return String.format("report-tag_%s.html", ReportUtils.toValidFileName(tagName.replace("@", "")).trim());
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object tag) {
        return ((Tag) tag).name.equals(name);
    }
}
