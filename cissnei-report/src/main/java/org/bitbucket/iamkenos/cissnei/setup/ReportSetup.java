package org.bitbucket.iamkenos.cissnei.setup;

import org.bitbucket.iamkenos.cissnei.config.ReportConfig;

import static org.bitbucket.iamkenos.cissnei.masterthought.ReportBuilder.BASE_DIRECTORY;
import static org.bitbucket.iamkenos.cissnei.masterthought.ReportBuilder.HOME_PAGE;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ReportSetup {
    public static final ReportConfig REPORT_CONFIG = new ReportConfig();
    public static final String DEF_REPORT_ROOT_DIR = "target/";
    public static final String DEF_JSON_FILE = "index.json";
    public static final String DEF_PLUGIN_ARG = "json:" + DEF_REPORT_ROOT_DIR + BASE_DIRECTORY + DEF_JSON_FILE;
    public static final String DEF_REPORT_HTML_PATH = DEF_REPORT_ROOT_DIR + BASE_DIRECTORY + HOME_PAGE;

    private ReportSetup() {

    }
}
