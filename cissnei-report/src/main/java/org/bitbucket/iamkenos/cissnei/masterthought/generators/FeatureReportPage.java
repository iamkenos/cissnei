package org.bitbucket.iamkenos.cissnei.masterthought.generators;

import org.bitbucket.iamkenos.cissnei.masterthought.Configuration;
import org.bitbucket.iamkenos.cissnei.masterthought.ReportResult;
import org.bitbucket.iamkenos.cissnei.masterthought.json.Feature;

public final class FeatureReportPage extends AbstractPage {

    private final Feature feature;

    public FeatureReportPage(ReportResult reportResult, Configuration configuration, Feature feature) {
        super(reportResult, "reportFeature.vm", configuration);
        this.feature = feature;
    }

    @Override
    public String getWebPage() {
        return feature.getReportFileName();
    }

    @Override
    public void prepareReport() {
        context.put("parallel", configuration.isParallelTesting());
        context.put("feature", feature);
    }

}
