package org.bitbucket.iamkenos.cissnei.masterthought.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.bitbucket.iamkenos.cissnei.masterthought.json.deserializers.OutputsDeserializer;
import org.bitbucket.iamkenos.cissnei.masterthought.json.support.Resultsable;

public final class Hook implements Resultsable {

    private final Result result = null;
    private final Match match = null;

    @JsonDeserialize(using = OutputsDeserializer.class)
    @JsonProperty("output")
    private final Output[] outputs = new Output[0];

    private final Embedding[] embeddings = new Embedding[0];

    @Override
    public Result getResult() {
        return result;
    }

    @Override
    public Match getMatch() {
        return match;
    }

    @Override
    public Output[] getOutputs() {
        return outputs;
    }

    public Embedding[] getEmbeddings() {
        return embeddings;
    }
}
